package com.commercecx.tasktracker.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.base.BaseSalesForceFragment
import com.commercecx.tasktracker.model.Quote
import com.commercecx.tasktracker.presenter.QuotesPresenter
import com.commercecx.tasktracker.presenter.QuotesViewTranslator
import kotlinx.android.synthetic.main.quote_detail_screen.view.*



/**
 * Created by Sagar Das on 8/25/20.
 */
class QuoteDetailFragment: BaseSalesForceFragment<QuotesPresenter>(), QuotesViewTranslator {

    private var fabExpanded = false
    private var bundle: Bundle? = null
    private lateinit var quotesPresenter: QuotesPresenter
    private var quote: Quote? = null
    private lateinit var root: View

    override fun onCreate(savedInstanceState: Bundle?) {
        val b: Bundle = requireArguments()
        quote = b.getSerializable("QUOTE") as Quote?
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.quote_detail_screen, container, false)



//        root.tvName.text = quote!!.name
//        root.tvDesc.text = "Dummy"
//        view.tvBudgetAmount.text = quote.billingStreet + "," + quote.billingCity + "," + quote.billingState + "," + quote.billingCountry + "," + quote.billingPostalCode
//        view.tvAccount.text = quote.shippingStreet + "," + quote.shippingCity + "," + quote.shippingState + "," + quote.shippingCountry + "," + quote.shippingPostalCode


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter().onViewCreated()
    }

    override fun onResume() {
        super.onResume()
        fabExpanded = false


    }

    override fun createPresenter(): QuotesPresenter = QuotesPresenter(this, quote!!.id)

    override fun displayQuotes(data: List<Quote>) {
        if(data.isNotEmpty()){

            quote = data[0]

        }

        root.tvName.text = quote!!.id
        root.tvDesc.text = quote!!.name

        root.pbQuotes.visibility = View.GONE
        root.cvQuote.visibility = View.VISIBLE

    }

    override fun getInstance(quotesPresenter: QuotesPresenter) {
        this.quotesPresenter = quotesPresenter
    }


}