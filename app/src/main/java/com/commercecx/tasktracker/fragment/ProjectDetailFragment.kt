package com.commercecx.tasktracker.fragment

import android.app.AlertDialog
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.base.BaseSalesForceFragment
import com.commercecx.tasktracker.interactors.ProjectsInteractor
import com.commercecx.tasktracker.model.*
import com.commercecx.tasktracker.model.database.DBProjectHelper
import com.commercecx.tasktracker.presenter.ProjectDetailPresenter
import com.commercecx.tasktracker.presenter.ProjectDetailViewTranslator
import com.commercecx.tasktracker.service.ProjectJobService
import com.commercecx.tasktracker.util.NetworkUtil
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.complete_dialog.view.*
import kotlinx.android.synthetic.main.fab_project_submenu.*
import kotlinx.android.synthetic.main.project_detail_screen.view.*
import java.math.RoundingMode
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Sagar Das on 11/18/20.
 */
class ProjectDetailFragment: BaseSalesForceFragment<ProjectDetailPresenter>(), ProjectDetailViewTranslator{

    private var fabExpanded = false
    private var bundle: Bundle? = null
    private lateinit var project: Project
    private var isEditMode: Boolean = false
    private lateinit var baseView: View
    private lateinit var projectsInteractor: ProjectsInteractor
    private lateinit var realm: Realm
    private lateinit var projectdetailspresenter: ProjectDetailPresenter
    private lateinit var networkUtil: NetworkUtil

    companion object{
        val TAG = "PROJECTDETAIL"
        val NOTE_UPDATE_JOB_ID = 201

    }


    internal lateinit var callback: OnTaskSelectedListener

    fun setOnTaskSelectedListener(callback: OnTaskSelectedListener) {
        this.callback = callback
    }

    interface OnTaskSelectedListener {
        fun onProjectTaskClicked(project: Project)
        fun onQuoteClicked(project: Project)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        baseView = inflater.inflate(R.layout.project_detail_screen, container, false)
        ButterKnife.bind(this, baseView)

        requireActivity().title = "Projects"

        realm = Realm.getDefaultInstance()
        ButterKnife.bind(this, baseView)
        val b: Bundle = requireArguments()
        project = b.getSerializable("PROJECT") as Project
        networkUtil = NetworkUtil(requireContext())
        baseView.tvName.setText(project.projectName)
        if(project.projectstatus == ""){
            baseView.tvDesc.setText("Pending")
        }
        else {
            baseView.tvDesc.setText(project.projectstatus)
        }
        
    //    val budgetAmount = "$" + project.budgetamount.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN)
        val budgetAmount = "$" +  String.format("%,.2f", project.budgetamount.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN));

        baseView.tvBudgetAmount.setText(budgetAmount)

        baseView.tvStartDate.setText(project.startDate)


        baseView.cvQuote.setOnClickListener( View.OnClickListener {

            callback.onQuoteClicked(project)
        })


        disableEditText(baseView.tvName)
        disableEditText(baseView.tvDesc)
        disableEditText(baseView.tvBudgetAmount)
        disableEditText(baseView.tvAccount)
        disableEditText(baseView.tvStartDate)

        setHasOptionsMenu(true)


        return baseView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter().onViewCreated(project.account,project.quoteProposal)
    }

    override fun onResume() {
        super.onResume()
        fabExpanded = false
        cvClone!!.visibility = View.GONE
        cvNewOrder!!.visibility = View.GONE
        fabClone!!.visibility = View.GONE
        fabNewOrder!!.visibility = View.GONE
        fabOrder!!.setImageResource(R.drawable.ic_add_no_circle)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_project, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_task -> {
               callback.onProjectTaskClicked(project)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    @OnClick(R.id.fabOrder)
    fun onClickOrder() {
        if (fabExpanded) {
           // cvClone!!.setVisibility(View.GONE)
            cvNewOrder!!.visibility = View.GONE
           // fabClone!!.setVisibility(View.GONE)
            fabNewOrder!!.visibility = View.GONE
            fabOrder!!.setImageResource(R.drawable.ic_add_no_circle)
            fabExpanded = false
            //  viewFade!!.visibility = View.GONE
        } else {
            //cvClone!!.setVisibility(View.VISIBLE)
            cvNewOrder!!.visibility = View.VISIBLE
            //fabClone!!.setVisibility(View.VISIBLE)
            fabNewOrder!!.visibility = View.VISIBLE
            fabOrder!!.setImageResource(R.drawable.ic_close_white)
            fabExpanded = true
            //    viewFade!!.visibility = View.VISIBLE
        }
    }

    @OnClick(R.id.fabNewOrder)
    fun onClickEdit(){

        fabExpanded = false
        isEditMode = true
        fabOrder.visibility = View.GONE
        fabOrder!!.setImageResource(R.drawable.ic_add_no_circle)


        enableEditText(baseView.tvName)
        enableEditText(baseView.tvDesc)
        enableEditText(baseView.tvBudgetAmount)
       // enableEditText(baseView.tvAccount)
        enableEditText(baseView.tvStartDate)

        ibApprove.setImageResource(R.drawable.ic_save)
        ibReject.setImageResource(R.drawable.cross_icon)

        fabNewOrder.visibility = View.GONE
        cvNewOrder.visibility = View.GONE




    }


    @OnClick(R.id.ibReject)
    fun onClickNewOrder() {
        if(isEditMode){

            isEditMode = false
            fabOrder.visibility = View.VISIBLE

            disableEditText(baseView.tvName)
            disableEditText(baseView.tvDesc)
            disableEditText(baseView.tvBudgetAmount)
            disableEditText(baseView.tvAccount)
            disableEditText(baseView.tvStartDate)

            ibApprove.setImageResource(R.drawable.ic_thumbs_up)
            ibReject.setImageResource(R.drawable.ic_thumbs_down)

            val toast = Toast.makeText(requireContext(), "Changes are dismissed", Toast.LENGTH_LONG)
            val toastView = toast.view
            val tvMessage = toastView.findViewById(android.R.id.message) as TextView
            tvMessage.textSize = 18F
            toast.show()

        }
        else {

            val dialogBuilder = AlertDialog.Builder(
                context
            )

            val completeDialog = layoutInflater.inflate(R.layout.complete_dialog, null)
            completeDialog.title.text = "Do you want to reject this project?"
            dialogBuilder.setView(completeDialog)
            val dialog = dialogBuilder.create()
            val btnYes =
                completeDialog.findViewById<View>(R.id.btnYes) as Button
            val btnNo =
                completeDialog.findViewById<View>(R.id.btnNo) as Button

            btnYes.setOnClickListener {
                //Saving data to soup

                project.projectstatus = "Rejected"
                baseView.tvDesc.setText("Rejected")
                // project.account = baseView.tvAccount.toString()
//            project.__locally_updated__ = true
//            project.__local__ = true


                val projects = mutableListOf<Project>()
                projects.add(project)

                DBProjectHelper().updateProjects(projects, false)

                if(networkUtil.isConnected){
                    sendUpdateToServer()
                }
                else{
                    scheduleUpdateJob()

                }

                val toast = Toast.makeText(requireContext(), "Project Rejected", Toast.LENGTH_LONG)
                val toastView = toast.view
                val tvMessage = toastView.findViewById(android.R.id.message) as TextView
                tvMessage.textSize = 18F
                toast.show()

                val d = Date()
                val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
                val strDate = dateFormat.format(d)

                val recentActivity = RecentActivityDash()

                val raResults: RealmResults<RecentActivityDash> =
                    realm.where(RecentActivityDash::class.java).findAll()
                val raId = if (raResults.size > 0) raResults[0]!!.getId() + 1 else 1
                recentActivity.setId(raId)
                recentActivity.setActivity_name("Rejected project "+project.projectName)
                recentActivity.setActivity_date(strDate)
                realm.executeTransactionAsync { realm ->

                    realm.insertOrUpdate(recentActivity)
                }


                dialog.dismiss()
            }


            btnNo.setOnClickListener { dialog.dismiss() }
            dialog.show()


        }
    }

    @OnClick(R.id.ibApprove)
    fun onClickClickOrder() {

        val dialogBuilder = AlertDialog.Builder(
            context
        )

        val completeDialog = layoutInflater.inflate(R.layout.complete_dialog, null)
        if(isEditMode){
            completeDialog.title.text = "Do you want to save the changes?"
        }
        else {
            completeDialog.title.text = "Do you want to approve this project?"
        }
        dialogBuilder.setView(completeDialog)
        val dialog = dialogBuilder.create()
        val btnYes =
            completeDialog.findViewById<View>(R.id.btnYes) as Button
        val btnNo =
            completeDialog.findViewById<View>(R.id.btnNo) as Button

        btnYes.setOnClickListener {

            performUpdate()

            dialog.dismiss()
        }


        btnNo.setOnClickListener {
            ibApprove.setImageResource(R.drawable.ic_thumbs_up)
            ibReject.setImageResource(R.drawable.ic_thumbs_down)
            dialog.dismiss() }
        dialog.show()
    }

    fun performUpdate(){

        if(isEditMode){


            //Saving data to soup
            project.projectName = baseView.tvName.text.toString()
            var budgetamount = baseView.tvBudgetAmount.text.toString()
            budgetamount = budgetamount.removePrefix("$")
            project.budgetamount = budgetamount
            // project.account = baseView.tvAccount.toString()
//            project.__locally_updated__ = true
//            project.__local__ = true


            val projects = mutableListOf<Project>()
            projects.add(project)

            DBProjectHelper().updateProjects(projects, false)

            if(networkUtil.isConnected){
                sendUpdateToServer()
            }
            else{

                scheduleUpdateJob()

            }

            isEditMode = false
            fabOrder.visibility = View.VISIBLE


            disableEditText(baseView.tvName)
            disableEditText(baseView.tvDesc)
            disableEditText(baseView.tvBudgetAmount)
            disableEditText(baseView.tvAccount)

            ibApprove.setImageResource(R.drawable.ic_thumbs_up)
            ibReject.setImageResource(R.drawable.ic_thumbs_down)


            val toast = Toast.makeText(requireContext(), "Changes are saved.", Toast.LENGTH_LONG)
            val toastView = toast.view
            val tvMessage = toastView.findViewById(android.R.id.message) as TextView
            tvMessage.textSize = 18F
            toast.show()





        }
        else {

            //Saving data to soup

            project.projectstatus = "Approved"
            baseView.tvDesc.setText("Approved")



            val projects = mutableListOf<Project>()
            projects.add(project)

            DBProjectHelper().updateProjects(projects, false)



            if(networkUtil.isConnected){
                sendUpdateToServer()
            }
            else{

                scheduleUpdateJob()

            }



            val toast = Toast.makeText(requireContext(), "Project Approved", Toast.LENGTH_LONG)
            val toastView = toast.view
            val tvMessage = toastView.findViewById(android.R.id.message) as TextView
            tvMessage.textSize = 18F
            toast.show()

            val d = Date()
            val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
            val strDate = dateFormat.format(d)

            val recentActivity = RecentActivityDash()
            val raResults: RealmResults<RecentActivityDash> =
                realm.where(RecentActivityDash::class.java).findAll()
            val raId = if (raResults.size > 0) raResults[raResults.size - 1]!!.getId() + 1 else 1
            recentActivity.setId(raId)
            recentActivity.setActivity_name("Approved project "+project.projectName)
            recentActivity.setActivity_date(strDate)
            realm.executeTransactionAsync { realm ->
                realm.insertOrUpdate(recentActivity)
            }


        }
    }


    fun sendUpdateToServer(){

        val projectData = "{" +
                " \"projectid\" : " + "\"" + project.id + "\"," +
                " \"name\" : " + "\"" + project.projectName  + "\"," +
                " \"budgetstatus\" : " + "\"" + project.projectstatus + "\"," +
                " \"budgetamount\" : " + "\"" + project.budgetamount + "\"," +
                " \"startdate\" : " + "\"" + project.startDate + "\"" +
                "}"

        projectsInteractor = ProjectsInteractor(BaseApplication.instance.restClient!!, projectData)
        projectsInteractor.syncCall()
    }

    fun scheduleUpdateJob(){

        var note = Note()
        note.setId(project.id)
        note.setServerCommitted(false)
        note.setName("project")

        realm.executeTransactionAsync { realm -> realm.insertOrUpdate(note) }


        val jobScheduler = requireActivity().getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler

        val jobService = ComponentName(requireContext(), ProjectJobService::class.java)

        val jobInfo =
            JobInfo.Builder(NOTE_UPDATE_JOB_ID, jobService)
                .setPersisted(true)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .build()

        if (jobScheduler.schedule(jobInfo) != JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "job scheduling failed ")
        }



    }





    @OnClick(R.id.cvQuote)
    fun onQuoteClicked(){
        callback.onQuoteClicked(project)
    }

    private fun disableEditText(editText: EditText) {
       // editText.isFocusable = false
        editText.isEnabled = false
      //  editText.isCursorVisible = false
      //  editText.keyListener = null
      // editText.setBackgroundColor(Color.TRANSPARENT)
        editText.background.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.SRC_IN)
    }

    private fun enableEditText(editText: EditText) {
       // editText.isFocusable = true
        editText.isEnabled = true
        //editText.isCursorVisible = true
        //editText.setBackgroundColor(resources.getColor(R.color.cool_gray_101))
        editText.background.setColorFilter(resources.getColor(R.color.cool_gray_101), PorterDuff.Mode.SRC_IN)


    }

    override fun createPresenter(): ProjectDetailPresenter = ProjectDetailPresenter(this)

    override fun displayAccount(data: List<Account>) {
        if(!data.isEmpty()){
            val account = data[0]
            baseView.tvAccountName.setText(account.name)
            baseView.tvAccount.setText(account.name)
            baseView.tvAccountDesc.setText(account.description)
        }
    }

    override fun displayOpportunity(data: List<Opportunity>) {
        if(!data.isEmpty()){
            val opportunity = data[0]
            baseView.tvOpportunityName.setText(opportunity.name)

        }
    }

    override fun getInstance(projectdetailspresenter: ProjectDetailPresenter) {
        this.projectdetailspresenter = projectdetailspresenter
    }


}