package com.commercecx.tasktracker.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.adapter.CatalogGridLayoutManager
import com.commercecx.tasktracker.adapter.QuotesAdapter
import com.commercecx.tasktracker.base.BaseSalesForceFragment
import com.commercecx.tasktracker.model.OrderOld
import com.commercecx.tasktracker.model.Quote
import com.commercecx.tasktracker.model.RouteOld
import com.commercecx.tasktracker.presenter.QuotesPresenter
import com.commercecx.tasktracker.presenter.QuotesViewTranslator
import com.commercecx.tasktracker.util.Utils
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_quote.*

/**
 * Created by Sagar Das on 8/25/20.
 */
class QuoteFragment : BaseSalesForceFragment<QuotesPresenter>(), SwipeRefreshLayout.OnRefreshListener,
    QuotesViewTranslator {
    private val DEFAULT_PAGE_NUMBER = 1
    private val quantity = 0
    private var linearLayoutManager: LinearLayoutManager? = null
    private var gridLayoutManager: CatalogGridLayoutManager? = null

    private var mCurrentPage = DEFAULT_PAGE_NUMBER

    // region Constants
    private val isLoading = false
    private var accountAdapter: QuotesAdapter? = null
    private val name: String? = null
    private var cActivity: Activity? = null
    private lateinit var accountsPresenter: QuotesPresenter

    private var bundle: Bundle? = null
    private var route: RouteOld? = null
    private var orderList: List<OrderOld>? = null
    private var realm: Realm? = null

    internal lateinit var callback: OnSearchSelectedListener

    fun setOnSearchSelectedListener(callback: OnSearchSelectedListener) {
        this.callback = callback
    }

    interface OnSearchSelectedListener {
        fun onQuoteSearchClicked()
    }





    private val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (gridLayoutManager != null) {
                try {
                    if (!isLoading) {
                        val visibleItemCount: Int = gridLayoutManager!!.childCount
                        val totalItemCount: Int = gridLayoutManager!!.itemCount
                        val firstVisibleItemPosition: Int =
                            gridLayoutManager!!.findLastVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        }
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        requireActivity().title = "Quotes"
        val view: View = inflater.inflate(R.layout.fragment_quote, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter().onViewCreated()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        linearLayoutManager = LinearLayoutManager(cActivity, LinearLayoutManager.VERTICAL, false)
        gridLayoutManager = CatalogGridLayoutManager(requireContext(), Utils.CATALOG_GRID_COLUMN_SIZE)
        //  linearLayoutManager = new WrapContentLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mCurrentPage = DEFAULT_PAGE_NUMBER
        homeRV!!.layoutManager = linearLayoutManager
        // homeRV.setLayoutManager(linearLayoutManager);
        homeRV!!.setHasFixedSize(true)
        homeRV!!.setItemViewCacheSize(20)
        homeRV!!.isDrawingCacheEnabled = true
        homeRV!!.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        //calling onScrollListner for Pagination
        homeRV!!.addOnScrollListener(onScrollListener)
        swipeRefreshLayout!!.setOnRefreshListener(this)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.header_theme)
        startShimmerAnimation()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        this.cActivity = activity

    }

    private fun startShimmerAnimation() {
        shimmerEffectLayout!!.baseAlpha = 0.7f
        shimmerDefaultLL!!.visibility = View.VISIBLE
        homeRV!!.visibility = View.GONE
        shimmerEffectLayout!!.startShimmerAnimation()
//        val handler = Handler()
//        handler.postDelayed({ //Do something after 100ms
//            setHomeFeedAdapter()
//        }, 500)
    }



    private fun stopShimmerAnimation() {
        shimmerEffectLayout!!.stopShimmerAnimation()
        homeRV!!.visibility = View.VISIBLE
        shimmerDefaultLL!!.visibility = View.GONE
    }

    fun scrollToBeginning() {
        gridLayoutManager!!.scrollToPositionWithOffset(0, 0)
    }

    private fun setHomeFeedAdapter(data: MutableList<Quote>) {
        if (mCurrentPage == DEFAULT_PAGE_NUMBER) if (true) {
            noDataTextTV!!.visibility = View.GONE
            accountAdapter = QuotesAdapter(requireActivity(), data, requireContext())
            homeRV!!.adapter = accountAdapter
        } else {
            noDataTextTV!!.visibility = View.VISIBLE
        }
        stopShimmerAnimation()
    }





    override fun onRefresh() {}

    companion object {
        private val TAG = QuoteFragment::class.java.simpleName
    }



    override fun createPresenter(): QuotesPresenter =
        QuotesPresenter(this,null)

    override fun displayQuotes(data: List<Quote>) {
        setHomeFeedAdapter(data.toMutableList())
    }

    override fun getInstance(accountsPresenter: QuotesPresenter) {
        this.accountsPresenter = accountsPresenter
    }

    @OnClick(R.id.ibSearch)
    fun onSearchClick() {
        //startActivity(Intent(getContext(), ImportActivity::class.java))
        callback.onQuoteSearchClicked()
    }

    fun receiveSearchQuery(query: String?){
        Toast.makeText(requireContext(),query, Toast.LENGTH_SHORT).show()
       accountsPresenter.searchQuotes(query)


    }



}
