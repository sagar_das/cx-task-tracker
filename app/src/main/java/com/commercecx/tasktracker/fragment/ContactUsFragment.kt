package com.commercecx.tasktracker.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_contact_us.*


/**
 * A simple [Fragment] subclass.
 */
class ContactUsFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_contact_us, container, false)
        ButterKnife.bind(this, v)
        tvContactUs!!.text =
            """LMX (Last Mile Exchange) Lite is a product of CommerceCX, Inc. The App is Self-Serving Order Creation application with the following key features.
-	Create Customers
-	Create Products/Services
-	Create Delivery Routes
-	Create Orders 
-	Generate Invoice
-	Submit Order/Invoice to Customers through Email.

The App is completely free . The App does not save any data to any of the servers or third party sites and in compliance of the Data Protection policies., 

Who can Use the app? 

Anybody who wants to place an order for any Customer. This can be Gig workers, Volunteers, Vending Machine Operators etc.
 
We also provide a custom Enterprise Standard version of LMX App with flexibility for Integration to SaleForce or AWS . The Premium version would have smart features such as real time sync of Products, Pricing, Ordering and Invoicing information SalesForce or other Cloud Providers.  

Interested? For business related queries, please contact us info@commercecx.com . More information about our Services available at www.commercecx.com 
"""
        return v
    }
}
