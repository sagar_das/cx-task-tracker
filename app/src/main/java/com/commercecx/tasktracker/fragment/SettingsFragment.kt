package com.commercecx.tasktracker.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.app.Activity
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import android.preference.PreferenceManager

import android.util.Patterns
import android.widget.Toast
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.commercecx.tasktracker.R


/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : PreferenceFragmentCompat(),
    OnSharedPreferenceChangeListener {
    interface ISettingsPrefListener {
        fun onPrefChanged()
        fun onCLickContactUs()
        fun onClickLogout()
    }

    var mSettingsPref: ISettingsPrefListener? = null
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preference_main, rootKey)
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)


        //prefFirstName = (EditTextPreference) findPreference<Preference>("prefFirstName")
        //prefFirstName.setText(preferences.getString("prefFirstName", "Guest"));
    }

    override fun onResume() {
        super.onResume()
        val sp: SharedPreferences = preferenceScreen.sharedPreferences
        sp.registerOnSharedPreferenceChangeListener(this)
//        findPreference<Preference>("prefFirstName")!!.summary = sp.getString(
//            "prefFirstName",
//            "First name of user"
//        )
//        findPreference<Preference>("prefLastName")!!.summary = sp.getString("prefLastName", "Last name of user")
//        findPreference<Preference>("prefEmailId")!!.summary = sp.getString("prefEmailId", "Email Id of user")
//        findPreference<Preference>("prefCompanyName")!!.summary = sp.getString(
//            "prefCompanyName",
//            "Company name of user"
//        )

        val prefContactUs: Preference = findPreference<Preference>("prefContactUs")!!
        prefContactUs.setOnPreferenceClickListener {
            mSettingsPref!!.onCLickContactUs()
            true
        }
        val prefLogout: Preference = findPreference<Preference>("prefLogout")!!
        prefLogout.setOnPreferenceClickListener {
            mSettingsPref!!.onClickLogout()
            true
        }
    }



    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mSettingsPref = activity as ISettingsPrefListener?
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences
            .unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        if (key.equals(
                "prefEmailId",
                ignoreCase = true
            ) && !isEmailValid(sharedPreferences.getString("prefEmailId", "Guest"))
        ) {
            Toast.makeText(context, "Please enter a valid email id", Toast.LENGTH_SHORT).show()
            return
        }
        findPreference<Preference>(key)!!.summary = sharedPreferences.getString(key, "")
        mSettingsPref!!.onPrefChanged()
    }

    fun isEmailValid(email: CharSequence?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email)
            .matches()
    }
}
