package com.commercecx.tasktracker.fragment

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.activity.DashboardPref
import com.commercecx.tasktracker.adapter.NotificationAdapter
import com.commercecx.tasktracker.adapter.RecentActivityAdapter
import com.commercecx.tasktracker.base.BaseSalesForceFragment
import com.commercecx.tasktracker.model.*
import com.commercecx.tasktracker.presenter.ProjectsPresenter
import com.commercecx.tasktracker.presenter.ProjectsViewTranslator
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.dashboard_stats_main.*
import kotlinx.android.synthetic.main.dashboard_stats_main.view.*
import java.util.*


class DashStatsFragment : BaseSalesForceFragment<ProjectsPresenter>(), NotificationAdapter.INotificationClickListener,
    ProjectsViewTranslator {
    private val tts: TextToSpeech? = null
    private val isNote = false
    private var realm: Realm? = null
    var notificationAdapter: NotificationAdapter? = null
    var recentActivityAdapter: RecentActivityAdapter? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var llmanagerRA: LinearLayoutManager? = null
    private val text: String? = null
    private var notificationList: List<Notification>? = null
    private var recentActivityList: List<RecentActivityDash>? = null
    private var orderList: List<OrderOld>? = null
    private var routeList: List<RouteOld>? = null
    private lateinit var projectsPresenter: ProjectsPresenter
    private lateinit var projects: List<Project>
    private lateinit var root: View
    var fromServer = false

    interface INotificationUpdateListener {
        fun reduceNotificationCount()
        fun onPendingProjectClick()
    }

    var mNotifyUpdate: INotificationUpdateListener? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        requireActivity().title = "Dashboard"
        root = inflater.inflate(R.layout.activity_dashboard, container, false)
        ButterKnife.bind(this, root)
        realm = Realm.getDefaultInstance()

        root.cvPendingProjects.setOnClickListener{
            mNotifyUpdate!!.onPendingProjectClick()
        }

        setHasOptionsMenu(true)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter().getAllPendingProjects()
    }

    override fun onResume() {
        super.onResume()
        val settings: SharedPreferences = requireActivity().getSharedPreferences(PREFS_NAME, 0)
        if (settings.getBoolean("recentactivity", true)) {
            cvRecentActivity!!.visibility = View.VISIBLE
        } else {
            cvRecentActivity!!.visibility = View.GONE
        }


     //   fetchNotifications()
        fetchRecentActivityList()
      //  setNotificationAdapter()
        setRecentActivityAdapter()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_dashboard, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh -> {
                fetchTasksFromServer()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    fun fetchTasksFromServer(){
        root.cvPendingProjects.visibility = View.GONE
        root.pbPendingProjects.visibility = View.VISIBLE
        fromServer = true
        presenter().refreshData()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mNotifyUpdate = activity as INotificationUpdateListener?
    }

    private fun setNotificationAdapter() {
        if (notificationList!!.size > 0) {
            tvNoNotifications!!.visibility = View.GONE
            notificationAdapter = NotificationAdapter(this, requireContext(), notificationList!!)
            linearLayoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rvNotifications!!.layoutManager = linearLayoutManager
            rvNotifications!!.adapter = notificationAdapter
        } else {
            //tvNoNotifications.setVisibility(View.VISIBLE);
        }
    }

    private fun setRecentActivityAdapter() {
        if (recentActivityList!!.size > 0) {
            //tvNoNotifications.setVisibility(View.GONE);
            recentActivityAdapter = RecentActivityAdapter(recentActivityList!!, requireContext())
            llmanagerRA = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            rvRecentActivity!!.layoutManager = llmanagerRA
            rvRecentActivity!!.adapter = recentActivityAdapter
        } else {
            //tvNoNotifications.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.fabMore)
    fun onPrefClick() {
        startActivity(Intent(context, DashboardPref::class.java))
    }

    private fun fetchNotifications() {
        notificationList = ArrayList<Notification>()
        var results: RealmResults<Notification> = realm!!.where(Notification::class.java).findAll()
        results = results.sort("updatedTSMP", Sort.DESCENDING)
        notificationList = realm!!.copyFromRealm(results)
    }

    private fun fetchRecentActivityList() {
        recentActivityList = ArrayList<RecentActivityDash>()
        val results: RealmResults<RecentActivityDash> =
            realm!!.where(RecentActivityDash::class.java).sort("id",Sort.DESCENDING).findAll()
        recentActivityList = realm!!.copyFromRealm(results)
    }

    private fun setPendingProjectsCount() {

        root.pbPendingProjects.visibility = View.GONE
        root.cvPendingProjects.visibility = View.VISIBLE

        if(projects.isEmpty()){
            root.tvProjectCount!!.text = "0"
            root.cvPendingProjects.setOnClickListener(null)
        }
        else{
            root.tvProjectCount!!.text = projects.size.toString()
        }



    }

    override fun OnNotificationClick() {
        mNotifyUpdate!!.reduceNotificationCount()
    }

    companion object {
        private const val SPEECH_REQUEST_CODE = 101
        private val TAG: String = "DASHBOARD"
        private const val PREFS_NAME = "dashboardpref"
    }

    override fun createPresenter(): ProjectsPresenter = ProjectsPresenter(this)

    override fun displayProjects(data: List<Project>) {

        if(fromServer){
            fromServer = false
            presenter().getAllPendingProjects()
        }
        else {
            projects = data
            setPendingProjectsCount()
        }
    }

    override fun getInstance(projectsPresenter: ProjectsPresenter) {
        this.projectsPresenter = projectsPresenter
    }
}
