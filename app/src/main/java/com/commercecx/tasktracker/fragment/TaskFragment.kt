package com.commercecx.tasktracker.fragment

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.base.BaseSalesForceFragment
import com.commercecx.tasktracker.model.Product
import com.commercecx.tasktracker.model.Project
import com.commercecx.tasktracker.model.ProjectTask
import com.commercecx.tasktracker.model.database.DBProductHelper
import com.commercecx.tasktracker.model.viewmodel.SheetViewModel
import com.commercecx.tasktracker.presenter.PricingTaskPresenter
import com.commercecx.tasktracker.presenter.PricingTasksViewTranslator
import com.commercecx.tasktracker.viewpager.SheetViewPager
import kotlinx.android.synthetic.main.filter_by_dialog.view.*
import kotlinx.android.synthetic.main.fragment_task.view.*


class TaskFragment : BaseSalesForceFragment<PricingTaskPresenter>(), PricingTasksViewTranslator {

    lateinit var fragmentRecyclerView : RecyclerView
    private var viewModel: SheetViewModel? = null
    lateinit var mPager: SheetViewPager
    lateinit var data: List<ProjectTask>
    lateinit var products: MutableList<Product>

    var tabCount = 1

    var selectedTab = 0

    val MY_REQ_READ_EXTERNAL_STORAGE : Int = 93 // random magic number



    companion object{
        lateinit var currentInstance: Fragment
    }

    var leftRowMargin = 0
    var topRowMargin = 0
    var rightRowMargin = 0
    var bottomRowMargin = 0
    var textSize = 0f
    var smallTextSize = 0f
    var mediumTextSize = 0f
    lateinit var baseView: View
    private lateinit var pricingTaskPresenter: PricingTaskPresenter
    lateinit var project: Project



    override fun onCreate(savedInstanceState: Bundle?) {



        val bundle = requireArguments()
        project = bundle.getSerializable("PROJECT") as Project
        super.onCreate(savedInstanceState)
        retainInstance = true
        this.data = mutableListOf<ProjectTask>()
        this.products = mutableListOf<Product>()


    }

    

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        baseView = inflater.inflate(R.layout.fragment_task, container, false)
        baseView.tableTasks.isStretchAllColumns = true

        setHasOptionsMenu(true)


        return baseView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter().onViewCreated()
    }

    override fun onAttachFragment(childFragment: Fragment) {
        currentInstance = childFragment
        super.onAttachFragment(childFragment)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_task, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_filter -> {
                showFilterDialog()
                true
            }
            R.id.action_refresh ->{
                refreshData()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showFilterDialog() {
        //Show confirmation dialog
        val dialogBuilder = AlertDialog.Builder(requireContext())
        val filterDialog: View = getLayoutInflater().inflate(R.layout.filter_by_dialog, null)
        val countryList = listOf("--Select--", "Argentina","Canada")
        val productList = listOf("--Select--", "Vendor Mgmt","CRA Training")
        filterDialog.spinCountry.setItems(countryList)
        filterDialog.spinProduct.setItems(productList)
        dialogBuilder.setView(filterDialog)
        val dialog = dialogBuilder.create()
        val ok = filterDialog.findViewById<View>(R.id.btnOK) as Button
        val cancel = filterDialog.findViewById<View>(R.id.btnCancel) as Button
        ok.setOnClickListener {

            val country = countryList[filterDialog.spinCountry.selectedIndex]
            var product = productList[filterDialog.spinProduct.selectedIndex]


            if(country!= "--Select--"){
                baseView.pbTasks.visibility = View.VISIBLE
                baseView.svData.visibility = View.GONE
                presenter().seatchProjectTasksByCountryName(country)
            }
            else if(product!= "--Select--"){
                baseView.pbTasks.visibility = View.VISIBLE
                baseView.svData.visibility = View.GONE

              for(item in this.products){

                  if(item.productname == product){
                      product = item.productid
                      break
                  }
              }

                presenter().seatchProjectTasksByProductName(product)
        }



            dialog.dismiss()

        }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    fun refreshData(){
        baseView.svData.visibility = View.GONE
        baseView.pbTasks.visibility = View.VISIBLE
        presenter().refreshData()
    }


    fun loadTaskData(projectTasks: List<ProjectTask>){

        textSize = resources.getDimension(R.dimen.font_size_verysmall)
        smallTextSize = resources.getDimension(R.dimen.font_size_small)
        mediumTextSize = resources.getDimension(R.dimen.font_size_medium)


        baseView.tableTasks.removeAllViews()
        var textSpacer: TextView? = null

        for(i in -1 until projectTasks.size){

            var projectTask: ProjectTask? = null

            if (i > -1){
                projectTask = projectTasks[i]
            }

            else {
                textSpacer = TextView(requireContext())
                textSpacer.text = ""
            }

            val tv = TextView(requireContext())
            tv.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT
            )
            tv.gravity = Gravity.LEFT
            tv.setPadding(5, 15, 0, 15)



            if (i == -1) {
                tv.text = "Task Name"
                tv.setBackgroundColor(Color.parseColor("#f0f0f0"))
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize)
            }
            else {
                tv.setBackgroundColor(Color.parseColor("#f8f8f8"))
                tv.text = projectTask!!.name
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize)
            }

            val tv2 = TextView(requireContext())
            if (i === -1) {
                tv2.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT
                )
                tv2.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize)
            } else {
                tv2.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
                tv2.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize)
            }
            tv2.gravity = Gravity.LEFT
            tv2.setPadding(5, 15, 0, 15)
            if (i === -1) {
                tv2.text = "No. of Units"
                tv2.setBackgroundColor(Color.parseColor("#f7f7f7"))
            } else {
                tv2.setBackgroundColor(Color.parseColor("#ffffff"))
                tv2.setTextColor(Color.parseColor("#000000"))
                tv2.text = projectTask!!.numberOfUnits
            }

            val layoutBudgetHours = LinearLayout(requireContext())
            layoutBudgetHours.orientation = LinearLayout.VERTICAL
            layoutBudgetHours.setPadding(0, 10, 0, 10)
            layoutBudgetHours.setBackgroundColor(Color.parseColor("#f8f8f8"))
            val tv3 = TextView(requireContext())
            if (i === -1) {
                tv3.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
                tv3.setPadding(5, 5, 0, 5)
                tv3.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize)
            } else {
                tv3.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
                tv3.setPadding(5, 0, 0, 5)
                tv3.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize)
            }
            tv3.gravity = Gravity.TOP
            if (i === -1) {
                tv3.text = "Hours per unit"
                tv3.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv3.setBackgroundColor(Color.parseColor("#f8f8f8"))
                tv3.setTextColor(Color.parseColor("#000000"))
                tv3.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize)
                tv3.text = projectTask!!.budgetHours
            }
            layoutBudgetHours.addView(tv3)
            if (i > -1) {
                val tv3b = TextView(requireContext())
                tv3b.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT
                )
                tv3b.gravity = Gravity.RIGHT
                tv3b.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize)
                tv3b.setPadding(5, 1, 0, 5)
                tv3b.setTextColor(Color.parseColor("#aaaaaa"))
                tv3b.setBackgroundColor(Color.parseColor("#f8f8f8"))
                //tv3b.setText(row.customerAddress)
                layoutBudgetHours.addView(tv3b)
            }

            val layoutRole = LinearLayout(requireContext())
            layoutRole.orientation = LinearLayout.VERTICAL
            layoutRole.gravity = Gravity.RIGHT
            layoutRole.setPadding(0, 10, 0, 10)
            layoutRole.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT
            )
            val tv4 = TextView(requireContext())
            if (i === -1) {
                tv4.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
                tv4.setPadding(5, 5, 1, 5)
                layoutRole.setBackgroundColor(Color.parseColor("#f7f7f7"))
            } else {
                tv4.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT
                )
                tv4.setPadding(5, 0, 1, 5)
                layoutRole.setBackgroundColor(Color.parseColor("#ffffff"))
            }
            tv4.gravity = Gravity.LEFT
            if (i === -1) {
                tv4.text = "Product"
                tv4.setBackgroundColor(Color.parseColor("#f7f7f7"))
                tv4.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize)
            } else {
                tv4.setBackgroundColor(Color.parseColor("#ffffff"))
                tv4.setTextColor(Color.parseColor("#000000"))
               // val products = presenter().getProductsById(projectTask!!.product)
                val products = DBProductHelper().getProductsById(projectTask!!.product)
                if(!products.isNullOrEmpty()){
                    tv4.text = products[0].productname
                    this.products.add(products[0])

                }

                tv4.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize)
            }
            layoutRole.addView(tv4)
            if (i > -1) {
                val tv4b = TextView(requireContext())
                tv4b.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT
                )
                tv4b.gravity = Gravity.RIGHT
                tv4b.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize)
                tv4b.setPadding(2, 2, 1, 5)
                tv4b.setTextColor(Color.parseColor("#00afff"))
                tv4b.setBackgroundColor(Color.parseColor("#ffffff"))
                var due = ""

                tv4b.text = due
                layoutRole.addView(tv4b)
            }


            val layoutDeliveryCountry = LinearLayout(requireContext())
            layoutDeliveryCountry.orientation = LinearLayout.VERTICAL
            layoutDeliveryCountry.setPadding(0, 10, 0, 10)
            layoutDeliveryCountry.setBackgroundColor(Color.parseColor("#f8f8f8"))
            val tv5 = TextView(requireContext())
            if (i === -1) {
                tv5.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
                tv5.setPadding(5, 5, 0, 5)
                tv5.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize)
            } else {
                tv5.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
                tv5.setPadding(5, 0, 0, 5)
                tv5.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize)
            }
            tv5.gravity = Gravity.TOP
            if (i === -1) {
                tv5.text = "Delivery Country"
                tv5.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv5.setBackgroundColor(Color.parseColor("#f8f8f8"))
                tv5.setTextColor(Color.parseColor("#000000"))
                tv5.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize)
                tv5.text = projectTask!!.deliveryCountry
            }
            layoutDeliveryCountry.addView(tv5)
            if (i > -1) {
                val tv5b = TextView(requireContext())
                tv5b.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT
                )
                tv5b.gravity = Gravity.RIGHT
                tv5b.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize)
                tv5b.setPadding(5, 1, 0, 5)
                tv5b.setTextColor(Color.parseColor("#aaaaaa"))
                tv5b.setBackgroundColor(Color.parseColor("#f8f8f8"))
                layoutDeliveryCountry.addView(tv5b)
            }





            // add table row
            val tr = TableRow(requireContext())
            tr.id = i + 1
            val trParams = TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT
            )
            trParams.setMargins(
                leftRowMargin, topRowMargin, rightRowMargin,
                bottomRowMargin
            )
            tr.setPadding(0, 0, 0, 0)
            tr.layoutParams = trParams
            tr.addView(tv)
            tr.addView(tv2)
            tr.addView(layoutBudgetHours)
            tr.addView(layoutRole)
            tr.addView(layoutDeliveryCountry)
            if (i > -1) {
                tr.setOnClickListener { v -> val tr = v as TableRow }
            }


            baseView.tableTasks.addView(tr, trParams)
            if (i > -1) {
                // add separator row
                val trSep = TableRow(requireContext())
                val trParamsSep = TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT
                )
                trParamsSep.setMargins(
                    leftRowMargin, topRowMargin,
                    rightRowMargin, bottomRowMargin
                )
                trSep.layoutParams = trParamsSep
                val tvSep = TextView(requireContext())
                val tvSepLay =
                    TableRow.LayoutParams(
                        TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT
                    )
                tvSepLay.span = 4
                tvSep.layoutParams = tvSepLay
                tvSep.setBackgroundColor(Color.parseColor("#d9d9d9"))
                tvSep.height = 1
                trSep.addView(tvSep)
                baseView.tableTasks.addView(trSep, trParamsSep)
            }



        //loop exits here
        }

    }

    override fun createPresenter(): PricingTaskPresenter =
        PricingTaskPresenter(this, project.id)

    override fun displayProjectTasks(data: List<ProjectTask>) {
        Log.i("dd","ss")
        this.data = data
        baseView.pbTasks.visibility = View.GONE

        if(data.isEmpty()){
            baseView.tvNoData.visibility = View.VISIBLE
            return
        }

        baseView.svData.visibility = View.VISIBLE
        loadTaskData(data)
    }

    override fun getInstance(pricingTaskPresenter: PricingTaskPresenter) {
        this.pricingTaskPresenter = pricingTaskPresenter
    }






}