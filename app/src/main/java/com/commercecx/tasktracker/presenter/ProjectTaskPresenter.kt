package com.commercecx.tasktracker.presenter

import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.base.BasePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import com.commercecx.tasktracker.interactors.*
import com.commercecx.tasktracker.model.Product
import com.commercecx.tasktracker.model.ProjectTask
import com.salesforce.androidsdk.rest.RestClient
import java.lang.ref.WeakReference

/**
 * Created by Sagar Das on 11/19/20.
 */
class PricingTaskPresenter (
    val view: PricingTasksViewTranslator,
    val projectID: String?
) : BasePresenter<PricingTasksViewTranslator>(WeakReference(view)) {

    private var restClient: RestClient? = null
    private lateinit var projectTasksInteractor: GetProjectTasksInteractor
    private lateinit var pricingTasksByProjectNameInteractor: GetPricingTasksByProjectNameInteractor

//    override fun onResume(client: RestClient?) {
//        super.onResume(client)
//
//        client?.let {
//            pricingTasksInteractor = GetPricingTasksInteractor(it, null)
//        }
//    }

    fun onViewCreated() {


        view()?.getInstance(this)



        GetProjectTasksInteractor(BaseApplication.instance.restClient!!, null, projectID, true).execute {


            it.data?.let { data ->

                var pricingTaskList = data.toMutableList()

                view()?.displayProjectTasks(pricingTaskList)

            }
        }

    }

    fun refreshData(){
        view()?.getInstance(this)



        GetProjectTasksInteractor(BaseApplication.instance.restClient!!, null, projectID, false).execute {


            it.data?.let { data ->

                var pricingTaskList = data.toMutableList()

                view()?.displayProjectTasks(pricingTaskList)

            }
        }
    }


    fun searchPricingTasks(query: String?){
        val searchQuery = "%$query%"
        GetPricingTasksByNameInteractor().execute(PricingTaskNameFilter(searchQuery)) {
            //view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayProjectTasks(data)
            }
        }
    }

    fun seatchProjectTasksByCountryName(countryName: String){
        val searchQuery = "%$countryName%"
        GetProjectTasksByCountryInteractor().execute(PricingTaskNameFilter(searchQuery)) {
            //view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayProjectTasks(data)
            }
        }
    }

    fun seatchProjectTasksByProductName(productName: String){
        val searchQuery = "%$productName%"
        GetProjectTasksByProductInteractor().execute(PricingTaskNameFilter(searchQuery)) {
            //view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayProjectTasks(data)
            }
        }
    }

    fun getProductsById(productId: String?): List<Product>?{


        var products: List<Product>? = null

        GetProductsByIdInteractor().execute(ProductNameFilter(productId!!)) {
            //view()?.hideLoader()
            it.data?.let { data ->
                products = data
            }
        }

        return products
    }






}

interface PricingTasksViewTranslator : IBaseViewTranslator {

    fun displayProjectTasks(data: List<ProjectTask>)
    fun getInstance(pricingTaskPresenter: PricingTaskPresenter)

}
