package com.commercecx.tasktracker.presenter

/**
 * Created by Sagar Das on 8/20/20.
 */
import android.content.SharedPreferences
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.base.BaseSalesForcePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import com.commercecx.tasktracker.interactors.SyncDataInteractor

import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.smartstore.store.SmartStore
import java.lang.ref.WeakReference


class HomePresenter(
    private val store: SmartStore,
    private val preferences: SharedPreferences,
    val view: HomeViewTranslator
) : BaseSalesForcePresenter<HomeViewTranslator>(WeakReference(view)) {

    private var restClient: RestClient? = null
    private lateinit var syncInteractor: SyncDataInteractor
    var name = ""
    var email = ""

    override fun onResume(client: RestClient?) {
        super.onResume(client)

        BaseApplication.instance.myRoute = null

        //JDELog.d("UDE", "is null " + (client == null).toString())
        client?.let {
            syncInteractor = SyncDataInteractor(it, store, preferences)
        } //todo: Throw an error if it's null

        view()?.showLoader(true)

        if (restClient == null) {
            syncInteractor.execute {
                view()?.hideLoader(true)
                it.data?.let {
                    view()?.selectFragment()
                }
                it.error?.let {
                    if (it.message == "400") {
                        view()?.showUnknownError()
                    } else {
                        view()?.selectFragment()
                    }
                }
            }
        } else {
            view()?.hideLoader(true)
        }
        restClient = client
    }

    fun onRoutesSelected() {
        view()?.showRoutesContent()
    }

    fun onCatalogSelected() {
        view()?.showCatalogContent()
    }

    fun onOrdersSelected() {
        view()?.showOrdersContent()
    }

    fun onProfileSelected() {
        view()?.showProfileContent(name, email)
    }
}

interface HomeViewTranslator : IBaseViewTranslator {
    fun showRoutesContent()
    fun showCatalogContent()
    fun showOrdersContent()
    fun showProfileContent(name: String, email: String)
    fun selectFragment()
    fun showLoader(windowsFlagsEnabled: Boolean)
    fun hideLoader(windowsFlagsEnabled: Boolean)
    fun showUnknownError()
}
