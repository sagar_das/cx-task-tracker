package com.commercecx.tasktracker.presenter

import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.base.BasePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import com.commercecx.tasktracker.interactors.GetPendingProjectsInteractor
import com.commercecx.tasktracker.interactors.GetProjectsByNameInteractor
import com.commercecx.tasktracker.interactors.GetProjectsInteractor
import com.commercecx.tasktracker.interactors.ProjectNameFilter
import com.commercecx.tasktracker.model.Project
import java.lang.ref.WeakReference

/**
 * Created by Sagar Das on 11/18/20.
 */

class ProjectsPresenter (
    val view: ProjectsViewTranslator
) : BasePresenter<ProjectsViewTranslator>(WeakReference(view)) {



    fun onViewCreated() {


        view()?.getInstance(this)



        GetProjectsInteractor(BaseApplication.instance.restClient!!, false).execute {


            it.data?.let { data ->

                var projectList = data.toMutableList()

                view()?.displayProjects(projectList)

            }
        }

    }

    fun refreshData(){
        view()?.getInstance(this)



        GetProjectsInteractor(BaseApplication.instance.restClient!!, true).execute {


            it.data?.let { data ->

                var projectList = data.toMutableList()

                view()?.displayProjects(projectList)

            }
        }
    }


    fun searchProjects(query: String?){
        val searchQuery = "%$query%"
        GetProjectsByNameInteractor().execute(ProjectNameFilter(searchQuery)) {
            //view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayProjects(data)
            }
        }
    }

    fun getAllPendingProjects(){
        GetPendingProjectsInteractor().execute() {
            //view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayProjects(data)
            }
        }
    }


}

interface ProjectsViewTranslator : IBaseViewTranslator {

    fun displayProjects(data: List<Project>)
    fun getInstance(projectsPresenter: ProjectsPresenter)

}
