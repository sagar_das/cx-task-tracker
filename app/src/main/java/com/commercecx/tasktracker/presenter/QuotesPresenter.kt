package com.commercecx.tasktracker.presenter

import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.base.BasePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import com.commercecx.tasktracker.interactors.GetQuotesByNameInteractor
import com.commercecx.tasktracker.interactors.GetQuotesInteractor
import com.commercecx.tasktracker.interactors.QuoteNameFilter
import com.commercecx.tasktracker.model.Quote
import java.lang.ref.WeakReference

/**
 * Created by Sagar Das on 11/17/20.
 */
class QuotesPresenter (
    val view: QuotesViewTranslator,
    val quoteId: String?
) : BasePresenter<QuotesViewTranslator>(WeakReference(view)) {



    fun onViewCreated() {


        view()?.getInstance(this)



        GetQuotesInteractor(BaseApplication.instance.restClient!!,quoteId).execute {


            it.data?.let { data ->

                var quoteList = data.toMutableList()

                view()?.displayQuotes(quoteList)

            }
        }

    }


    fun searchQuotes(query: String?){
        val searchQuery = "%$query%"
        GetQuotesByNameInteractor().execute(QuoteNameFilter(searchQuery)) {
            //view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayQuotes(data)
            }
        }
    }




}

interface QuotesViewTranslator : IBaseViewTranslator {

    fun displayQuotes(data: List<Quote>)
    fun getInstance(quotesPresenter: QuotesPresenter)

}
