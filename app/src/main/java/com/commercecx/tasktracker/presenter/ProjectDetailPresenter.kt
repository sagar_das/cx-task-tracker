package com.commercecx.tasktracker.presenter

import com.commercecx.tasktracker.base.BasePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import com.commercecx.tasktracker.interactors.AccountNameFilter
import com.commercecx.tasktracker.interactors.GetAccountsByIDInteractor
import com.commercecx.tasktracker.interactors.GetOpportunitysByIDInteractor
import com.commercecx.tasktracker.interactors.OpportunityNameFilter
import com.commercecx.tasktracker.model.Account
import com.commercecx.tasktracker.model.Opportunity
import java.lang.ref.WeakReference

/**
 * Created by Sagar Das on 12/9/20.
 */
class ProjectDetailPresenter (
    val view: ProjectDetailViewTranslator
) : BasePresenter<ProjectDetailViewTranslator>(WeakReference(view)) {



    fun onViewCreated(accountId: String?, opportunityId: String?) {


        view()?.getInstance(this)



        GetAccountsByIDInteractor().execute(AccountNameFilter(accountId!!)) {


            it.data?.let { data ->

                var accountsList = data.toMutableList()

                view()?.displayAccount(accountsList)

            }
        }

        GetOpportunitysByIDInteractor().execute(OpportunityNameFilter(opportunityId!!)) {


            it.data?.let { data ->

                var opportunityList = data.toMutableList()

                view()?.displayOpportunity(opportunityList)

            }
        }



    }








}

interface ProjectDetailViewTranslator : IBaseViewTranslator {

    fun displayAccount(data: List<Account>)
    fun displayOpportunity(data: List<Opportunity>)
    fun getInstance(projectdetailspresenter: ProjectDetailPresenter)

}