package com.commercecx.tasktracker.presenter

/**
 * Created by Sagar Das on 8/20/20.
 */

import com.commercecx.tasktracker.base.BasePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import java.lang.ref.WeakReference

class LoginPresenter(
    val view: LoginViewTranslator
) : BasePresenter<LoginViewTranslator>(WeakReference(view)) {

    fun onSubmitButtonClick() {
        view()?.openHomeScreen()
    }

    fun onForgotPasswordClick() {
        view()?.openForgotPasswordUrl()
    }
}

interface LoginViewTranslator : IBaseViewTranslator {
    fun openHomeScreen()
    fun openForgotPasswordUrl()
}