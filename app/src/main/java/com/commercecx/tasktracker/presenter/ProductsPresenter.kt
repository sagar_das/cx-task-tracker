package com.commercecx.tasktracker.presenter

import android.os.Bundle
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.base.BasePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import com.commercecx.tasktracker.interactors.*
import com.commercecx.tasktracker.model.Category
import com.commercecx.tasktracker.model.Product

import java.lang.ref.WeakReference

class ProductsPresenter(
    val view: ProductsViewTranslator
) : BasePresenter<ProductsViewTranslator>(WeakReference(view)) {

    var isCatalog = false

    fun onViewCreated(brand: Category?, category: Category?, subCategory: Category?) {

        val orderNonContractedProducts = BaseApplication.instance.myRoute?.orderNonContractedProducts ?: false

        view()?.showLoader()
        view()?.getInstance(this)

        if (isCatalog) {
            GetProductsInteractor().execute(ProductFilter(brand!!, category!!, subCategory!!, orderNonContractedProducts, isCatalog)) {
                view()?.hideLoader()
                it.data?.let { data ->
                    view()?.displayProducts(data)
                }
            }
        } else {
            if (orderNonContractedProducts) {
                GetProductsInteractor().execute(ProductFilter(brand!!, category!!, subCategory!!, orderNonContractedProducts, isCatalog)) {
                    view()?.hideLoader()
                    it.data?.let { data ->
                        view()?.displayProducts(data)
                    }
                }
            } else {
                GetAllProductsInteractor().execute(true) {
                    view()?.hideLoader()

                    it.data?.let { data ->

                        var orderList = data.toMutableList()
                        if (!BaseApplication.instance.accountId.isEmpty() ) {
                            orderList.clear()
                            orderList = data.filterTo(orderList,{ product ->  product.accountID.equals(BaseApplication.instance.accountId)})
                        }
                        view()?.displayProducts(orderList)

                    }
                }
            }
        }


    }

    fun onProductClick(product: Product) {
        view()?.showProductDetailsContent(product)
    }

    fun searchProducts(query: String?){
        val searchQuery = "%$query%"
        GetProductsByNameInteractor().execute(ProductNameFilter(searchQuery)) {
            view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayProducts(data)
            }
        }
    }


}

interface ProductsViewTranslator : IBaseViewTranslator {
    fun initViews()
    fun showProductDetailsContent(product: Product)
    fun displayProducts(data: List<Product>)
    fun showLoader()
    fun hideLoader()
    fun getInstance(productsPresenter: ProductsPresenter)
}
