package com.commercecx.tasktracker.presenter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle

import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.base.BaseSalesForcePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import com.commercecx.tasktracker.interactors.SendOrderInteractor
import com.commercecx.tasktracker.interactors.StoreOrderInteractor
import com.commercecx.tasktracker.model.OperatorType
import com.commercecx.tasktracker.model.OrderSubtype
import com.commercecx.tasktracker.model.PendingOrder
import com.commercecx.tasktracker.util.Logger

import com.salesforce.androidsdk.rest.RestClient
import java.lang.ref.WeakReference

enum class PoNumberRequired {
    MANDATORY, OPTIONAL, NOT_REQUIRED
}

class CheckoutPresenter(
        val view: CheckoutViewTranslator
) : BaseSalesForcePresenter<CheckoutViewTranslator>(WeakReference(view)) {

    private val storeOrderInteractor: StoreOrderInteractor by lazy { StoreOrderInteractor() }
    private lateinit var sendOrderInteractor: SendOrderInteractor
    private var hideExtraInfo: Boolean = false
    private var signatureFilename: String? = null
    private lateinit var poNumberRequired: PoNumberRequired

    override fun onCreate(savedInstanceState: Bundle?, extras: Bundle?) {
        super.onCreate(savedInstanceState, extras)
//        BaseApplication.instance.fromVan = (BaseApplication.instance.userInfo.operatorType == OperatorType.VAN)
        extras?.let {
            hideExtraInfo = false
        }
    }

    override fun onReady() {
        super.onReady()
        configurePoNumber()
        if (hideExtraInfo) {
            view()?.hideExtraInfo()
        }
    }

    override fun onResume(client: RestClient?) {
        client?.let {
            sendOrderInteractor = SendOrderInteractor(it)
        }
    }

    private fun configurePoNumber() {
        with(BaseApplication.instance) {
            if (orderSubtype == OrderSubtype.CREATE_ORDER) {
                if (myRoute!!.poNumberRequired) {
                    if (myRoute!!.poNumber.isBlank()) {
                        poNumberRequired = PoNumberRequired.MANDATORY
                    } else {
                        poNumberRequired = PoNumberRequired.MANDATORY
                        view()?.disablePoNumber()
                        view()?.setPoNumberText(myRoute!!.poNumber)
                    }
                } else {
                    poNumberRequired = PoNumberRequired.OPTIONAL
                }
            } else {
                poNumberRequired = PoNumberRequired.NOT_REQUIRED
                view()?.hidePoNumber()
            }
        }
    }

    fun onVanOrWarehouseClick() {
        view()?.showVanOrWarehouseDialog()
    }

    fun onSignatureClick() {
        view()?.openSignatureScreen()
    }

    fun onOperatorSelected(operator: OperatorType) {
        with(operator.code) {
            BaseApplication.instance.fromVan = (operator == OperatorType.VAN)
            view()?.updateCheckOutOperatorRow(this)
        }
    }

    fun onSignatureResultOk(signatureFilename: String?) {
        signatureFilename?.let {
            processSignatureImage(it)
        } ?: view()?.showSignatureImage(null)
    }

    private fun processSignatureImage(signatureFilename: String) {
        this.signatureFilename = signatureFilename
        try {
            val fileInputStream = view()?.context()?.openFileInput(signatureFilename)
            val bmp = BitmapFactory.decodeStream(fileInputStream)
            view()?.showSignatureImage(bmp)

            fileInputStream?.close()
        } catch (e: Exception) {
            Logger.error("Error in processSignatureImage" + e.message)
        }
    }

//    private fun getSignatureImageBase64(): String? {
//        var encoded: String? = null
//        try {
//            val fileInputStream = view()?.context()?.openFileInput(signatureFilename)
//            val signatureBitmap = BitmapFactory.decodeStream(fileInputStream)
//            val byteArrayOutputStream = ByteArrayOutputStream()
//            signatureBitmap?.compress(Bitmap.CompressFormat.JPEG, ProcessSignatureImageInteractor.SIGNATURE_FILE_QUALITY, byteArrayOutputStream)
//            encoded = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
//            byteArrayOutputStream.close()
//        } catch (e: Exception) {
//            JDELog.e("Error in processSignatureImage " + e.message)
//        }
//        return encoded
//    }

    fun onBackButtonClick() {
        view()?.finish()
    }

    // Add delivery date on this
    fun placeOrder(poNumber: String, comments: String) {
        if (isValidPoNumber(poNumber)) {
            with(BaseApplication.instance) {
                view()?.showLoader()
                removeProductsWithZeroQuantity()
                storeOrderInteractor.execute(PendingOrder.create(myRoute!!, userInfo, deliveryDate, orderList, orderSubtype!!, fromVan, poNumber, comments, "")) {
                    //view()?.openRouteDetailsScreen()
                    view()?.hideLoader()
                }
                BaseApplication.instance.myRoute?.updateVisited(System.currentTimeMillis())
            }
        }
    }

    private fun isValidPoNumber(poNumber: String): Boolean {
        if (poNumberRequired == PoNumberRequired.MANDATORY) {
            if (poNumber.isEmpty()) {
                view()?.showPoNumberEmptyError()
                return false
            }
        }
        return true
    }

    private fun removeProductsWithZeroQuantity() {
        BaseApplication.instance.orderList.removeAll { it.productquantity == null }
        BaseApplication.instance.orderList.removeAll { it.productquantity == 0 }
    }
}

interface CheckoutViewTranslator : IBaseViewTranslator {
    fun finish()
    fun hideExtraInfo()
    fun showVanOrWarehouseDialog()
    fun disablePoNumber()
    fun hidePoNumber()
    fun setPoNumberText(poNumber: String)
    fun showPoNumberEmptyError()
    fun openSignatureScreen()
    fun updateCheckOutOperatorRow(workOrderType: String)
    fun showSignatureImage(signatureBitmap: Bitmap?)
    fun openRouteDetailsScreen()
    fun showLoader()
    fun hideLoader()
    fun showDatePicker()
    fun setDate(year: Int, month: Int, day: Int, date : Long)
}
