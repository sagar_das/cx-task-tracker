package com.commercecx.tasktracker.presenter

import com.commercecx.tasktracker.base.BasePresenter
import com.commercecx.tasktracker.base.IBaseViewTranslator
import com.commercecx.tasktracker.interactors.*
import com.commercecx.tasktracker.model.Account
import java.lang.ref.WeakReference

/**
 * Created by Sagar Das on 8/25/20.
 */
class AccountsPresenter (
    val view: AccountsViewTranslator
) : BasePresenter<AccountsViewTranslator>(WeakReference(view)) {



    fun onViewCreated() {


        view()?.getInstance(this)



        GetAccountsInteractor().execute {


                    it.data?.let { data ->

                        var accountList = data.toMutableList()

                        view()?.displayAccounts(accountList)

                    }
                }

        }


    fun searchAccounts(query: String?){
        val searchQuery = "%$query%"
        GetAccountsByIDInteractor().execute(AccountNameFilter(searchQuery)) {
            //view()?.hideLoader()
            it.data?.let { data ->
                view()?.displayAccounts(data)
            }
        }
    }




}

interface AccountsViewTranslator : IBaseViewTranslator {

    fun displayAccounts(data: List<Account>)
    fun getInstance(accountsPresenter: AccountsPresenter)

}
