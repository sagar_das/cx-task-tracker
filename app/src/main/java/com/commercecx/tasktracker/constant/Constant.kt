package com.commercecx.tasktracker.constant

/**
 * Created by Sagar Das on 8/19/20.
 */
internal class Constant private constructor() {
    interface RequestCode {
        companion object {
            const val CHECK_SETTINGS = 0x0105
        }
    }

    interface BundleKeys {
        companion object {
            const val BARCODE_KEY = "BARCODE_KEY"
            const val NOTE_KEY = "NOTE_KEY"
        }
    }

    interface NetworkTypes {
        companion object {
            const val MOBILE_DATA = "MOBILEDATA"
        }
    }
}