package com.commercecx.tasktracker.service

import android.app.job.JobParameters
import android.app.job.JobService
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.interactors.GetProjectsByIdInteractor
import com.commercecx.tasktracker.interactors.ProjectNameFilter
import com.commercecx.tasktracker.interactors.ProjectsInteractor
import com.commercecx.tasktracker.model.Note
import com.commercecx.tasktracker.model.Project
import com.commercecx.tasktracker.util.NetworkUtil
import io.realm.Realm


class ProjectJobService : JobService() {
    private var noteList: List<Note?>? = null
    private var jobParameters: JobParameters? = null
    var realm: Realm? = null
    override fun onStartJob(jobParameters: JobParameters): Boolean {
        this.jobParameters = jobParameters
        realm = Realm.getDefaultInstance()
        noteList = realm!!.where(Note::class.java).equalTo("isServerCommitted", false).findAll()


        //  new FetchNotesAsync().execute();
        val networkUtil = NetworkUtil(applicationContext)

        //  new FetchNotesAsync().execute();

//        NetworkUtil networkUtil = new NetworkUtil(getApplicationContext());
//        NetworkOperations operations = new NetworkOperations(getApplicationContext());
        for (note in noteList!!) {

            if(note!!.getName() == "project"){

                GetProjectsByIdInteractor().execute(ProjectNameFilter(note.getId()!!)) {
                    //view()?.hideLoader()
                    it.data?.let { data ->

                        var projects : List<Project> = data
                        var project: Project = projects[0]

                        if(networkUtil.isConnected){


                            val projectData = "{" +
                                    " \"projectid\" : " + "\"" + project.id + "\"," +
                                    " \"name\" : " + "\"" + project.projectName  + "\"," +
                                    " \"budgetstatus\" : " + "\"" + project.projectstatus + "\"," +
                                    " \"budgetamount\" : " + "\"" + project.budgetamount + "\"," +
                                    " \"startdate\" : " + "\"" + project.startDate + "\"" +
                                    "}"

                            val projectsInteractor = ProjectsInteractor(BaseApplication.instance.restClient!!, projectData)
                            projectsInteractor.syncCall()

                          }

                    }
                }

                var updatedNote = Note()
                updatedNote.setServerCommitted(true)
                updatedNote.setId(note.getId())
                updatedNote.setName(note.getName())
               // note.setServerCommitted(true)
                realm!!.executeTransactionAsync { realm -> realm.insertOrUpdate(updatedNote) }


            }


        }
        jobFinished(jobParameters, false)
        return true
    }

    override fun onStopJob(jobParameters: JobParameters): Boolean {
        return false
    }



    companion object {
        private val TAG = ProjectJobService::class.java.simpleName
    }
}

