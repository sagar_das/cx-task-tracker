package com.commercecx.tasktracker.service.syncv

import android.app.IntentService
import android.content.Intent
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.model.Status

import com.commercecx.tasktracker.interactors.SendCustomerSignatureInteractor
import com.commercecx.tasktracker.interactors.SendOrderInteractor
import com.commercecx.tasktracker.model.CustomerSignature

import com.commercecx.tasktracker.model.database.DBOrdersHelper

import com.salesforce.androidsdk.smartstore.app.SmartStoreSDKManager
import org.json.JSONArray
import org.json.JSONObject


class OrdersSyncService : IntentService("OrderSyncService") {
    companion object {
        const val TAG = "OrdersSyncService"
    }

    override fun onHandleIntent(intent: Intent?) {
        val dbHelper = DBOrdersHelper()
        val orderList = dbHelper.getAllPendingOrders()

//        JDELog.d(TAG, "BG Service started")
//        JDELog.d(TAG, "To proceed with ${orderList.size} orders ")
        orderList.forEach { pendingOrder ->
            when (pendingOrder.status) {
                Status.PENDING_ORDER -> {
                    //JDELog.d(TAG, "Begin with order ${pendingOrder.index}")
                    getClient()?.let { restClient ->
                        SendOrderInteractor(restClient).execute(getOrderJson(pendingOrder.jsonOrder)) {
                            if (it.error == null) {

                                pendingOrder.status = Status.PENDING_SIGNATURE
                                dbHelper.insertOrder(pendingOrder)
                                //JDELog.d(TAG, "${pendingOrder.index} is now waiting for signature to be sent")

                                SendCustomerSignatureInteractor(restClient, getStore()).execute(
                                    CustomerSignature.Mapper.toJson(getCustomerSignature(pendingOrder.index.toString(), pendingOrder.signature))
                                )
                                {
                                    if (it.error == null) {
                                        pendingOrder.status = Status.SUBMITTED
                                        dbHelper.insertOrder(pendingOrder)
                                        //JDELog.d(TAG, "${pendingOrder.index} is now submitted")
                                    }
                                }
                            } else {
                                //JDELog.e(TAG, "error " + it.error.message)
                            }
                        }
                    }

                }

                Status.PENDING_SIGNATURE -> {
                    getClient()?.let {
                        SendCustomerSignatureInteractor(it, getStore()).execute(
                            CustomerSignature.Mapper.toJson(getCustomerSignature(pendingOrder.index.toString(), pendingOrder.signature))
                        )
                        {
                            if (it.error == null) {
                                pendingOrder.status = Status.SUBMITTED
                                dbHelper.insertOrder(pendingOrder)
                                //JDELog.d(TAG, "${pendingOrder.index} is now submitted")
                            }
                        }
                    }
                }

                Status.SUBMITTED -> Unit
            }
        }
    }


    fun getStore() = SmartStoreSDKManager.getInstance().getGlobalSmartStore("CX")

    fun getClient() = BaseApplication.instance.restClient


    private fun getCustomerSignature(orderNumber: String, encodedSignature: String): CustomerSignature {
        return CustomerSignature(BaseApplication.instance.userInfo.email, orderNumber, "$orderNumber.JPEG", encodedSignature)
    }

    private fun getOrderJson(jsonInput: JSONObject): JSONObject {
        val result = JSONObject()
        val orders = JSONArray()

        orders.put(jsonInput)

        result.put("Orders", orders)
        result.put("RSOEmployeeEmail", BaseApplication.instance.restClient?.clientInfo?.email)
        result.put("Source", "LMX")

        return result
    }


}