package com.commercecx.tasktracker.service

import android.app.*
import android.app.job.JobParameters
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.core.app.NotificationCompat
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.activity.MainNavActivity
import com.commercecx.tasktracker.model.OrderOld
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class OrderNotificationService : IntentService("OrderNotificationService") {
    private val jobParameters: JobParameters? = null
    private var notificationManager: NotificationManager? = null
    override fun onHandleIntent(@Nullable intent: Intent?) {
        showNotification(intent!!.getBundleExtra("ORDER_BUNDLE"))
    }

    private fun showNotification(bundle: Bundle) {
        val order = bundle.getSerializable("ORDER_SER") as OrderOld?
        val date = Date()
        val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
        val strDate = dateFormat.format(date)
        val intent = Intent(this, MainNavActivity::class.java)
        intent.action = ACTION_ORDER_REM
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.iqvia)
            .setContentTitle("Order recurrence reminder for " + order!!.getName())
            .setContentText("Time to reorder " + order.getName())
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText("Time to reorder " + order.getName())
            )
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
        notificationManager = getSystemService(NotificationManager::class.java)
        createNotificationChannel()
        val notification: Notification = mBuilder.build()
        notificationManager!!.notify(ORDER_REM_NOTIF_ID, notification)
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "Order_Reminder"
            val description = "Order description"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    companion object {
        private val TAG = OrderNotificationService::class.java.simpleName
        private const val CHANNEL_ID = "LMX_ORDER_CHANNEL"
        private const val ACTION_ORDER_REM = "com.org.jde.ACTION_ORDER_REMINDER"
        private const val ORDER_REM_NOTIF_ID = 103
    }
}
