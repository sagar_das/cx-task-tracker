package com.commercecx.tasktracker.util.spreadsheet

import com.commercecx.tasktracker.base.spreadsheet.Cell

/**
 * Created by Sagar Das on 11/25/20.
 */
class ProjectCell(string: String) : Cell() {

    init {
        cellValue = string
    }


}