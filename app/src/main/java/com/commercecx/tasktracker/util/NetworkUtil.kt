package com.commercecx.tasktracker.util

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo


class NetworkUtil(context: Context) {
    //region Variable Declaration
    private val connectivityManager: ConnectivityManager
    private val networkInfo: NetworkInfo?
    private val account: Account? = null
    val isConnected: Boolean
        get() = networkInfo != null &&
                networkInfo.isConnectedOrConnecting
    val networkType: String
        get() {
            return when (networkInfo!!.type) {
                ConnectivityManager.TYPE_WIFI -> "WIFI"
                ConnectivityManager.TYPE_MOBILE -> "MOBILEDATA"
                ConnectivityManager.TYPE_ETHERNET -> "ETHERNET"
                ConnectivityManager.TYPE_WIMAX -> "WIMAX"
                else -> "Not connected"
            }
        }

    fun CreateSyncAccount(context: Context): Account {
        val newAccount = Account(ACCOUNT, ACCOUNT_TYPE)
        val accountManager = context.getSystemService(Context.ACCOUNT_SERVICE) as AccountManager
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            //success
        } else {
            //fail
        }
        return newAccount
    }

    companion object {
        private const val ACCOUNT_TYPE = "login.salesforce.com"

        //dummy account
        private const val ACCOUNT = "sampleaccount"
    }

    //endregion
    init {
        connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        networkInfo = connectivityManager.activeNetworkInfo
    }
}
