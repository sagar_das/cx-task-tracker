package com.commercecx.tasktracker.util

import android.util.Log

/**
 * Created by Sagar Das on 8/19/20.
 */
object Logger {
    /**
     * Flag to hold all log enable status.
     */
    private val IS_LOG_ENABLED: Boolean = com.commercecx.tasktracker.BuildConfig.ENABLE_LOGS
    private const val LOG_TAG = "JDE_LOG"
    fun verbose(tag: String?, message: String?) {
        if (IS_LOG_ENABLED) {
            Log.v(tag, message)
        }
    }

    fun debug(message: String?) {
        if (IS_LOG_ENABLED) {
            Log.d(LOG_TAG, message)
        }
    }

    fun debug(tag: String?, message: String?) {
        if (IS_LOG_ENABLED) {
            Log.d(tag, message)
        }
    }

    fun info(tag: String?, message: String?) {
        if (IS_LOG_ENABLED) {
            Log.i(tag, message)
        }
    }

    fun info(message: String?) {
        if (IS_LOG_ENABLED) {
            Log.i(LOG_TAG, message)
        }
    }

    fun warn(message: String?) {
        if (IS_LOG_ENABLED) {
            Log.w(LOG_TAG, message)
        }
    }

    fun warn(tag: String?, message: String?) {
        if (IS_LOG_ENABLED) {
            Log.w(tag, message)
        }
    }

    fun error(tag: String?, message: String?) {
        if (IS_LOG_ENABLED) {
            Log.e(tag, message)
        }
    }

    fun error(tag: String?, exception: Exception?) {
        if (IS_LOG_ENABLED) {
            val message = Log.getStackTraceString(exception)
            Log.e(tag, message)
        }
    }

    fun error(exception: Exception?) {
        if (IS_LOG_ENABLED) {
            val message = Log.getStackTraceString(exception)
            Log.e(LOG_TAG, message)
        }
    }

    fun error(error: String?) {
        if (IS_LOG_ENABLED) {
            Log.e(LOG_TAG, error)
        }
    }
}
