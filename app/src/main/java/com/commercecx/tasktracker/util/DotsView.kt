package com.commercecx.tasktracker.util

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.commercecx.tasktracker.R


class DotsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = -1
) :
    View(context, attrs, defStyleAttr) {
    var radius = 0f
        protected set
    var strokeWidth = DEFAULT_STROKE_WIDTH
        protected set
    protected var paint: Paint
    private var color = DEFAULT_COLOR
    protected var filled = true
    fun setRadius(radius: Int) {
        this.radius = radius.toFloat()
        invalidate()
    }

    fun setColor(color: Int) {
        this.color = color
        paint.color = color
        invalidate()
    }

    fun setFilledVal(filled: Boolean) {
        this.filled = filled
        paint.style = if (this.filled) Paint.Style.FILL else Paint.Style.STROKE
    }

    fun isFilled(): Boolean {
        return filled
    }

    fun getColor(): Int {
        return color
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec))
    }

    /**
     * Measures height according to the passed measure spec
     *
     * @param measureSpec int measure spec to use
     * @return int pixel size
     */
    protected fun measureHeight(measureSpec: Int): Int {
        val specMode = MeasureSpec.getMode(measureSpec)
        val specSize = MeasureSpec.getSize(measureSpec)
        var result: Int
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize
        } else {
            result = (2 * radius).toInt() + paddingTop + paddingBottom + (2 * strokeWidth).toInt()
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize)
            }
        }
        return result
    }

    /**
     * Measures width according to the passed measure spec
     *
     * @param measureSpec int measure spec to use
     * @return int pixel size
     */
    protected fun measureWidth(measureSpec: Int): Int {
        val specMode = MeasureSpec.getMode(measureSpec)
        val specSize = MeasureSpec.getSize(measureSpec)
        var result: Int
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize
        } else {
            result = (2 * radius).toInt() + paddingLeft + paddingRight + (2 * strokeWidth).toInt()
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize)
            }
        }
        return result
    }

    public override fun onDraw(canvas: Canvas) {
        val x = width shr 1
        val y = height shr 1
        canvas.drawCircle(x.toFloat(), y.toFloat(), radius - strokeWidth, paint)
        super.onDraw(canvas)
    }

    companion object {
        private val TAG = DotsView::class.java.simpleName
        private const val DEFAULT_STROKE_WIDTH = 3.0f
        private const val DEFAULT_RADIUS = 20
        private val DEFAULT_COLOR = Color.parseColor("#FF0000")
    }

    init {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.DotsView)
            try {
                radius = a.getDimensionPixelSize(
                    R.styleable.DotsView_cv___radius, DEFAULT_RADIUS
                ).toFloat()
                color = a.getInt(
                    R.styleable.DotsView_cv___color, DEFAULT_COLOR
                )
                filled = a.getBoolean(
                    R.styleable.DotsView_cv___filled, true
                )
            } finally {
                a.recycle()
            }
        }
        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.strokeWidth = strokeWidth
        paint.color = color
        if (filled) {
            paint.style = Paint.Style.FILL
        } else {
            paint.style = Paint.Style.STROKE
        }
    }
}
