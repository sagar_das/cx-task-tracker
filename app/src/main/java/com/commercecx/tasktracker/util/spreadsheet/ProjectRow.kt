package com.commercecx.tasktracker.util.spreadsheet

import com.commercecx.tasktracker.base.spreadsheet.Row

/**
 * Created by Sagar Das on 11/25/20.
 */
class ProjectRow(line: String) : Row() {

    init {

        var thisString = ""
        for (i in 0 until line.length) {
            val c = line[i]
            if (c == ',') {
                val ProjectCell = ProjectCell(thisString)
                cellList.add(ProjectCell)

                thisString = ""
            } else {
                thisString = thisString + c
            }
        }

        // last one
        // TODO: Deal with last column with trailing ^M and such
        if (thisString.length > 0) {
            val ProjectCell = ProjectCell(thisString)
            cellList.add(ProjectCell)
        }

    }
}