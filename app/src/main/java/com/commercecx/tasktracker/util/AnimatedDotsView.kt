package com.commercecx.tasktracker.util

/**
 * Created by Sagar Das on 8/19/20.
 */
import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.Interpolator
import android.widget.LinearLayout
import com.commercecx.tasktracker.R


class AnimatedDotsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = -1
) :
    LinearLayout(context, attrs, defStyleAttr) {
    protected lateinit var dotViews: Array<DotsView?>
    protected var animatorSet: AnimatorSet? = null
    var isStop = false
        protected set
    var dotCount = 0
        protected set
    var blinkingColor = DEFAULT_BLINKING_COLOR
        protected set
    var neutralColor = DEFAULT_NEUTRAL_COLOR
        protected set
    var dotRadius = DEFAULT_DOT_RADIUS
        protected set

    private fun prepareAnimators(): AnimatorSet {
        val animators = arrayOfNulls<ObjectAnimator>(dotCount)
        var half = dotCount shr 1
        var d = DURATION_DIFF
        /**
         * Assign duration increasing -> decreasing
         *
         *
         * N even:
         * [d][d + 1]...[d + 1][d]
         *
         * N odd:
         * [d][d + 1][d + 2][d + 1][d]
         *
         */
        /** Walk forward with increasing duration   */
        for (i in 0 until half) {
            animators[i] = createAnimator(dotViews[i], d)
            d += DURATION_DIFF
        }
        /** If dot count is odd, the middle dot has the longest duration  */
        if (dotCount % 2 == 1) {
            animators[half] = createAnimator(dotViews[half], d)
            half++
        }
        /** Walk backward with decreasing duration  */
        for (i in half until dotCount) {
            d -= DURATION_DIFF
            animators[i] = createAnimator(dotViews[i], d)
        }
        val animatorSet = AnimatorSet()
        animatorSet.playSequentially(*animators)
        animatorSet.interpolator = AccelerateInterpolator(2.0f)
        animatorSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                if (!isStop) {
                    animatorSet.start()
                }
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })
        return animatorSet
    }

    private fun addCircleViews() {
        dotViews = arrayOfNulls<DotsView>(dotCount)
        val context = context
        for (i in 0 until dotCount) {
            Log.e(TAG, "add view: $i")
            dotViews[i] = DotsView(context)
            dotViews[i]!!.setRadius(dotRadius)
            dotViews[i]!!.setColor(neutralColor)
            addView(
                dotViews[i],
                0,
                LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            )
        }
        animatorSet = prepareAnimators()
    }

    fun startAnimation() {
        isStop = false
        animatorSet!!.start()
    }

    fun createAnimator(v: DotsView?, duration: Long): ObjectAnimator {
        val animator: ObjectAnimator = ObjectAnimator.ofObject(
            v,
            "color",
            ArgbEvaluator(),
            neutralColor,
            blinkingColor
        )
        animator.duration = duration
        animator.repeatCount = 1
        animator.interpolator = DOT_INTERPOLATOR
        animator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                v!!.setColor(neutralColor)
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })
        return animator
    }

    fun stopAnimation() {
        isStop = true
        animatorSet!!.end()
    }

    companion object {
        private val TAG = AnimatedDotsView::class.java.simpleName
        private val DEFAULT_NEUTRAL_COLOR = Color.parseColor("#777777")
        private val DEFAULT_BLINKING_COLOR = Color.parseColor("#FF00FF00")
        private const val DEFAULT_DOT_COUNT = 5
        private const val DEFAULT_DOT_RADIUS = 20
        private const val DURATION_DIFF = 100L
        private val DOT_INTERPOLATOR: Interpolator = AccelerateInterpolator(2.0f)
    }

    init {
        inflate(context, R.layout.v_animated_dots, this)
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.AnimatedDotsView)
            try {
                dotRadius = a.getDimensionPixelSize(
                    R.styleable.AnimatedDotsView_adv___dotRadius, DEFAULT_DOT_RADIUS
                )
                dotCount = a.getInt(
                    R.styleable.AnimatedDotsView_adv___dotCount, DEFAULT_DOT_COUNT
                )
                blinkingColor = a.getColor(
                    R.styleable.AnimatedDotsView_adv___dotBlinkingColor, DEFAULT_BLINKING_COLOR
                )
                neutralColor = a.getColor(
                    R.styleable.AnimatedDotsView_adv___dotNeutralColor, DEFAULT_NEUTRAL_COLOR
                )
            } finally {
                a.recycle()
            }
        }
        orientation = HORIZONTAL
        require(!(dotCount < 1 || dotCount > 10)) { "The number of dot should be between [1, 10]" }
        addCircleViews()
    }
}
