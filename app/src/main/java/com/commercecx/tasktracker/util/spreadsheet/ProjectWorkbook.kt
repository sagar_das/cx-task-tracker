package com.commercecx.tasktracker.util.spreadsheet

import com.commercecx.tasktracker.base.spreadsheet.Workbook
import java.io.InputStream

/**
 * Created by Sagar Das on 11/25/20.
 */
class ProjectWorkbook (inputStream: InputStream): Workbook() {


    init {
        var ProjectSheet : ProjectSheet

        ProjectSheet = ProjectSheet(inputStream)

        if (sheetList.size > 1) {
            sheetList.clear()
        }
        sheetList.set(0, ProjectSheet)
    }

}