package com.commercecx.tasktracker.util.spreadsheet

import com.commercecx.tasktracker.base.spreadsheet.Workbook
import java.io.InputStream

/**
 * Created by Sagar Das on 11/25/20.
 */

class Spreadsheet {

    var workbook : Workbook =
        Workbook()

    // empty spreadsheet
    constructor() {
        workbook = Workbook()
    }

    // spreadsheetFormat from string to other
    constructor(inputStream: InputStream, spreadsheetFormat: String?) {

        workbook = ProjectWorkbook(inputStream)


    }


}