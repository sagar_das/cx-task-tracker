package com.commercecx.tasktracker.util.extensions

/**
 * Created by Sagar Das on 8/20/20.
 */

import android.view.View

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}