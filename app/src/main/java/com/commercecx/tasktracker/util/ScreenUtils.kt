package com.commercecx.tasktracker.util

/**
 * Created by Sagar Das on 8/20/20.
 */
import android.graphics.Point
import android.view.Display

object ScreenUtils {

    fun getDisplaySize(display: Display): Point {
        val size = Point()
        display.getSize(size)
        return size
    }

    fun getImageSizeBasedOnDisplay(display: Display, widthRatio: Float = 1F, heightRatio: Float = 1F): Point {
        return with(getDisplaySize(display)) {
            val imageWidth = (this.x * widthRatio).toInt()
            Point(imageWidth, (imageWidth * heightRatio).toInt())
        }
    }

}