package com.commercecx.tasktracker.util

/**
 * Created by Sagar Das on 8/20/20.
 */
import android.text.Editable
import android.text.TextWatcher

class BasicFormattingTextWatcher(private val listener: (text: String) -> Unit) : TextWatcher {

    override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(s: Editable) {
        listener(s.toString())
    }
}