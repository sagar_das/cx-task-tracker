package com.commercecx.tasktracker.util

import android.widget.Toast
import com.commercecx.tasktracker.application.BaseApplication
import com.salesforce.androidsdk.push.PushNotificationInterface

/**
 * Created by Sagar Das on 12/18/20.
 */
class SalesforceNotification : PushNotificationInterface {
    override fun onPushMessageReceived(data: MutableMap<String, String>?) {
        Toast.makeText(BaseApplication.instance.applicationContext, "Notification received", Toast.LENGTH_SHORT).show()
    }
}