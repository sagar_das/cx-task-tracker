package com.commercecx.tasktracker.util

/**
 * Created by Sagar Das on 11/25/20.
 */

enum class EXECUTION {
    RUNNING,
    SUCCESSACK,
    SUCCESSNAK,
    FAILED
}

@Suppress("DataClassPrivateConstructor")
data class ExecutionState private constructor(
    val execution: EXECUTION,
    val msg: String? = null) {
    companion object {
        val LOADED_ACK = ExecutionState(EXECUTION.SUCCESSACK)
        val LOADED_NAK = ExecutionState(EXECUTION.SUCCESSNAK)
        val LOADING = ExecutionState(EXECUTION.RUNNING)
        fun error(msg: String?) = ExecutionState(EXECUTION.FAILED, msg)
    }
}