package com.commercecx.tasktracker.util

/**
 * Created by Sagar Das on 8/24/20.
 */
class Utils {

    companion object{
        fun fetchUnixEpoch(): String{

            return System.currentTimeMillis().toString()
        }

        const val CATALOG_GRID_COLUMN_SIZE = 400
    }
}