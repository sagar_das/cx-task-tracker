package com.commercecx.tasktracker.util.extensions

/**
 * Created by Sagar Das on 8/20/20.
 */

import android.content.res.Resources

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()