package com.commercecx.tasktracker.util.extensions

/**
 * Created by Sagar Das on 8/20/20.
 */

import org.json.JSONObject

/**
 * Extension to JSON.optString. If no key found or if it returns null then it'll return ""
 */

fun JSONObject.stringFromKey(key: String) =
    if(this.has(key)){
        this.getString(key)
    }
else{
        ""
    }
//    if (this.optString(key, "").equals("null", true)) {
//        ""
//    } else {
//        this.getString(key)
//
//    }