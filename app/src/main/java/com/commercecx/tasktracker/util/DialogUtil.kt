package com.commercecx.tasktracker.util


import android.app.Activity
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.provider.Settings
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import java.util.*
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.constant.Constant


/**
 * Created by Sagar Das on 8/19/20.
 */
object DialogUtil {
    private val TAG = DialogUtil::class.java.simpleName
    private var progressDialog: ProgressDialog? = null
    @JvmOverloads
    fun showProgressDialog(@NonNull activity: Activity?, message: String? = null): ProgressDialog? {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(activity, R.style.StyleProgressDialog)
            progressDialog!!.setCanceledOnTouchOutside(false)
            progressDialog!!.setCancelable(false)
            if (message != null) {
                progressDialog!!.setMessage(message)
            }
            progressDialog!!.setOnCancelListener { progressDialog = null }
            try {
                progressDialog!!.show()
            } catch (e: Exception) {
                Logger.error(TAG, e)
            }
        }
        return progressDialog
    }

    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            try {
                progressDialog!!.dismiss()
            } catch (e: Exception) {
                Logger.error(TAG, e)
            }
        }
        progressDialog = null
    }

    fun showAlertDialog(activity: Activity, message: String?, cancelable: Boolean) {
        showAlertDialog(activity, activity.getString(R.string.app_name), message, cancelable)
    }

    fun showAlertDialog(activity: Activity, title: String?, message: String?, cancelable: Boolean) {
        showAlertDialog(
            activity,
            title,
            message,
            activity.getString(android.R.string.ok),
            null,
            null,
            null,
            null,
            cancelable
        )
    }

    fun showDeleteConfirmationDialog(
        @NonNull activity: Activity,
        itemName: String?,
        positiveButtonClickListener: DialogInterface.OnClickListener?
    ) {
        showAlertDialog(
            activity, activity.getString(R.string.alert_title_delete_confirmation), String.format(
                Locale.ENGLISH, activity
                    .getString(R.string.alert_message_delete_confirmation), itemName
            ), "YES", positiveButtonClickListener, null,
            "NO", null, false
        )
    }

    fun showAlertDialog(
        activity: Activity?,
        title: String?,
        message: String?,
        cancelable: Boolean,
        positiveButtonText: String?,
        positiveButtonClickListener: DialogInterface.OnClickListener?
    ) {
        showAlertDialog(
            activity,
            title,
            message,
            positiveButtonText,
            positiveButtonClickListener,
            null,
            null,
            null,
            cancelable
        )
    }

    fun showAlertDialog(
        activity: Activity,
        title: String?,
        message: String?,
        cancelable: Boolean,
        positiveButtonClickListener: DialogInterface.OnClickListener?
    ) {
        showAlertDialog(
            activity,
            title,
            message,
            activity.getString(android.R.string.ok),
            positiveButtonClickListener,
            null,
            null,
            null,
            cancelable
        )
    }

    fun showAlertDialog(
        activity: Activity?,
        title: String?,
        message: String?,
        positiveButtonText: String?,
        positiveButtonClickListener: DialogInterface.OnClickListener?,
        positiveButtonColor: Int?,
        negativeButtonText: String?,
        negativeButtonClickListener: DialogInterface.OnClickListener?,
        cancelable: Boolean
    ) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
        if (negativeButtonText != null) {
            builder.setNegativeButton(negativeButtonText, negativeButtonClickListener)
        }
        if (title != null) {
            builder.setTitle(title)
        }
        val alertDialog: AlertDialog = builder.setMessage(message)
            .setPositiveButton(positiveButtonText, positiveButtonClickListener)
            .setCancelable(cancelable).create()
        if (positiveButtonColor != null) {
            alertDialog.setOnShowListener(OnShowListener {
                alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setTextColor(positiveButtonColor)
            })
        }
        try {
            alertDialog.show()
        } catch (e: Exception) {
            Logger.error(TAG, e)
        }
    }

    @JvmOverloads
    fun showSettingsDialog(
        @NonNull activity: Activity,
        @NonNull message: String? = activity.getString(R.string.message_enable_all_permissions)
    ) {
        val alertDialog: AlertDialog = AlertDialog.Builder(activity, R.style.AlertDialogTheme).setTitle(
            "Required permissions " + ""
                    + "are disabled"
        ).setMessage(message).setPositiveButton("OPEN SETTINGS",
            DialogInterface.OnClickListener { dialog, whichButton ->
                val i = Intent()
                i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                i.data = Uri.parse("package:" + activity.packageName)
                activity.startActivityForResult(i, Constant.RequestCode.CHECK_SETTINGS)
            }).setCancelable(false).create()
        alertDialog.setOnShowListener(OnShowListener {
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(
                Color.RED
            )
        })
        alertDialog.show()
    }
}
