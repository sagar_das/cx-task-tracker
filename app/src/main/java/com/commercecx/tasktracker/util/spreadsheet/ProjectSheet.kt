package com.commercecx.tasktracker.util.spreadsheet

import com.commercecx.tasktracker.base.spreadsheet.Sheet
import java.io.InputStream

/**
 * Created by Sagar Das on 11/25/20.
 */
class ProjectSheet(inputStream: InputStream) : Sheet() {

    companion object {
        const val END_OF_STREAM = -1
    }

    init {

        var c: Char
        try {
            var lineSoFar = ""
            var nextByte = inputStream.read()
            while (nextByte != END_OF_STREAM) {
                c = nextByte.toChar()
                if (c == '\n') {
                    val ProjectRow = ProjectRow(lineSoFar)
                    rowList.add(ProjectRow)
                    lineSoFar = ""
                } else {
                    lineSoFar = lineSoFar + c
                }
                nextByte = inputStream.read()
            }
            inputStream.close()
        } catch (e: Exception) {

        }
    }
}
