package com.commercecx.tasktracker.activity

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import androidx.preference.PreferenceManager

import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.base.BaseActivity
import com.commercecx.tasktracker.util.DialogUtil


class SplashActivity : BaseActivity(), Runnable {
    private var mErrorMessage: String? = null
    private val mHandler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                SUCCESS -> {
                    //                    final User user = PreferenceManager.getUser(SplashActivity.this);
                    val sp = PreferenceManager.getDefaultSharedPreferences(applicationContext)
                    val isLoggedIn = sp.getBoolean("prefLogin", false)
                    //                    if (user != null && !user.getId().isEmpty()) {
                    if (isLoggedIn) {
                        val intent = Intent(this@SplashActivity, MainNavActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    } else {
                        val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()


                    }
                }
                FAILURE -> DialogUtil.showAlertDialog(this@SplashActivity,
                    getString(R.string.app_name),
                    mErrorMessage,
                    false,
                    DialogInterface.OnClickListener { dialogInterface, i -> finish() })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val launcherThread = Thread(this)
        launcherThread.start()
    }

    override fun run() {
        mErrorMessage = null
        mHandler.sendEmptyMessageDelayed(SUCCESS, 1000)
    }

    companion object {
        private const val SUCCESS = 1
        private const val FAILURE = 0
    }
}
