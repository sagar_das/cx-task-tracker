package com.commercecx.tasktracker.activity

import android.os.Bundle

import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.preference.PreferenceManager
import butterknife.ButterKnife
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.base.BaseActivity
import kotlinx.android.synthetic.main.activity_signin.*


class SignInActivity : BaseActivity(), View.OnClickListener {
    //region Variable declaration
//    @BindView(R.id.signInBTN)
//    var signUp: Button? = null
//
//    //    @BindView(R.id.userNameET)
//    //    EditText userNameET;
//    //    @BindView(R.id.passwordET)
//    //    EditText passwordET;
//    @BindView(R.id.userNameET)
//    var userNameET: TextInputEditText? = null
//
//    @BindView(R.id.lastNameET)
//    var lastNameET: TextInputEditText? = null
//
//    @BindView(R.id.emailET)
//    var emailET: TextInputEditText? = null
    private val loginEnabled = false

    //endregion
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        ButterKnife.bind(this)

        signInBTN.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.signInBTN -> {
                if (userNameET!!.text.toString().equals("", true) || lastNameET!!.text
                        .toString().equals("",true)
                    || emailET!!.text.toString().equals("", true)
                ) {
                    Toast.makeText(this, "Please enter all the details", Toast.LENGTH_LONG).show()
                    return
                }
                if (!isEmailValid(emailET!!.text.toString())) {
                    Toast.makeText(this, "Please enter a valid email address", Toast.LENGTH_LONG)
                        .show()
                    return
                }
                val sp = PreferenceManager.getDefaultSharedPreferences(this)
                val spe = sp.edit()
                spe.putString("prefFirstName", userNameET!!.text.toString())
                spe.putString("prefLastName", lastNameET!!.text.toString())
                spe.putString("prefEmailId", emailET!!.text.toString())
                spe.putBoolean("prefLogin", true)
                spe.commit()
                spe.clear()
                launchActivity(MainNavActivity::class.java)
            }
            else -> {
            }
        }
    }

    fun isEmailValid(email: CharSequence?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email)
            .matches()
    }

    companion object {
        private val TAG = SignInActivity::class.java.simpleName
    }
}

