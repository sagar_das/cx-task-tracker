package com.commercecx.tasktracker.activity

import android.app.SearchManager
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.MenuItemCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.preference.PreferenceManager
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Optional
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.adapter.ProjectsAdapter
import com.commercecx.tasktracker.adapter.QuotesAdapter
import com.commercecx.tasktracker.base.BaseSalesForceActivity
import com.commercecx.tasktracker.fragment.*
import com.commercecx.tasktracker.model.Project
import com.commercecx.tasktracker.model.Quote
import com.commercecx.tasktracker.presenter.HomePresenter
import com.commercecx.tasktracker.presenter.HomeViewTranslator
import com.commercecx.tasktracker.util.Logger
import com.commercecx.tasktracker.workmanager.ProjectWorker
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import io.realm.Realm
import java.util.concurrent.TimeUnit


class MainNavActivity : BaseSalesForceActivity<HomePresenter>(), HomeViewTranslator,
    NavigationView.OnNavigationItemSelectedListener,
    DashStatsFragment.INotificationUpdateListener, SettingsFragment.ISettingsPrefListener,
    ProjectsAdapter.IProjectClickListener,
    QuotesAdapter.IQuoteClickListener, ProjectFragment.OnSearchSelectedListener,
    QuoteFragment.OnSearchSelectedListener, ProjectDetailFragment.OnTaskSelectedListener {

    private val sharedPrefs: SharedPreferences by lazy { PreferenceManager.getDefaultSharedPreferences(this) }

    protected var fragment: Fragment? = null
    private var ft: FragmentTransaction? = null
    private var realm: Realm? = null
    var preferences: SharedPreferences? = null
    private var tvUsername: TextView? = null
    private var tvEmailid: TextView? = null

    private var ivProfilePic: ImageView? = null
    var nav_dashboard: TextView? = null

    companion object {
        var TAG: String? = "MAINNAVACT"
        private const val PREFS_NAME = "notificationcount"

    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_nav)


        ButterKnife.bind(this)


        TAG = getString(R.string.tag)
        val toolbar: Toolbar? = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
//        setSupportActionBar(toolbar)
        realm = Realm.getDefaultInstance()
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val drawer: DrawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        val navigationView: NavigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        val header: View = navigationView.getHeaderView(0)
        nav_dashboard =
            MenuItemCompat.getActionView(navigationView.menu.findItem(R.id.nav_dashboard)) as TextView

//        if (Intent.ACTION_SEARCH == intent.action) {
//            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
//                Toast.makeText(this,"Search is not accessible at this time", Toast.LENGTH_SHORT)
//
//            }
//        }
         if (intent.action != null) {
//            if (intent.action.equals(ACTION_ROUTES)) {
//               // onRoutesClickEvent()
//            } else {
//              //  onNotesClickEvent()
//            }
        } else {
            onDashboardClickEvent()
        }
        tvUsername = header.findViewById(R.id.tvUsername)
        tvEmailid = header.findViewById(R.id.tvEmailid)
        ivProfilePic = header.findViewById(R.id.ivProfilePic)
        tvUsername!!.text = preferences!!.getString("prefFirstName", "") + " " + preferences!!.getString("prefLastName", "")
        tvEmailid!!.text = preferences!!.getString("prefEmailId", "")
        val profilePicPath = preferences!!.getString("prefProfilePic", "no_pic")
        if (profilePicPath.equals("no_pic", ignoreCase = true)) {
            val resourceId: Int = resources.getIdentifier(
                "iqvia", "drawable", packageName
            )
            ivProfilePic!!.setImageResource(resourceId)
        } else {
//            val photo = NotePhoto(profilePicPath, this@MainNavActivity)
       //     ivProfilePic!!.setImageDrawable(photo.bitmapDrawable)
        }



        /* Initializing Firebase */
        initFireBase()




    }

    override fun onResume() {
        super.onResume()
        updateNotificationCounter()

    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment is ProjectFragment) {
            fragment.setOnSearchSelectedListener(this)
        }
        else if(fragment is QuoteFragment){
            fragment.setOnSearchSelectedListener(this)
        }
        else if(fragment is ProjectDetailFragment){
            fragment.setOnTaskSelectedListener(this)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                
                var currentFragment = supportFragmentManager.fragments.last()
                if (currentFragment is ProjectFragment){
                    var catalogFragment : ProjectFragment = currentFragment
                    catalogFragment.receiveSearchQuery(query)
                }
                else if (currentFragment is QuoteFragment){
                    var quoteFragment : QuoteFragment = currentFragment
                    quoteFragment.receiveSearchQuery(query)
                }
                else{
                    Toast.makeText(this,"Search is not accessible at this time", Toast.LENGTH_SHORT).show()
                }


            }
        }
    }

    private fun updateNotificationCounter() {
        nav_dashboard!!.gravity = Gravity.CENTER_VERTICAL
        nav_dashboard!!.setTypeface(null, Typeface.BOLD)
        nav_dashboard!!.setTextColor(resources.getColor(R.color.orange102))
        val preferences: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
        val count = preferences.getInt("count", 0)
        if (count == 0) {
            nav_dashboard!!.text = ""
        } else {
            nav_dashboard!!.text = if (count > 99) "99+" else count.toString()
            Log.i(TAG, "updateNotificationCounter: " + nav_dashboard!!.text.toString())
        }
    }

    override fun onBackPressed() {
        val drawer: DrawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
            return
        }
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
            return
        }


            super.onBackPressed()

    }

    fun initFireBase(){

        Firebase.messaging.isAutoInitEnabled = true

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            val msg = getString(R.string.msg_token_fmt, token)
            Log.d(TAG, msg)
           // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        })
    }

    fun onSettingsClickEvent() {

        val fragment: Fragment = supportFragmentManager.findFragmentByTag(TAG)!!
        if (fragment != null) supportFragmentManager.beginTransaction().remove(fragment).commit()
        title = "Settings"
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_layout_fl, SettingsFragment(), TAG)
            .commit()
    }



    fun onProjectClickEvent() {


        title = "Projects"
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()
        val bundle = Bundle()
        bundle.putBoolean("ALLPROJECTS", true)
        fragment = ProjectFragment()
        fragment!!.arguments = bundle
        ft = supportFragmentManager.beginTransaction().setReorderingAllowed(true)
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        //ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onQuoteClickEvent() {


        //title = "Quotes"
        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()
        fragment = QuoteFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onProjectItemClickEvent(project: Project){

        title = "Project Details"
//        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
//            .remove(
//                supportFragmentManager.findFragmentByTag(
//                    TAG
//                )!!
//            ).commit()

        val bundle = Bundle()
        bundle.putSerializable("PROJECT", project)
        fragment = ProjectDetailFragment()
        fragment!!.arguments = bundle
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    override fun onProjectTaskClicked(project: Project) {

        title = "Project Tasks"
//        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
//            .remove(
//                supportFragmentManager.findFragmentByTag(
//                    TAG
//                )!!
//            ).commit()

        val bundle = Bundle()
        bundle.putSerializable("PROJECT", project)
        fragment = TaskFragment()
        fragment!!.arguments = bundle
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onQuoteItemClickEvent(quote: Quote){



        title = "Quote Detail"
//        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
//            .remove(
//                supportFragmentManager.findFragmentByTag(
//                    TAG
//                )!!
//            ).commit()

        val bundle = Bundle()
        bundle.putSerializable("QUOTE", quote)
        fragment = QuoteDetailFragment()
        fragment!!.arguments = bundle
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    fun onDashboardClickEvent() {



        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
            .remove(
                supportFragmentManager.findFragmentByTag(
                    TAG
                )!!
            ).commit()

        title = getString(R.string.dashboard)
        fragment = DashStatsFragment()
        ft = supportFragmentManager.beginTransaction().setReorderingAllowed(true)
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        //ft!!.addToBackStack(null)
        ft!!.commit()
    }



    fun onContactUsClickEVent() {

        title = "Contact Us"
//        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
//            .remove(
//                supportFragmentManager.findFragmentByTag(
//                    TAG
//                )!!
//            ).commit()
        fragment = ContactUsFragment()
        ft = supportFragmentManager.beginTransaction()
        ft!!.replace(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.commit()
    }

    @Optional
    @OnClick(R.id.notifyContainerRL)
    fun onNotifyClickEvent() {
//        Builder(this)
//            .anchorView(notifyIV)
//            .contentView(R.layout.notify_pop_up_view)
//            .gravity(Gravity.BOTTOM)
//            .dismissOnOutsideTouch(true)
//            .dismissOnInsideTouch(true)
//            .arrowColor(Color.WHITE)
//            .modal(true)
//            .animated(false)
//            .maxWidth(R.dimen.max_width)
//            .build()
//            .show()
    }



    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_dashboard ->                // startActivity(new Intent(this, DashboardActivity.class));
                onDashboardClickEvent()
            R.id.nav_settings -> onSettingsClickEvent()
            R.id.nav_product -> onProjectClickEvent()
           // R.id.nav_account -> onQuoteClickEvent()

        }
        val drawer: DrawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }



    override fun reduceNotificationCount() {
        val preferences: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
        val count = preferences.getInt("count", 0)
        val editor = preferences.edit()
        editor.putInt("count", count - 1)
        editor.commit()
        updateNotificationCounter()
    }

    override fun onPendingProjectClick() {
        title = "Projects"
//        if (supportFragmentManager.findFragmentByTag(TAG) != null) supportFragmentManager.beginTransaction()
//            .remove(
//                supportFragmentManager.findFragmentByTag(
//                    TAG
//                )!!
//            ).commit()

        val bundle = Bundle()
        bundle.putBoolean("ALLPROJECTS", false)
        fragment = ProjectFragment()
        fragment!!.arguments = bundle
        ft = supportFragmentManager.beginTransaction()
        ft!!.add(R.id.frame_layout_fl, fragment!!, TAG)
        ft!!.addToBackStack(null)
        ft!!.commit()
    }

    override fun onPrefChanged() {
        tvUsername!!.text = preferences!!.getString(
            "prefFirstName",
            "Guest"
        ) + preferences!!.getString("prefLastName", "")
        tvEmailid!!.text = preferences!!.getString("prefEmailId", "No email id found")
    }



    override fun onCLickContactUs() {
        onContactUsClickEVent()
    }

    override fun onClickLogout() {
        getSmartStoreManager().logout(this, false)

        val sp = PreferenceManager.getDefaultSharedPreferences(this)
        val spe = sp.edit()
        spe.putBoolean("prefLogin", false)
        spe.commit()
        spe.clear()
        //           android.preference.PreferenceManager.getDefaultSharedPreferences(this).edit().remove(LAST_CALL_PREF).apply()
//            finish()
    }



    override fun createPresenter(): HomePresenter = HomePresenter(
        getSmartStore(),
        sharedPrefs,
        this
    )

    override fun showRoutesContent() {
        TODO("Not yet implemented")
    }

    override fun showCatalogContent() {
        TODO("Not yet implemented")
    }

    override fun showOrdersContent() {
        TODO("Not yet implemented")
    }

    override fun showProfileContent(name: String, email: String) {
        TODO("Not yet implemented")
    }

    override fun selectFragment() {
        Logger.info("select fragment")
    }

    override fun showLoader(windowsFlagsEnabled: Boolean) {
        Logger.info("show loader")
    }

    override fun hideLoader(windowsFlagsEnabled: Boolean) {
        Logger.info("hide loader")
    }

    override fun showUnknownError() {
        TODO("Not yet implemented")
    }

    override fun onProjectClick(project: Project?) {
        //Toast.makeText(this,"Clicked", Toast.LENGTH_SHORT).show()
        onProjectItemClickEvent(project!!)

    }

    override fun onQuoteClick(quote: Quote?) {
        //Toast.makeText(this,"Clicked", Toast.LENGTH_SHORT).show()
        onQuoteItemClickEvent(quote!!)
    }

    override fun onProjectSearchClicked() {
        onSearchRequested()
    }

    override fun onQuoteSearchClicked() {
        onSearchRequested()
    }



    override fun onQuoteClicked(project: Project) {

        var quote = Quote(project.quoteProposal,"","",
        "","","","",
        "","","","",
        "","")

        onQuoteItemClickEvent(quote)
    }

    private fun scheduleRepeatingTasks() {

        /*Setting up different constraints on the work request.
         */
        val constraints = Constraints.Builder().apply {
            setRequiredNetworkType(NetworkType.CONNECTED)
            setRequiresCharging(true)
            setRequiresStorageNotLow(true)
        }.build()

        /*Build up an obejct of PeriodicWorkRequestBuilder
        */
        val repeatingWork = PeriodicWorkRequestBuilder<ProjectWorker>(
            1,
            TimeUnit.HOURS
        ).setConstraints(constraints)
            .build()

        /*Enqueue the work request to an instance of Work Manager
         */
        WorkManager.getInstance(this).enqueue(repeatingWork)
    }


}
