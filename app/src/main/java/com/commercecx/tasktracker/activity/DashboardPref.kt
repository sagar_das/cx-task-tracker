package com.commercecx.tasktracker.activity


import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.appcompat.widget.Toolbar
import com.commercecx.tasktracker.R


class DashboardPref : AppCompatActivity() {
    var settings: SharedPreferences? = null
    private var swRecentActivity: SwitchCompat? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_pref)
        title = "Dashboard preferences"
        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        settings = getSharedPreferences(PREFS_NAME, 0)
        swRecentActivity = findViewById<SwitchCompat>(R.id.swRecentActivity)
        if (settings!!.getBoolean("recentactivity", true)) {
            swRecentActivity!!.toggle()
        }
        swRecentActivity!!.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            val editor = settings!!.edit()
            if (b) {
                editor.putBoolean("recentactivity", true)
            } else {
                editor.putBoolean("recentactivity", false)
            }
            editor.commit()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val PREFS_NAME = "dashboardpref"
    }
}
