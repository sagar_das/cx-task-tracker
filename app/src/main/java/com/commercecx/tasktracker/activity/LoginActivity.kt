package com.commercecx.tasktracker.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle

import android.view.WindowManager
import androidx.preference.PreferenceManager
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.base.BaseSalesForceActivity
import com.commercecx.tasktracker.presenter.HomePresenter
import com.commercecx.tasktracker.presenter.HomeViewTranslator
import com.commercecx.tasktracker.util.Logger
import com.salesforce.androidsdk.rest.RestClient

class LoginActivity : BaseSalesForceActivity<HomePresenter>(), HomeViewTranslator {
    private val sharedPrefs: SharedPreferences by lazy { PreferenceManager.getDefaultSharedPreferences(this) }

    var client: RestClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    }

    override fun createPresenter(): HomePresenter = HomePresenter(
        getSmartStore(),
        sharedPrefs,
        this
    )

    override fun onResume() {
        super.onResume()
        BaseApplication.instance.accountId = ""
    }

    override fun showRoutesContent() {
        TODO("Not yet implemented")
    }

    override fun showCatalogContent() {
        TODO("Not yet implemented")
    }

    override fun showOrdersContent() {
        TODO("Not yet implemented")
    }

    override fun showProfileContent(name: String, email: String) {
        TODO("Not yet implemented")
    }

    override fun selectFragment() {
        Logger.info("select fragment")
    }

    override fun showLoader(windowsFlagsEnabled: Boolean) {
        Logger.info("Show loader")
    }

    override fun hideLoader(windowsFlagsEnabled: Boolean) {

        val spe = sharedPrefs.edit()
//        spe.putString("prefFirstName", userNameET!!.text.toString())
//        spe.putString("prefLastName", lastNameET!!.text.toString())
//        spe.putString("prefEmailId", emailET!!.text.toString())
        spe.putBoolean("prefLogin", true)
        spe.commit()
        spe.clear()
        val intent = Intent(this@LoginActivity, MainNavActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun showUnknownError() {
        TODO("Not yet implemented")
    }


}