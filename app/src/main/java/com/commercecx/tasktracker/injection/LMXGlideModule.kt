package com.commercecx.tasktracker.injection

/**
 * Created by Sagar Das on 8/20/20.
 */
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class LMXGlideModule : AppGlideModule()