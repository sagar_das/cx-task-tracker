package com.commercecx.tasktracker.adapter

/**
 * Created by Sagar Das on 8/19/20.
 */


import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.model.Notification
import io.realm.Realm


class NotificationAdapter(
    fragment: Fragment,
    private val context: Context,
    private val notificationsList: List<Notification>
) :
    RecyclerView.Adapter<NotificationAdapter.NotificationVH?>() {
    private val realm: Realm
    private val preferences: SharedPreferences? = null

    interface INotificationClickListener {
        fun OnNotificationClick()
    }

    var mNotifyListener: INotificationClickListener
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationVH {
        return NotificationVH(
            LayoutInflater.from(context).inflate(R.layout.li_notifications, parent, false)
        )
    }

    override fun onBindViewHolder(@NonNull holder: NotificationVH, position: Int) {
        holder.tvNotifyTitle.text = notificationsList[position].getTitle()
        holder.tvNotifyContents.text = notificationsList[position].getContents()
        when (notificationsList[position].getType()) {
            "customer" -> {
                holder.ivBackColor.setColorFilter(Color.argb(255, 231, 83, 72))
                holder.ivNotifyImage.setImageDrawable(context.getDrawable(R.drawable.ic_person))
            }
            "truck" -> {
                holder.ivBackColor.setColorFilter(Color.argb(255, 0, 174, 239))
                holder.ivNotifyImage.setImageDrawable(context.getDrawable(R.drawable.ic_assignment))
            }
            "more" -> {
                holder.ivBackColor.setColorFilter(Color.argb(255, 44, 60, 83))
                holder.ivNotifyImage.setImageDrawable(context.getDrawable(R.drawable.ic_more_white))
                val params: ViewGroup.LayoutParams = holder.cvNotification.layoutParams
                params.width = 260
            }
        }
        if (!notificationsList[position].isRead()) {
            holder.cvNotification.setCardBackgroundColor(Color.argb(255, 255, 204, 179))
        }
        holder.cvNotification.setOnClickListener(View.OnClickListener {
            holder.cvNotification.setCardBackgroundColor(Color.argb(255, 255, 255, 255))
            if (!notificationsList[position].isRead()) {
                val notification = realm.where(Notification::class.java).equalTo(
                    "notification_id",
                    notificationsList[position].getNotification_id()
                ).findFirst()
                realm.executeTransaction { notification!!.setRead(true) }
                mNotifyListener.OnNotificationClick()
            }
        })
    }

    override fun getItemCount(): Int {
        return notificationsList.size
    }

    inner class NotificationVH(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var ivBackColor: ImageView
        var ivNotifyImage: ImageView
        var tvNotifyTitle: TextView
        var tvNotifyContents: TextView
        var cvNotification: CardView

        init {
            ivBackColor = itemView.findViewById<View>(R.id.ivBackColor) as ImageView
            ivNotifyImage = itemView.findViewById<View>(R.id.ivNotifyImage) as ImageView
            tvNotifyTitle = itemView.findViewById<View>(R.id.tvNotifyTitle) as TextView
            tvNotifyContents = itemView.findViewById<View>(R.id.tvNotifyContents) as TextView
            cvNotification = itemView.findViewById<View>(R.id.cvNotification) as CardView
        }
    }

    init {
        realm = Realm.getDefaultInstance()
        mNotifyListener = fragment as INotificationClickListener
    }
}
