package com.commercecx.tasktracker.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.model.Quote

/**
 * Created by Sagar Das on 8/25/20.
 */

class QuotesAdapter(
    private val activity: Activity,
    quotes: MutableList<Quote>?,
    private val context: Context
) : RecyclerView.Adapter<QuotesAdapter.ViewHolder?>() {

    private var quotes: MutableList<Quote>? = null
    var quote: Quote? = null

    interface IQuoteClickListener {
        fun onQuoteClick(quote: Quote?)
    }

    var quoteClickListener: IQuoteClickListener? = null

    init {
        this.quotes = quotes
        quoteClickListener = activity as IQuoteClickListener?
    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView
        var tvDesc: TextView
        var llQuoteItem: LinearLayout


        init {
            llQuoteItem = itemView.findViewById<View>(R.id.llQuoteItem) as LinearLayout
            tvName = itemView.findViewById<View>(R.id.tvName) as TextView
            tvDesc = itemView.findViewById<View>(R.id.tvDescription) as TextView

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.li_quote_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        quote = quotes!![position]
        holder.tvName.text = quote!!.name
        holder.tvDesc.text = quote!!.quotenumber

        holder.llQuoteItem.setOnClickListener {
            quote = quotes!![position]
            quoteClickListener!!.onQuoteClick(quote)
        }

    }

    override fun getItemCount(): Int {
        return quotes!!.size
    }
}