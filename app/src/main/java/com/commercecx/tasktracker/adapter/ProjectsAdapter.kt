package com.commercecx.tasktracker.adapter

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.model.Project

/**
 * Created by Sagar Das on 11/18/20.
 */
class ProjectsAdapter(
    private val activity: Activity,
    projects: MutableList<Project>?,
    private val context: Context
) : RecyclerView.Adapter<ProjectsAdapter.ViewHolder?>() {

    private var projects: MutableList<Project>? = null
    var project: Project? = null

    interface IProjectClickListener {
        fun onProjectClick(project: Project?)
    }

    var projectClickListener: IProjectClickListener? = null

    init {
        this.projects = projects
        projectClickListener = activity as IProjectClickListener?
    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView
        var tvDesc: TextView
        var tvStartDate: TextView
        var ivStatus: ImageView
        var llProjectItem: LinearLayout



        init {
            llProjectItem = itemView.findViewById<View>(R.id.llProjectItem) as LinearLayout
            tvName = itemView.findViewById<View>(R.id.tvName) as TextView
            tvDesc = itemView.findViewById<View>(R.id.tvDescription) as TextView
            tvStartDate = itemView.findViewById<View>(R.id.tvStartDate) as TextView
            ivStatus = itemView.findViewById<View>(R.id.ivStatus) as ImageView



        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.li_project_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        project = projects!![position]
        holder.tvName.text = project!!.projectName
        holder.tvStartDate.text = project!!.startDate

        if(project!!.projectstatus == ""){
            holder.tvDesc.text = "Pending"
            holder.ivStatus.setImageResource(R.drawable.ic_alert)

            ImageViewCompat.setImageTintList(holder.ivStatus, ColorStateList.valueOf(activity.resources.getColor(R.color.light_blue101)))
         //   holder.tvDesc.setTextColor(activity.resources.getColor(R.color.light_blue101))


        }
        else {
            holder.tvDesc.text = project!!.projectstatus
        }

        if(project!!.projectstatus == "Approved"){
            holder.ivStatus.setImageResource(R.drawable.order_submitted)
            ImageViewCompat.setImageTintList(holder.ivStatus, ColorStateList.valueOf(activity.resources.getColor(R.color.green)))
       //     holder.tvDesc.setTextColor(activity.resources.getColor(R.color.green))
            holder.llProjectItem.setBackgroundResource(R.drawable.card_approved_bg)
        }
        else if(project!!.projectstatus == "Rejected"){
            holder.ivStatus.setImageResource(R.drawable.cross_icon)
            ImageViewCompat.setImageTintList(holder.ivStatus, ColorStateList.valueOf(activity.resources.getColor(R.color.red_300)))
         //   holder.tvDesc.setTextColor(activity.resources.getColor(R.color.red_300))
            holder.llProjectItem.setBackgroundResource(R.drawable.card_rejected_bg)

        }


        holder.llProjectItem.setOnClickListener {
            project = projects!![position]
            projectClickListener!!.onProjectClick(project)
        }

    }

    override fun getItemCount(): Int {
        return projects!!.size
    }
}