package com.commercecx.tasktracker.adapter

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.model.RecentActivityDash
import io.realm.Realm

class RecentActivityAdapter(
    private val recentActivityList: List<RecentActivityDash>,
    private val context: Context
) :
    RecyclerView.Adapter<RecentActivityAdapter.RecentActivityVH?>() {
    private val realm: Realm
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentActivityVH {
        return RecentActivityVH(
            LayoutInflater.from(context).inflate(R.layout.li_recent_activity, parent, false)
        )
    }

    override fun onBindViewHolder(@NonNull holder: RecentActivityVH, position: Int) {
        holder.tvRecentActivityName.text = recentActivityList[position].getActivity_name()
        holder.tvRecentActivityDate.text = recentActivityList[position].getActivity_date()
    }

    override fun getItemCount(): Int {
        return recentActivityList.size
    }

    inner class RecentActivityVH(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var tvRecentActivityName: TextView
        var tvRecentActivityDate: TextView

        init {
            tvRecentActivityName =
                itemView.findViewById<View>(R.id.tvRecentActivityName) as TextView
            tvRecentActivityDate =
                itemView.findViewById<View>(R.id.tvRecentActivityDate) as TextView
        }
    }

    init {
        realm = Realm.getDefaultInstance()
    }
}
