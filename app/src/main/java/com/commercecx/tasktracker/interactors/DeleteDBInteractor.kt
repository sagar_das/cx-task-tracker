package com.commercecx.tasktracker.interactors

import com.commercecx.tasktracker.model.database.*



class DeleteDBInteractor : BaseInteractor<Unit, Result<Boolean>> {

    override fun syncCall(params: Unit?): Result<Boolean> {
        return try {
            DBProductHelper().deleteAll()
            DBOrdersHelper().deleteAll()
            DBRoutesHelper().deleteAll()
            DBContractedProductHelper().deleteAll()
            DBUserHelper().deleteAll()
            Result(true)
        } catch (e: Exception) {
            Result(null, e)
        }
    }
}