package com.commercecx.tasktracker.interactors

import com.commercecx.tasktracker.model.PendingOrder

import com.commercecx.tasktracker.model.database.DBOrdersHelper


class StoreOrderInteractor : BaseInteractor<PendingOrder, Result<Unit>> {

    override fun syncCall(params: PendingOrder?): Result<Unit> {
        try {
            if (params == null) {
                throw IllegalArgumentException()
            }

            DBOrdersHelper().insertOrder(params)

            with(DBOrdersHelper()) {
                insertOrder(params)
                getAllOrders()
            }
            return Result(Unit)


        } catch (e: Exception) {
            return Result(null, e)
        }
    }
}