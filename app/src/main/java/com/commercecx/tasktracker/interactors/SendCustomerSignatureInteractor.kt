package com.commercecx.tasktracker.interactors

import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.rest.RestRequest
import com.salesforce.androidsdk.smartstore.store.SmartStore
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject

class
SendCustomerSignatureInteractor(private val client: RestClient, private val store: SmartStore) :
    BaseInteractor<JSONObject, Result<Unit>> {

    override fun syncCall(params: JSONObject?): Result<Unit> {
        try {
            if (params == null) {
                throw IllegalArgumentException()
            }

            val syncRequest = RestRequest(
                RestRequest.RestMethod.POST,
                "/services/apexrest/RSOOrder/CustomerSignature",
                RequestBody.create(MediaType.parse("application/json"), params.toString())
            )

            val syncResponse = client.sendSync(syncRequest)

            if (syncResponse.isSuccess) {
//                with(JDEDBHelper(store)) {
                //TODO: We're supposed to store signature with the order, not send them to backend
//            }
                return Result(Unit)
            } else {
                throw IllegalStateException()
            }

        } catch (e: Exception) {
            return Result(null, e)
        }
    }
}