package com.commercecx.tasktracker.interactors


import com.commercecx.tasktracker.model.Product
import com.commercecx.tasktracker.model.database.DBContractedProductHelper
import com.commercecx.tasktracker.model.database.DBProductHelper

class GetAllProductsInteractor : BaseInteractor<Boolean, Result<List<Product>>> {

    override fun syncCall(params: Boolean?): Result<List<Product>> {
        return try {
            var result = mutableListOf<Product>()

            result.addAll(DBContractedProductHelper().getAllProducts())

            if (params != null && params == true) {
                result.addAll(DBProductHelper().getAllProducts())
                result = result.distinctBy { it.productid }.toMutableList()
            }

            result.sortBy { it.productname }

            Result(result)
        } catch (ex: Exception) {
         //   JDELog.e("GetAllProductsInteractor exception: {${ex.message}}")
            Result(null, ex)
        }
    }
}

