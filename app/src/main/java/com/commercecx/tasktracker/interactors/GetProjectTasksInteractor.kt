package com.commercecx.tasktracker.interactors

import android.content.SharedPreferences
import com.commercecx.tasktracker.model.PricingTaskInfo
import com.commercecx.tasktracker.model.ProductInfo
import com.commercecx.tasktracker.model.ProjectTask
import com.commercecx.tasktracker.model.database.DBProductHelper
import com.commercecx.tasktracker.model.database.DBProjectTaskHelper
import com.commercecx.tasktracker.network.NetworkConstant
import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.rest.RestRequest
import org.json.JSONArray
import java.util.*

/**
 * Created by Sagar Das on 11/19/20.
 */

data class PricingTaskNameFilter(val pricingTaskName: String)

class GetProjectTasksInteractor(private val client: RestClient,
                                private val prefs: SharedPreferences?,
                                private val projectId: String?,
                                private val checkLocalFirst: Boolean) : BaseInteractor<Unit, Result<List<ProjectTask>>> {

    override fun syncCall(params: Unit?): Result<List<ProjectTask>> {

        return try {
            var result = mutableListOf<ProjectTask>()


            if(checkLocalFirst){
                result.addAll(DBProjectTaskHelper().getProjectTasksByProjectId(projectId!!))
                //  result.addAll(DBPricingTaskHelper().getPricingTaskByProjectName(projectName!!))
                result = result.distinctBy { it.id }.toMutableList()


                result.sortBy { it.name }
            }




            //Fetching from salesforce
            if(result.size == 0){

//                val pricingTaskRequest = RestRequest.getRequestForQuery(
//                    BaseApplication.instance.getString(
//                        R.string.salesforce_api_version), NetworkConstant.PRICINGTASK_GET_QUERY)

                val pricingTaskRequest = RestRequest(
                    RestRequest.RestMethod.GET,
                    NetworkConstant.SALESFORCE_PROJECTTASK_PATH + "/" + projectId
                )




                val pricingTaskResponse = client.sendSync(pricingTaskRequest)

                if(pricingTaskResponse.isSuccess){
                  //  val response = pricingTaskResponse.asJSONObject().getJSONArray("records")

                    val response = pricingTaskResponse.asJSONArray()
                    var  productJSONArray = JSONArray()

                    for (i in 0 until response.length()) {
                        val item = response.getJSONObject(i)
                        //Checking if Product details exist
                        if(item.has("Product__r")){

                            productJSONArray.put(item["Product__r"])
                        }


                    }

                    val pricingTaskInfo = PricingTaskInfo.Mapper.from(response)
                    val productInfo = ProductInfo.Mapper.from(productJSONArray)

                   if (checkLocalFirst) {

                        DBProjectTaskHelper().insertProjectTasks(pricingTaskInfo.projectTasks)
                        DBProductHelper().insertProducts(productInfo.products)
                  } else {

                       DBProjectTaskHelper().updateProjectTasks(pricingTaskInfo.projectTasks)
                       DBProductHelper().updateProducts(productInfo.products)
                   }
//                    storeSyncDate(prefs)

                    result.addAll(pricingTaskInfo.projectTasks)


                }


            }

            Result(result)
        } catch (ex: Exception) {
            Result(null, ex)
        }
    }

    private fun isInitialLoad(prefs: SharedPreferences) = prefs.getLong(
        SyncDataInteractor.LAST_CALL_PREF,
        SyncDataInteractor.NO_VALUE_PREF
    ) == SyncDataInteractor.NO_VALUE_PREF

    private fun storeSyncDate(prefs: SharedPreferences) = prefs.edit().putLong(SyncDataInteractor.LAST_CALL_PREF, Date().time).apply()
}

class GetPricingTasksByNameInteractor : BaseInteractor<PricingTaskNameFilter, Result<List<ProjectTask>>> {

    override fun syncCall(params: PricingTaskNameFilter?): Result<List<ProjectTask>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<ProjectTask>()
            result.addAll(DBProjectTaskHelper().getProjectTaskByName(params.pricingTaskName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {

            Result(null, ex)
        }
    }


}

class GetPricingTasksByProjectNameInteractor : BaseInteractor<PricingTaskNameFilter, Result<List<ProjectTask>>> {

    override fun syncCall(params: PricingTaskNameFilter?): Result<List<ProjectTask>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<ProjectTask>()
            result.addAll(DBProjectTaskHelper().getProjectTaskByProjectName(params.pricingTaskName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {

            Result(null, ex)
        }
    }


}

class GetProjectTasksByCountryInteractor : BaseInteractor<PricingTaskNameFilter, Result<List<ProjectTask>>> {

    override fun syncCall(params: PricingTaskNameFilter?): Result<List<ProjectTask>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<ProjectTask>()
            result.addAll(DBProjectTaskHelper().getProjectTaskByCountryName(params.pricingTaskName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {

            Result(null, ex)
        }
    }


}

class GetProjectTasksByProductInteractor : BaseInteractor<PricingTaskNameFilter, Result<List<ProjectTask>>> {

    override fun syncCall(params: PricingTaskNameFilter?): Result<List<ProjectTask>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<ProjectTask>()
            result.addAll(DBProjectTaskHelper().getProjectTaskByProductName(params.pricingTaskName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {

            Result(null, ex)
        }
    }


}