package com.commercecx.tasktracker.interactors

import android.content.SharedPreferences
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.model.AccountInfo
import com.commercecx.tasktracker.model.OpportunityInfo
import com.commercecx.tasktracker.model.Project
import com.commercecx.tasktracker.model.ProjectInfo
import com.commercecx.tasktracker.model.database.DBAccountHelper
import com.commercecx.tasktracker.model.database.DBOpportunityHelper
import com.commercecx.tasktracker.model.database.DBProjectHelper
import com.commercecx.tasktracker.model.database.DBUserHelper
import com.commercecx.tasktracker.network.NetworkConstant
import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.rest.RestRequest
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


//TODO: Errors


class SyncDataInteractor(
    private val client: RestClient,
    private val store: SmartStore?,
    private val prefs: SharedPreferences?
) : BaseInteractor<Unit, Result<Unit>> {

    companion object {
        const val LAST_CALL_PREF = "last_call_pref"
        const val NO_VALUE_PREF = -1L
    }

    override fun syncCall(params: Unit?): Result<Unit> {
        try {

//            DBOrdersHelper().deleteOldOrders()
//            DBRoutesHelper().deletePreviousRoutes(Date())

            val param = JSONObject()
            param.put("EmpEmail", client.clientInfo.email)
            if(prefs != null) {
                param.put("InitialLoad", isInitialLoad(prefs))
            }


            /* https://iqvia--clmxna.lightning.force.com/lightning/setup/ApexClasses/page?address=%2F01p0100000055R2%2Fe%3FretURL%3D%252Fsetup%252Fbuild%252FviewApexClass.apexp%253Fid%253D01p0100000055R2%2526id%253D01p0100000055R2%2526isdtp%253Dp1%2526sfdcIFrameOrigin%253Dhttps%25253A%25252F%25252Fiqvia--clmxna.lightning.force.com%2526clc%253D1%26sfdcIFrameOrigin%3Dhttps%253A%252F%252Fiqvia--clmxna.lightning.force.com%26clc%3D1
             */

            val syncRequest = RestRequest(
                RestRequest.RestMethod.GET,
                NetworkConstant.SALESFORCE_PROJECT_PATH
            )

            val projectRequest = RestRequest.getRequestForQuery(BaseApplication.instance.getString(R.string.salesforce_api_version), NetworkConstant.PROJECTS_GET_QUERY)



            val syncResponse = client.sendSync(syncRequest)
            if (syncResponse.isSuccess) {

                val response = syncResponse.asJSONArray()
                var quoteJSONArray = JSONArray()
                var  projectJSONArray = JSONArray()
                var  accountJsonArray = JSONArray()
                var  opportunityJsonArray = JSONArray()

                for (i in 0 until response.length()) {
                    var item = response.getJSONObject(i)

                    //For custom project object

                    if(item.has("Quote__r")){

                        quoteJSONArray.put(item["Quote__r"])

                        for (index in 0 until quoteJSONArray.length()) {

                            val qItem = quoteJSONArray.getJSONObject(index)
                            //Checking if Account details exist
                            if(qItem.has("Apttus_Proposal__Account__r")){

                                accountJsonArray.put(qItem["Apttus_Proposal__Account__r"])

                                item.put("pse__Account__c", (qItem["Apttus_Proposal__Account__r"] as JSONObject).getString("Id"))

                            }
                            if(qItem.has("Apttus_Proposal__Opportunity__r")){

                                opportunityJsonArray.put(qItem["Apttus_Proposal__Opportunity__r"])


                                item.put("pse__Opportunity__c", (qItem["Apttus_Proposal__Opportunity__r"] as JSONObject).getString("Id"))
                            }


                        }


                    }

                    //Checking if Account details exist
//                   if(item.has("pse__Account__r")){
//
//                       accountJsonArray.put(item["pse__Account__r"])
//                   }
//                    if(item.has("pse__Opportunity__r")){
//
//                        opportunityJsonArray.put(item["pse__Opportunity__r"])
//                    }
//                    val attribute = item["attributes"] as JSONObject
//                    if(attribute.get("type").toString().equals("Product2",true)
//                        && item.get("Name").toString().contains("DB ",true)){
//                        productJsonArray.put(item)
//                    }

                    projectJSONArray.put(item)



                }

                val projectInfo = ProjectInfo.Mapper.from(projectJSONArray)
                val accountInfo = AccountInfo.Mapper.from(accountJsonArray)
                val opportunityInfo = OpportunityInfo.Mapper.from(opportunityJsonArray)


                if(prefs != null){
                    if (isInitialLoad(prefs)) {
//                    JDELog.w("Initial load, inserting products")
                        DBProjectHelper().insertProjects(projectInfo.projects)
                        DBAccountHelper().insertAccounts(accountInfo.accounts)
                        DBOpportunityHelper().insertOpportunitys(opportunityInfo.opportunities)


                    } else {
//                    JDELog.w("Not initial load, updating products")
                        DBProjectHelper().updateProjects(projectInfo.projects, true)
                        DBAccountHelper().updateAccounts(accountInfo.accounts, true)
                        DBOpportunityHelper().updateOpportunitys(opportunityInfo.opportunities, true)

                    }
                    storeSyncDate(prefs)

                }

                else{
                    DBProjectHelper().updateProjects(projectInfo.projects, true)
                    DBAccountHelper().updateAccounts(accountInfo.accounts, true)
                    DBOpportunityHelper().updateOpportunitys(opportunityInfo.opportunities, true)
                }





                return Result(Unit)
            } else {
                return Result(null, Exception(syncResponse.statusCode.toString()))
            }

            /*
            * Name, Account, BudgetAmount, ProjectStatus */





        } catch (e: Exception) {
            BaseApplication.instance.userInfo = DBUserHelper().getUserData()
            return Result(null, e)
        }
    }

    fun refreshProjectData(): List<Project>? {

        var result: List<Project>? = null
        val syncRequest = RestRequest(
            RestRequest.RestMethod.GET,
            NetworkConstant.SALESFORCE_PROJECT_PATH
        )

        val projectRequest = RestRequest.getRequestForQuery(
            BaseApplication.instance.getString(R.string.salesforce_api_version),
            NetworkConstant.PROJECTS_GET_QUERY
        )


        val syncResponse = client.sendSync(syncRequest)
        if (syncResponse.isSuccess) {

            val response = syncResponse.asJSONArray()
            var projectJSONArray = JSONArray()
            var accountJsonArray = JSONArray()
            var opportunityJsonArray = JSONArray()

            for (i in 0 until response.length()) {
                val item = response.getJSONObject(i)

                //Checking if Account details exist
                if (item.has("pse__Account__r")) {

                    accountJsonArray.put(item["pse__Account__r"])
                }
                if (item.has("pse__Opportunity__r")) {

                    opportunityJsonArray.put(item["pse__Opportunity__r"])
                }
//                    val attribute = item["attributes"] as JSONObject
//                    if(attribute.get("type").toString().equals("Product2",true)
//                        && item.get("Name").toString().contains("DB ",true)){
//                        productJsonArray.put(item)
//                    }

                projectJSONArray.put(item)


            }

            val projectInfo = ProjectInfo.Mapper.from(projectJSONArray)
            result = projectInfo.projects
            val accountInfo = AccountInfo.Mapper.from(accountJsonArray)
            val opportunityInfo = OpportunityInfo.Mapper.from(opportunityJsonArray)



            DBProjectHelper().updateProjects(projectInfo.projects, true)
            DBAccountHelper().updateAccounts(accountInfo.accounts, true)
            DBOpportunityHelper().updateOpportunitys(opportunityInfo.opportunities, true)



        }

        return result
    }


    private fun isInitialLoad(prefs: SharedPreferences) = prefs.getLong(LAST_CALL_PREF, NO_VALUE_PREF) == NO_VALUE_PREF

    private fun storeSyncDate(prefs: SharedPreferences) = prefs.edit().putLong(LAST_CALL_PREF, Date().time).apply()

}







