package com.commercecx.tasktracker.interactors

import com.commercecx.tasktracker.model.Category
import com.commercecx.tasktracker.model.Product
import com.commercecx.tasktracker.model.database.DBContractedProductHelper
import com.commercecx.tasktracker.model.database.DBProductHelper


data class ProductFilter(val brand: Category, val category: Category, val subCategory: Category, val orderNonContractedProducts: Boolean = false, val isCatalog: Boolean)
data class ProductNameFilter(val productName: String)

class GetProductsInteractor : BaseInteractor<ProductFilter, Result<List<Product>>> {

    override fun syncCall(params: ProductFilter?): Result<List<Product>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Product>()


            result.addAll(DBContractedProductHelper().getProducts(params.category.name, params.subCategory.name, params.brand.name))

            if (params.orderNonContractedProducts) {
                result.addAll(DBProductHelper().getProducts(params.category.name, params.subCategory.name, params.brand.name))
                result = result.distinctBy { it.productid }.toMutableList()
            }

            result.sortBy { it.productname }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}

class GetProductsByNameInteractor : BaseInteractor<ProductNameFilter, Result<List<Product>>> {

    override fun syncCall(params: ProductNameFilter?): Result<List<Product>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Product>()


            result.addAll(DBProductHelper().getProductByName(params.productName))
            result = result.distinctBy { it.productid }.toMutableList()


            result.sortBy { it.productname }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}

class GetProductsByIdInteractor : BaseInteractor<ProductNameFilter, Result<List<Product>>> {

    override fun syncCall(params: ProductNameFilter?): Result<List<Product>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Product>()


            result.addAll(DBProductHelper().getProductsById(params.productName))
            result = result.distinctBy { it.productid }.toMutableList()


            result.sortBy { it.productname }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}