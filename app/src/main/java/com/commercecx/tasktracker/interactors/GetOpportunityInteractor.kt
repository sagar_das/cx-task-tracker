package com.commercecx.tasktracker.interactors

import com.commercecx.tasktracker.model.Opportunity
import com.commercecx.tasktracker.model.database.DBOpportunityHelper

/**
 * Created by Sagar Das on 12/9/20.
 * 
 * 
 */

data class OpportunityNameFilter(val opportunityName: String)

class GetOpportunityInteractor : BaseInteractor<Unit, Result<List<Opportunity>>> {

    override fun syncCall(params: Unit?): Result<List<Opportunity>> {

        return try {
            var result = mutableListOf<Opportunity>()




            result.addAll(DBOpportunityHelper().getAllOpportunitys())
            result = result.distinctBy { it.id }.toMutableList()


            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {
            //   JDELog.e("GetAllProductsInteractor exception: {${ex.message}}")
            Result(null, ex)
        }
    }
}

class GetOpportunitysByIDInteractor : BaseInteractor<OpportunityNameFilter, Result<List<Opportunity>>> {

    override fun syncCall(params: OpportunityNameFilter?): Result<List<Opportunity>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Opportunity>()
            result.addAll(DBOpportunityHelper().getOpportunitysById(params.opportunityName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}