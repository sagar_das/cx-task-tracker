package com.commercecx.tasktracker.interactors

import com.commercecx.tasktracker.model.Account
import com.commercecx.tasktracker.model.database.DBAccountHelper

/**
 * Created by Sagar Das on 8/25/20.
 */

data class AccountNameFilter(val accountName: String)

class GetAccountsInteractor : BaseInteractor<Unit, Result<List<Account>>> {

    override fun syncCall(params: Unit?): Result<List<Account>> {

        return try {
            var result = mutableListOf<Account>()




                result.addAll(DBAccountHelper().getAllAccounts())
                result = result.distinctBy { it.id }.toMutableList()


            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {
            //   JDELog.e("GetAllProductsInteractor exception: {${ex.message}}")
            Result(null, ex)
        }
    }
}

class GetAccountsByIDInteractor : BaseInteractor<AccountNameFilter, Result<List<Account>>> {

    override fun syncCall(params: AccountNameFilter?): Result<List<Account>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Account>()
            result.addAll(DBAccountHelper().getAccountsById(params.accountName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}