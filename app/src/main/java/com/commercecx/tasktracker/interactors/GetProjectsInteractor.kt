package com.commercecx.tasktracker.interactors

import android.util.Log
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.model.AccountInfo
import com.commercecx.tasktracker.model.OpportunityInfo
import com.commercecx.tasktracker.model.Project
import com.commercecx.tasktracker.model.ProjectInfo
import com.commercecx.tasktracker.model.database.DBAccountHelper
import com.commercecx.tasktracker.model.database.DBOpportunityHelper
import com.commercecx.tasktracker.model.database.DBProjectHelper
import com.commercecx.tasktracker.network.NetworkConstant
import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.rest.RestRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Sagar Das on 11/18/20.
 */
data class ProjectNameFilter(val projectName: String)

class GetProjectsInteractor(private val client: RestClient, val fetchFromServer: Boolean) : BaseInteractor<Unit, Result<List<Project>>> {

    override fun syncCall(params: Unit?): Result<List<Project>> {

        return try {
            var result = mutableListOf<Project>()
            if(fetchFromServer){

                val syncRequest = RestRequest(
                    RestRequest.RestMethod.GET,
                    NetworkConstant.SALESFORCE_PROJECT_PATH
                )

                val projectRequest = RestRequest.getRequestForQuery(
                    BaseApplication.instance.getString(R.string.salesforce_api_version),
                    NetworkConstant.PROJECTS_GET_QUERY
                )


                val syncResponse = client.sendSync(syncRequest)
                if (syncResponse.isSuccess) {

                    val response = syncResponse.asJSONArray()
                    var quoteJSONArray = JSONArray()
                    var projectJSONArray = JSONArray()
                    var accountJsonArray = JSONArray()
                    var opportunityJsonArray = JSONArray()

                    for (i in 0 until response.length()) {
                        val item = response.getJSONObject(i)

                        //For custom project object

                        if(item.has("Quote__r")){

                            quoteJSONArray.put(item["Quote__r"])

                            for (index in 0 until quoteJSONArray.length()) {

                                val qItem = quoteJSONArray.getJSONObject(index)
                                //Checking if Account details exist
                                if(qItem.has("Apttus_Proposal__Account__r")){

                                    accountJsonArray.put(qItem["Apttus_Proposal__Account__r"])
                                    item.put("pse__Account__c", (qItem["Apttus_Proposal__Account__r"] as JSONObject).getString("Id"))

                                }
                                if(qItem.has("Apttus_Proposal__Opportunity__r")){

                                    opportunityJsonArray.put(qItem["Apttus_Proposal__Opportunity__r"])
                                    item.put("pse__Opportunity__c", (qItem["Apttus_Proposal__Opportunity__r"] as JSONObject).getString("Id"))

                                }


                            }


                        }

                        //Checking if Account details exist
//                        if (item.has("pse__Account__r")) {
//
//                            accountJsonArray.put(item["pse__Account__r"])
//                        }
//                        if (item.has("pse__Opportunity__r")) {
//
//                            opportunityJsonArray.put(item["pse__Opportunity__r"])
//                        }
//                    val attribute = item["attributes"] as JSONObject
//                    if(attribute.get("type").toString().equals("Product2",true)
//                        && item.get("Name").toString().contains("DB ",true)){
//                        productJsonArray.put(item)
//                    }

                        projectJSONArray.put(item)


                    }

                    val projectInfo = ProjectInfo.Mapper.from(projectJSONArray)
                    result.addAll(projectInfo.projects)
                    val accountInfo = AccountInfo.Mapper.from(accountJsonArray)
                    val opportunityInfo = OpportunityInfo.Mapper.from(opportunityJsonArray)



                    DBProjectHelper().updateProjects(projectInfo.projects, true)
                    DBAccountHelper().updateAccounts(accountInfo.accounts, true)
                    DBOpportunityHelper().updateOpportunitys(opportunityInfo.opportunities, true)



                }

            }

            else{
                result.addAll(DBProjectHelper().getAllProjects())
            }



            result = result.distinctBy { it.id }.toMutableList()


            result.sortBy { it.projectName }

            Result(result)
        } catch (ex: Exception) {
            //   JDELog.e("GetAllProductsInteractor exception: {${ex.message}}")
            Result(null, ex)
        }
    }


}

class GetProjectsByNameInteractor : BaseInteractor<ProjectNameFilter, Result<List<Project>>> {

    override fun syncCall(params: ProjectNameFilter?): Result<List<Project>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Project>()
            result.addAll(DBProjectHelper().getProjectByName(params.projectName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.projectName }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}


class GetProjectsByIdInteractor : BaseInteractor<ProjectNameFilter, Result<List<Project>>> {

    override fun syncCall(params: ProjectNameFilter?): Result<List<Project>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Project>()
            result.addAll(DBProjectHelper().getProjectsById(params.projectName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.projectName }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}

class GetPendingProjectsInteractor : BaseInteractor<Unit, Result<List<Project>>>{
    override fun syncCall(params: Unit?): Result<List<Project>> {
        return try {
            var result = mutableListOf<Project>()
            result.addAll(DBProjectHelper().getAllPendingProjects())
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.projectName }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }

}

class ProjectsInteractor (private val client: RestClient,
private val data: String) {



    private suspend fun sendProject(): Any {
        return GlobalScope.async(Dispatchers.IO) {
            // make network call
            try {



                val syncRequest = RestRequest(
                    RestRequest.RestMethod.POST,
                    NetworkConstant.SALESFORCE_PROJECT_PATH,
                    RequestBody.create(MediaType.parse("application/json"), data)
                )


                //JDELog.d("UDE", params.toString())


                val syncResponse = client.sendSync(syncRequest)

                if (syncResponse.isSuccess) {
//                with(JDEDBProductHelper(store)) {

                    Log.i("TAG", "sendProject: ")
                } else {
                    throw IllegalStateException("Response not success: " + syncResponse.statusCode)
                }

            }
            catch (e: Exception) {
              //  Logger.error(e.message)
            }
        }.await()
    }

    fun syncCall() {


        GlobalScope.launch(Dispatchers.Main) {
            sendProject()
        }

    }


}