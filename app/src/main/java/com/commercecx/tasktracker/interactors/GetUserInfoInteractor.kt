package com.commercecx.tasktracker.interactors

import com.commercecx.tasktracker.model.UserInfo
import com.commercecx.tasktracker.model.database.DBUserHelper


class GetUserInfoInteractor : BaseInteractor<Unit, Result<UserInfo>> {

    override fun syncCall(params: Unit?): Result<UserInfo> {
        return try {
            Result(DBUserHelper().getUserData())
        } catch (ex: Exception) {
            //JDELog.e("GetUserInfoInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}