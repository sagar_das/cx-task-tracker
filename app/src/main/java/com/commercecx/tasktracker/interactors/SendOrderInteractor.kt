package com.commercecx.tasktracker.interactors

import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.rest.RestRequest
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject

class SendOrderInteractor(private val client: RestClient) :
    BaseInteractor<JSONObject, Result<Unit>> {

    override fun syncCall(params: JSONObject?): Result<Unit> {
        try {
            if (params == null) {
                throw IllegalArgumentException("No params provided")
            }

            val syncRequest = RestRequest(
                RestRequest.RestMethod.POST,
                "/services/apexrest/RSOOrder/CreateOrder",
                RequestBody.create(MediaType.parse("application/json"), params.toString())
            )


            //JDELog.d("UDE", params.toString())


            val syncResponse = client.sendSync(syncRequest)

            if (syncResponse.isSuccess) {
//                with(JDEDBProductHelper(store)) {

                //TODO: We''e supposed to store the orders not send them to backendz
                return Result(Unit)
            } else {
                throw IllegalStateException("Response not success: " + syncResponse.statusCode)
            }

        } catch (e: Exception) {
            return Result(null, e)
        }
    }
}