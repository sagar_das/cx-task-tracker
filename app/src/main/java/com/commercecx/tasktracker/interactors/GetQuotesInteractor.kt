package com.commercecx.tasktracker.interactors

import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.model.Quote
import com.commercecx.tasktracker.model.QuoteInfo
import com.commercecx.tasktracker.model.database.DBQuoteHelper
import com.commercecx.tasktracker.network.NetworkConstant
import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.rest.RestRequest

/**
 * Created by Sagar Das on 11/17/20.
 */
data class QuoteNameFilter(val quoteName: String)

class GetQuotesInteractor(private val client: RestClient, val quoteId: String?) : BaseInteractor<Unit, Result<List<Quote>>> {

    override fun syncCall(params: Unit?): Result<List<Quote>> {

        return try {
            var result = mutableListOf<Quote>()




            result.addAll(DBQuoteHelper().getQuotesById(quoteId!!))
            result = result.distinctBy { it.id }.toMutableList()


            result.sortBy { it.name }

            //Fetching from salesforce
            if(result.size == 0){

                val quoteRequest = RestRequest.getRequestForQuery(
                    BaseApplication.instance.getString(
                        R.string.salesforce_api_version), NetworkConstant.QUOTES_GET_QUERY + " WHERE Id = '"+quoteId+"'")



                val quoteResponse = client.sendSync(quoteRequest)

                if(quoteResponse.isSuccess){
                    val response = quoteResponse.asJSONObject().getJSONArray("records")
                    val quoteInfo = QuoteInfo.Mapper.from(
                        response
                    )

//                    if (isInitialLoad(prefs)) {

                    DBQuoteHelper().insertQuotes(quoteInfo.quotes)
//                    } else {
//
//                        DBPricingTaskHelper().updatePricingTasks(pricingTaskInfo.pricingTasks)
//                    }
//                    storeSyncDate(prefs)

                    result.addAll(quoteInfo.quotes)


                }


            }

            Result(result)
        } catch (ex: Exception) {
            //   JDELog.e("GetAllProductsInteractor exception: {${ex.message}}")
            Result(null, ex)
        }
    }
}

class GetQuotesByNameInteractor : BaseInteractor<QuoteNameFilter, Result<List<Quote>>> {

    override fun syncCall(params: QuoteNameFilter?): Result<List<Quote>> {

        if (params == null) {
            throw IllegalArgumentException()
        }

        return try {
            var result = mutableListOf<Quote>()
            result.addAll(DBQuoteHelper().getQuoteByName(params.quoteName))
            result = result.distinctBy { it.id }.toMutableList()
            result.sortBy { it.name }

            Result(result)
        } catch (ex: Exception) {
            //JDELog.e("GetAllProductsInteractor exception: ${ex.message}")
            Result(null, ex)
        }
    }
}