package com.commercecx.tasktracker.interactors
//
//import android.util.Log
//import com.commercecx.tasktracker.application.BaseApplication
//import com.commercecx.tasktracker.model.dao.ProjectDBDao
//import com.commercecx.tasktracker.model.database.DBProjectHelper
//import com.salesforce.androidsdk.mobilesync.app.MobileSyncSDKManager
//import com.salesforce.androidsdk.mobilesync.manager.SyncManager
//import com.salesforce.androidsdk.mobilesync.manager.SyncManager.MobileSyncException
//import com.salesforce.androidsdk.mobilesync.manager.SyncManager.SyncUpdateCallback
//import com.salesforce.androidsdk.mobilesync.target.SyncUpTarget
//import com.salesforce.androidsdk.mobilesync.util.SyncOptions
//import com.salesforce.androidsdk.mobilesync.util.SyncState
//import org.json.JSONException
//import java.lang.Exception
//import java.util.*
//
//
///**
// * Created by Sagar Das on 11/23/20.
// */
//class ProjectSync  {
//
//    private lateinit var syncManager: SyncManager
//    private val TAG = "ProjectSync"
//
//    init {
//
//        val mobileSyncSDKManager = MobileSyncSDKManager.getInstance()
//
//        syncManager = SyncManager.getInstance(BaseApplication.instance.userAccount)
//        syncManager.smartStore.allSoupNames
//
////        // Setup schema if needed
// //       mobileSyncSDKManager.setupUserStoreFromDefaultConfig();
////        // Setup syncs if needed
// //       mobileSyncSDKManager.setupUserSyncsFromDefaultConfig();
//
//    }
//
//
//    /**
//     * Pushes local changes up to the server.
//     */
//   // @Synchronized
//    fun syncUp() {
//        try {
//            syncManager.reSync("syncUpProjects" /* see usersyncs.json */,
//                SyncUpdateCallback { sync ->
//                    if (SyncState.Status.DONE == sync.status) {
//                        //syncDown()
//                        Log.i(TAG, "DOne")
//
//                    }
//                })
//        } catch (e: JSONException) {
//            Log.e(
//                TAG,
//                "JSONException occurred while parsing",
//                e
//            )
//        } catch (e: MobileSyncException) {
//            Log.e(
//                TAG,
//                "MobileSyncException occurred while attempting to sync up",
//                e
//            )
//        }
//        catch(e: Exception){
//            Log.e(
//                TAG,
//                "Error",
//                e
//            )
//        }
//
////        val syncUpTarget = SyncUpTarget()
////        val fieldList = mutableListOf<String>()
////        fieldList.add("Id")
////        fieldList.add("Name")
////        fieldList.add("pse__Account__c")
////        fieldList.add("Budget_Amount__c")
////        fieldList.add("Budget_Status__c")
////        fieldList.add("Quote_Proposal__c")
////        val options = SyncOptions.optionsForSyncUp(fieldList, SyncState.MergeMode.LEAVE_IF_CHANGED)
////
////
////                try {
////            syncManager.syncUp(syncUpTarget, options, DBProjectHelper.TABLE_NAME ,
////                SyncUpdateCallback { sync ->
////                    if (SyncState.Status.DONE == sync.status) {
////                        //syncDown()
////                    }
////                })
////        } catch (e: JSONException) {
////            Log.e(
////                TAG,
////                "JSONException occurred while parsing",
////                e
////            )
////        } catch (e: MobileSyncException) {
////            Log.e(
////                TAG,
////                "MobileSyncException occurred while attempting to sync up",
////                e
////            )
////        }
////        catch(e: Exception){
////            Log.e(
////                TAG,
////                "Error",
////                e
////            )
////        }
//    }
//
//}