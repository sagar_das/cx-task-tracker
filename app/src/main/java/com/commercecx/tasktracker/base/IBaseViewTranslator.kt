package com.commercecx.tasktracker.base

/**
 * Created by Sagar Das on 8/20/20.
 */

import android.content.Context
import android.content.res.Resources
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle


/**
 * MVP view for the mobgen UI representations.
 */
interface IBaseViewTranslator {

    /**
     * The current view context. It provides the context of an activity, never the application
     * one.

     * @return The context to provide.
     */
    fun context(): Context?

    /**
     * The resources where we can take some items.

     * @return The resources.
     */
    fun resources(): Resources

    /**
     * Provides the lifecycle object.

     * @return The lifecycle.
     */
    fun lifecycle(): Lifecycle?

    /**
     * Provides the support fragment manager of the view.

     * @return The support fragment manager of the view.
     */
    fun getSupportFragmentManager(): FragmentManager?

    /**
     * Display a message in the UI.
     * @param text   The text string represented in human readable form.
     */
    fun showMessage(text: String)
}
