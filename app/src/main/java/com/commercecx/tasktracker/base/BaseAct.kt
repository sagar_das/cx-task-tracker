package com.commercecx.tasktracker.base

/**
 * Created by Sagar Das on 8/20/20.
 */

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.google.android.material.snackbar.Snackbar


/**
 * Base activity that supports MVP.
 */
abstract class BaseAct<out T : IBasePresenter> : AppCompatActivity(), IBaseViewTranslator {

    /**
     * The current presenter.
     */
    private lateinit var presenter: T

    /**
     * Method that must provide a presenter item.

     * @return The presenter provided.
     */
    protected abstract fun createPresenter(): T

    /**
     * Provides the presenter.

     * @return The presenter provided.
     */
    fun presenter(): T = presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()
        presenter.onCreate(savedInstanceState, intent.extras)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        presenter.onReady()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()

    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }


    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        presenter.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        presenter.onRestoreInstanceState(savedInstanceState)
    }


    override fun context(): Context = applicationContext

    override fun resources(): Resources = resources

    override fun lifecycle(): Lifecycle = lifecycle

    override fun showMessage(text: String) {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }
}
