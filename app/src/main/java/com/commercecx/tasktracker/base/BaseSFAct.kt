package com.commercecx.tasktracker.base

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.salesforce.androidsdk.smartstore.app.SmartStoreSDKManager
import com.salesforce.androidsdk.smartstore.store.SmartStore
import com.salesforce.androidsdk.ui.SalesforceActivityInterface

/**
 * Created by Sagar Das on 8/24/20.
 */
abstract class BaseSFAct<out T : IBasePresenter> : AppCompatActivity(), IBaseViewTranslator, SalesforceActivityInterface{



    private lateinit var presenter: T

    override fun context(): Context? = applicationContext

    override fun lifecycle(): Lifecycle? = lifecycle

    override fun resources(): Resources = resources



    fun getSmartStore(): SmartStore = SmartStoreSDKManager.getInstance().getGlobalSmartStore("CX")

    fun getSmartStoreManager(): SmartStoreSDKManager = SmartStoreSDKManager.getInstance()


    override fun showMessage(text: String) {
      //  view?.let {
       //     Snackbar.make(it, text, Snackbar.LENGTH_LONG).show()
      //  }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()
        presenter.onCreate(savedInstanceState, null)
    }



    abstract fun createPresenter(): T

    fun presenter(): T = presenter

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        presenter.onSaveInstanceState(outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

}