package com.commercecx.tasktracker.base

/**
 * Created by Sagar Das on 8/20/20.
 */
import android.content.Intent
import android.os.Bundle
import java.lang.ref.WeakReference

/**
 * Base class for the presenter.
 */
abstract class BasePresenter<out T : IBaseViewTranslator>
/**
 * The base constructor with the view translator.

 * @param view The view translator of the MVP.
 */
    (private val view: WeakReference<T>) : IBasePresenter {

    /**
     * Provides the view.

     * @return The current view.
     */
    fun view(): T? = view.get()

    override fun onCreate(savedInstanceState: Bundle?, extras: Bundle?) {}

    override fun onReady() {}

    override fun onStart() {}

    override fun onResume() {}

    override fun onPause() {}

    override fun onStop() {}

    override fun onDestroy() {}

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {}

    override fun onSaveInstanceState(savedInstanceState: Bundle?) {}

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}
}
