package com.commercecx.tasktracker.base

/**
 * Created by Sagar Das on 8/20/20.
 */

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import com.google.android.material.snackbar.Snackbar

import com.salesforce.androidsdk.smartstore.app.SmartStoreSDKManager
import com.salesforce.androidsdk.smartstore.store.SmartStore

abstract class BaseSalesForceFragment<out T : IBasePresenter> : Fragment(), IBaseViewTranslator {

    protected var baseActivity: BaseSalesForceActivity<*>? = null

    private lateinit var presenter: T

    override fun context(): Context? = activity?.applicationContext

    override fun lifecycle(): Lifecycle? = activity?.lifecycle

    override fun resources(): Resources = resources

    override fun getSupportFragmentManager(): FragmentManager? = activity?.supportFragmentManager

    fun getSmartStore(): SmartStore = SmartStoreSDKManager.getInstance().getGlobalSmartStore("CX")

    fun getSmartStoreManager(): SmartStoreSDKManager = SmartStoreSDKManager.getInstance()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        baseActivity = context as? BaseSalesForceActivity<*>
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    override fun showMessage(text: String) {
        view?.let {
            Snackbar.make(it, text, Snackbar.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()
        presenter.onCreate(savedInstanceState, arguments)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.onRestoreInstanceState(savedInstanceState)
        presenter.onReady()
    }

    abstract fun createPresenter(): T

    fun presenter(): T = presenter

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        presenter.onSaveInstanceState(outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}
