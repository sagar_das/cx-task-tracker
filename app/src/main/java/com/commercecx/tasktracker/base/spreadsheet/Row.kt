package com.commercecx.tasktracker.base.spreadsheet

/**
 * Created by Sagar Das on 11/25/20.
 */
open class Row {

    var cellList : MutableList<Cell> = mutableListOf()

    var height = 60

    fun getCell(column : Int) : Cell {
        while(cellList.size <= column) {
            cellList.add(Cell())
        }
        return cellList[column]
    }

}