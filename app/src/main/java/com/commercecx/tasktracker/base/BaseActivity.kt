package com.commercecx.tasktracker.base

/**
 * Created by Sagar Das on 8/19/20.
 */

import android.app.FragmentManager
import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity

import com.commercecx.tasktracker.util.Logger
import org.json.JSONObject

open class BaseActivity : AppCompatActivity() {
    var baseFragmentManager: FragmentManager? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseFragmentManager = fragmentManager
    }

    protected fun launchActivity(activity: Class<*>?) {
        startActivity(Intent(this@BaseActivity, activity))
    }

    protected fun startActivityForResult(activity: Class<*>?, requestCode: Int) {
        startActivityForResult(Intent(this@BaseActivity, activity), requestCode)
    }

    protected fun startActivityForResult(activity: Class<*>?, requestCode: Int, bundle: Bundle?) {
        val intent = Intent(this@BaseActivity, activity)
        intent.putExtras(bundle!!)
        startActivityForResult(intent, requestCode)
    }

    protected fun launchActivity(activity: Class<*>?, bundle: Bundle?) {
        val intent = Intent(this@BaseActivity, activity)
        intent.putExtras(bundle!!)
        startActivity(intent)
    }

    protected fun printParams(params: Map<String?, String?>?) {
        val jsonObject = JSONObject(params)
        Logger.info("PARAMS:: $jsonObject")
    }
}
