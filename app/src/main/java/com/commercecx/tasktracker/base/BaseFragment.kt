package com.commercecx.tasktracker.base

/**
 * Created by Sagar Das on 8/19/20.
 */



import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import java.lang.reflect.Field

open class BaseFragment : Fragment() {
    var baseFragmentManager: FragmentManager? = null
    protected var activity: Activity? = null

    companion object {
        private var sChildFragmentManagerField: Field? = null
        private const val TAG = "FRAGMENT"

        init {
            var f: Field? = null
            try {
                f = Fragment::class.java.getDeclaredField("mChildFragmentManager")
                f.isAccessible = true
            } catch (e: NoSuchFieldException) {
            }
            sChildFragmentManagerField = f
        }
    }

//    override fun onDetach() {
//        super.onDetach()
//        if (sChildFragmentManagerField != null) {
//            try {
//                sChildFragmentManagerField!![this] = null
//            } catch (e: Exception) {
//            }
//        }
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity()
        try {
            baseFragmentManager = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                childFragmentManager
            } else {
              fragmentManager
            }
        } catch (e: Exception) {
            baseFragmentManager = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                childFragmentManager
            } else {
                fragmentManager
            }
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "Setting screen name: BaseFragment")
    }

}
