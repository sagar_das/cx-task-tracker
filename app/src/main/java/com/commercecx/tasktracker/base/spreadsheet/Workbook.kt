package com.commercecx.tasktracker.base.spreadsheet

/**
 * Created by Sagar Das on 11/25/20.
 */

open class Workbook {

    var sheetList: MutableList<Sheet> = mutableListOf()

    var currentSheet : Int

    init {
        val sheet = Sheet()
        sheetList.add(sheet)
        currentSheet = 0
    }


}