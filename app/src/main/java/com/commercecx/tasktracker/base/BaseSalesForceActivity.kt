package com.commercecx.tasktracker.base

/**
 * Created by Sagar Das on 8/20/20.
 */

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle

import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import com.commercecx.tasktracker.application.BaseApplication
import com.google.android.material.snackbar.Snackbar

import com.salesforce.androidsdk.rest.RestClient
import com.salesforce.androidsdk.smartstore.app.SmartStoreSDKManager
import com.salesforce.androidsdk.smartstore.store.SmartStore
import com.salesforce.androidsdk.ui.SalesforceActivityDelegate
import com.salesforce.androidsdk.ui.SalesforceActivityInterface


/**
 * Base activity that supports MVP.
 */
abstract class BaseSalesForceActivity<out T : IBaseSalesForcePresenter> : AppCompatActivity(), IBaseViewTranslator, SalesforceActivityInterface {
    lateinit var delegate: SalesforceActivityDelegate

    /**
     * The current presenter.
     */
    private lateinit var presenter: T

    /**
     * Method that must provide a presenter item.

     * @return The presenter provided.
     */
    protected abstract fun createPresenter(): T

    /**
     * Provides the presenter.

     * @return The presenter provided.
     */
    fun presenter(): T = presenter

    fun getSmartStore(): SmartStore = SmartStoreSDKManager.getInstance().getGlobalSmartStore("CX")

    fun getSmartStoreManager(): SmartStoreSDKManager = SmartStoreSDKManager.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        delegate = SalesforceActivityDelegate(this)
        delegate.onCreate()
        presenter = createPresenter()
        presenter.onCreate(savedInstanceState, intent.extras)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        presenter.onReady()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()

    }

    override fun onResume() {
        super.onResume()
        delegate.onResume(true)
    }

    override fun onResume(client: RestClient?) {
        super.onResume()
        BaseApplication.instance.restClient = client
        presenter().onResume(client)
    }


    override fun onUserInteraction() {
        delegate.onUserInteraction()
    }

    override fun onPause() {
        presenter.onPause()
        delegate.onPause()
        super.onPause()
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        delegate.onDestroy()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        presenter.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        presenter.onRestoreInstanceState(savedInstanceState)
    }


    @Override
    override fun onUserSwitched() {
        delegate.onResume(true)
    }

    override fun context(): Context = applicationContext

    override fun resources(): Resources = resources

    override fun lifecycle(): Lifecycle = lifecycle


    override fun showMessage(text: String) {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return delegate.onKeyUp(keyCode, event) || super.onKeyUp(keyCode, event)
    }

    override fun onLogoutComplete() = Unit
}
