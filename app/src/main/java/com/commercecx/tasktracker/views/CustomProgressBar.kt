package com.commercecx.tasktracker.views

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.ProgressBar


class CustomProgressBar : ProgressBar {
    private var paint: Paint? = null
    var primary_progress = 0
    var max_value = 0

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    @Synchronized
    override fun setMax(max: Int) {
        max_value = max
        super.setMax(max)
    }

    @Synchronized
    override fun setProgress(progress: Int) {
        var progress = progress
        if (progress > max_value) {
            progress = max_value
        }
        primary_progress = progress
        super.setProgress(progress)
    }

    @Synchronized
    override fun setSecondaryProgress(secondaryProgress: Int) {
        var secondaryProgress = secondaryProgress
        if (primary_progress + secondaryProgress > max_value) {
            secondaryProgress = max_value - primary_progress
        }
        super.setSecondaryProgress(primary_progress + secondaryProgress)
    }

    private fun init() {
        paint = Paint()
        paint!!.color = Color.BLACK
        primary_progress = 0
        max_value = 100
    }
}
