package com.commercecx.tasktracker.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.util.Logger


/**
 * Created by Sagar Das on 8/19/20.
 */
class CustomButton : AppCompatButton {
    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        try {
            var value = 0
            if (attrs != null) {
                val a =
                    context.theme.obtainStyledAttributes(attrs, R.styleable.JDECustomButton, 0, 0)
                // Gets you the 'value' number - 0 or 666 in your example
                if (a.hasValue(R.styleable.JDECustomButton_jdeFontStyle)) {
                    value = a.getInt(R.styleable.JDECustomButton_jdeFontStyle, 0)
                }
                a.recycle()
            }
            var typeface = Typeface.createFromAsset(context.assets, FONT_ROBOTO_REGULAR)
            when (FontStyle.fromId(value)) {
                FontStyle.Bold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_ROBOTO_BOLD)
                FontStyle.Medium -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_ROBOTO_MEDIUM)
                FontStyle.SemiBold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_ROBOTO_SEMIBOLD)
                FontStyle.MediumItalic -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_ROBOTO_MEDIUM_ITALIC)
                FontStyle.Open_San_Regular -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_REGULAR)
                FontStyle.Open_San_SemiBold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_SEMIBOLD)
                FontStyle.Open_San_Bold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_BOLD)
                FontStyle.Open_San_Italic -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_ITALIC)
            }
            setTypeface(typeface)
        } catch (e: Exception) {
            Logger.error(TAG, e)
        }
    }

    internal enum class FontStyle(var style: Int) {
        None(0), Regular(1), Medium(2), SemiBold(3), Bold(4), MediumItalic(5), Open_San_Regular(6), Open_San_SemiBold(
            7
        ),
        Open_San_Bold(8), Open_San_Italic(9);

        companion object {
            fun fromId(style: Int): FontStyle {
                for (fontStyle in com.commercecx.tasktracker.views.CustomButton.FontStyle.values()) {
                    if (fontStyle.style == style) return fontStyle
                }
                throw IllegalArgumentException()
            }
        }
    }

    companion object {
        private const val FONT_ROBOTO_MEDIUM = "fonts/Roboto-Medium.ttf"
        private const val FONT_ROBOTO_REGULAR = "fonts/Roboto-Regular.ttf"
        private const val FONT_ROBOTO_SEMIBOLD = "fonts/Roboto-Black.ttf"
        private const val FONT_ROBOTO_BOLD = "fonts/Roboto-Bold.ttf"
        private const val FONT_ROBOTO_MEDIUM_ITALIC = "fonts/Roboto-MediumItalic.ttf"
        private const val FONT_OPEN_SAN_REGULAR = "fonts/OpenSans-Regular.ttf"
        private const val FONT_OPEN_SAN_SEMIBOLD = "fonts/OpenSans-Semibold.ttf"
        private const val FONT_OPEN_SAN_BOLD = "fonts/OpenSans-Bold.ttf"
        private const val FONT_OPEN_SAN_ITALIC = "fonts/OpenSans-Italic.ttf"
        private val TAG = CustomButton::class.java.simpleName
    }
}

