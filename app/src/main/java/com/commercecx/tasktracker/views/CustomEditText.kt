package com.commercecx.tasktracker.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.util.Logger


/**
 * Created by Sagar Das on 8/19/20.
 */
class CustomEditText : AppCompatEditText {
    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        try {
            var value = 0
            if (attrs != null) {
                val a =
                    context.theme.obtainStyledAttributes(attrs, R.styleable.JDECustomEditText, 0, 0)
                // Gets you the 'value' number - 0 or 666 in your example
                if (a.hasValue(R.styleable.JDECustomEditText_jdeFontStyle)) {
                    value = a.getInt(R.styleable.JDECustomEditText_jdeFontStyle, 0)
                }
                a.recycle()
            }
            var typeface = Typeface.createFromAsset(context.assets, FONT_ROBOTO_REGULAR)
            when (CustomButton.FontStyle.fromId(value)) {
                CustomButton.FontStyle.Bold -> typeface = Typeface.createFromAsset(context.assets, FONT_ROBOTO_BOLD)
                CustomButton.FontStyle.Medium -> typeface = Typeface.createFromAsset(context.assets, FONT_ROBOTO_MEDIUM)
                CustomButton.FontStyle.SemiBold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_ROBOTO_SEMIBOLD)
                CustomButton.FontStyle.MediumItalic -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_ROBOTO_MEDIUM_ITALIC)
                CustomButton.FontStyle.Open_San_Regular -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_REGULAR)
                CustomButton.FontStyle.Open_San_SemiBold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_SEMIBOLD)
                CustomButton.FontStyle.Open_San_Bold -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_BOLD)
                CustomButton.FontStyle.Open_San_Italic -> typeface =
                    Typeface.createFromAsset(context.assets, FONT_OPEN_SAN_ITALIC)
            }
            setTypeface(typeface)
        } catch (e: Exception) {
            Logger.error(TAG, e)
        }
    }

    companion object {
        private const val FONT_ROBOTO_MEDIUM = "fonts/Roboto-Medium.ttf"
        private const val FONT_ROBOTO_REGULAR = "fonts/Roboto-Regular.ttf"
        private const val FONT_ROBOTO_SEMIBOLD = "fonts/Roboto-Black.ttf"
        private const val FONT_ROBOTO_BOLD = "fonts/Roboto-Bold.ttf"
        private const val FONT_ROBOTO_MEDIUM_ITALIC = "fonts/Roboto-MediumItalic.ttf"
        private const val FONT_OPEN_SAN_REGULAR = "fonts/OpenSans-Regular.ttf"
        private const val FONT_OPEN_SAN_SEMIBOLD = "fonts/OpenSans-Semibold.ttf"
        private const val FONT_OPEN_SAN_BOLD = "fonts/OpenSans-Bold.ttf"
        private const val FONT_OPEN_SAN_ITALIC = "fonts/OpenSans-Italic.ttf"
        private val TAG = CustomEditText::class.java.simpleName
    }
}
