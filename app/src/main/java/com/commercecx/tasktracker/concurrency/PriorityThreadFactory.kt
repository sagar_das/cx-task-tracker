package com.commercecx.tasktracker.concurrency


import android.os.Process
import java.util.concurrent.ThreadFactory

/**
 * Created by Sagar Das on 8/25/20.
 */
class PriorityThreadFactory(threadPriority: Int) : ThreadFactory {
    private val mThreadPriority: Int
    override fun newThread(runnable: Runnable): Thread {
        val wrapperRunnable = Runnable {
            try {
                Process.setThreadPriority(mThreadPriority)
            } catch (t: Throwable) {
            }
            runnable.run()
        }
        return Thread(wrapperRunnable)
    }

    init {
        mThreadPriority = threadPriority
    }
}