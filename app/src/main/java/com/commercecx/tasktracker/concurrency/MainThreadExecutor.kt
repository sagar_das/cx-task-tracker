package com.commercecx.tasktracker.concurrency

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor


/**
 * Created by Sagar Das on 8/25/20.
 */
class MainThreadExecutor : Executor {
    private val handler: Handler = Handler(Looper.getMainLooper())
    override fun execute(runnable: Runnable?) {
        handler.post(runnable)
    }
}