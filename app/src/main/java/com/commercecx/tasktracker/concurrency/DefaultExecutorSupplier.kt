package com.commercecx.tasktracker.concurrency

import android.os.Process
import java.util.concurrent.*

/**
 * Created by Sagar Das on 8/25/20.
 */
class DefaultExecutorSupplier {

    /*
    * Number of cores to decide the number of threads
    */
    val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()

    /*
    * thread pool executor for background tasks
    */
    private var mForBackgroundTasks: ThreadPoolExecutor? = null

    /*
    * thread pool executor for light weight background tasks
    */
    private var mForLightWeightBackgroundTasks: ThreadPoolExecutor? = null

    /*
    * thread pool executor for main thread tasks
    */
    private var mMainThreadExecutor: Executor? = null

    /*
    * an instance of DefaultExecutorSupplier
    */
    private var sInstance: DefaultExecutorSupplier? = null

    fun getInstance(): DefaultExecutorSupplier? {
        if (sInstance == null) {
            synchronized(DefaultExecutorSupplier::class.java) {
                sInstance = DefaultExecutorSupplier()
            }
            return sInstance
        }

        return null
    }


    init{

        // setting the thread factory
        val backgroundPriorityThreadFactory: ThreadFactory =
            PriorityThreadFactory(Process.THREAD_PRIORITY_BACKGROUND)

        // setting the thread pool executor for mForBackgroundTasks;
        mForBackgroundTasks = ThreadPoolExecutor(
            NUMBER_OF_CORES * 2,
            NUMBER_OF_CORES * 2,
            60L,
            TimeUnit.SECONDS,
            LinkedBlockingQueue<Runnable>(),
            backgroundPriorityThreadFactory
        )

        // setting the thread pool executor for mForLightWeightBackgroundTasks;
        mForLightWeightBackgroundTasks = ThreadPoolExecutor(
            NUMBER_OF_CORES * 2,
            NUMBER_OF_CORES * 2,
            60L,
            TimeUnit.SECONDS,
            LinkedBlockingQueue<Runnable>(),
            backgroundPriorityThreadFactory
        )

        // setting the thread pool executor for mMainThreadExecutor;
        mMainThreadExecutor = MainThreadExecutor()
    }

    /*
    * returns the thread pool executor for background task
    */
    fun forBackgroundTasks(): ThreadPoolExecutor? {
        return mForBackgroundTasks
    }

    /*
    * returns the thread pool executor for light weight background task
    */
    fun forLightWeightBackgroundTasks(): ThreadPoolExecutor? {
        return mForLightWeightBackgroundTasks
    }

    /*
    * returns the thread pool executor for main thread task
    */
    fun forMainThreadTasks(): Executor? {
        return mMainThreadExecutor
    }

}