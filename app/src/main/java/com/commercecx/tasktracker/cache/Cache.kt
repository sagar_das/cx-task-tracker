package com.commercecx.tasktracker.cache

/**
 * Created by Sagar Das on 8/25/20.
 */
interface Cache {
    val size: Int

    operator fun set(key: Any, value: Any)

    operator fun get(key: Any): Any?

    fun remove(key: Any): Any?

    fun clear()
}