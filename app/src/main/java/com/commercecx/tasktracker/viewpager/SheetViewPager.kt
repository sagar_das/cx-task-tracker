package com.commercecx.tasktracker.viewpager

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/**
 * Created by Sagar Das on 11/25/20.
 */

class SheetViewPager : ViewPager {

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    // no swiping
    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return false
    }

}