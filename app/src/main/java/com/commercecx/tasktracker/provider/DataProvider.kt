package com.commercecx.tasktracker.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.MatrixCursor
import android.net.Uri
import android.provider.BaseColumns
import com.commercecx.tasktracker.model.DatabaseContract
import com.commercecx.tasktracker.model.OrderOld
import io.realm.Realm
import io.realm.RealmConfiguration
import java.util.*

class DataProvider : ContentProvider() {

    //region Variable Declaration
    private val ORDERS = 100
    private val ORDERS_WITH_ID = 101
    private val CUSTOMERS = 200
    private val CUSTOMERS_WITH_ID = 201
    private val NOTES = 300
    private val NOTES_WITH_ID = 301
    private val TRUCK = 400
    private val TRUCK_WITH_ID = 401
    //endregion

    //endregion
    //region URI Matchers
    private var sUriMatcher = UriMatcher(UriMatcher.NO_MATCH)




    init
    {
        sUriMatcher.addURI(
            "content://" + DatabaseContract.AUTHORITY,
            DatabaseContract.TABLE_ORDER,
            ORDERS
        )
        sUriMatcher.addURI(
            "content://" + DatabaseContract.AUTHORITY,
            DatabaseContract.TABLE_ORDER + "/#",
            ORDERS_WITH_ID
        )
        sUriMatcher.addURI(
            "content://" + DatabaseContract.AUTHORITY,
            DatabaseContract.TABLE_ROUTE,
            CUSTOMERS
        )
        sUriMatcher.addURI(
            "content://" + DatabaseContract.AUTHORITY,
            DatabaseContract.TABLE_ROUTE + "/#",
            CUSTOMERS_WITH_ID
        )
        sUriMatcher.addURI(
            "content://" + DatabaseContract.AUTHORITY,
            DatabaseContract.TABLE_NOTE,
            NOTES
        )
        sUriMatcher.addURI(
            "content://" + DatabaseContract.AUTHORITY,
            DatabaseContract.TABLE_NOTE + "/#",
            NOTES_WITH_ID
        )
        sUriMatcher.addURI(
            "content://" + DatabaseContract.AUTHORITY,
            DatabaseContract.TABLE_TRUCK,
            TRUCK
        )
        sUriMatcher.addURI(
            "content://" + DatabaseContract.AUTHORITY,
            DatabaseContract.TABLE_TRUCK + "/#",
            TRUCK_WITH_ID
        )
    }
    //endregion

    //endregion
    fun DataProvider() {}

    override fun onCreate(): Boolean {
        // TODO: Check if the Realm instance initialized through Application class will suffice
        Realm.init(context)
        val config = RealmConfiguration.Builder().name("RVR.realm")
            .schemaVersion(0)
            .deleteRealmIfMigrationNeeded().build()
        Realm.setDefaultConfiguration(config)
        return true
    }

    //region CRUD Operations

    //region CRUD Operations
    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        // Implement this to handle requests to delete one or more rows.
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        // TODO: Implement this to handle requests to insert a new row.
        throw UnsupportedOperationException("Not yet implemented")
    }


    override fun query(
        uri: Uri, projection: Array<String?>?, selection: String?,
        selectionArgs: Array<String?>?, sortOrder: String?
    ): Cursor? {
        var resultsCursor: MatrixCursor? = null
        var orders: ArrayList<OrderOld>? = null
        when (sUriMatcher.match(uri)) {
            ORDERS -> {
                resultsCursor = MatrixCursor(
                    arrayOf(
                        BaseColumns._ID,
                        DatabaseContract.OrderColumns.NAME,
                        DatabaseContract.OrderColumns.PRODUCTS,
                        DatabaseContract.OrderColumns.TOTAL,
                        DatabaseContract.OrderColumns.SUBTOTAL,
                        DatabaseContract.OrderColumns.TAX,
                        DatabaseContract.OrderColumns.DATE,
                        DatabaseContract.OrderColumns.IMAGE,
                        DatabaseContract.OrderColumns.TYPE
                    )
                )
                orders = ArrayList<OrderOld>(
                    Realm.getDefaultInstance().where(
                        OrderOld::class.java
                    ).findAll()
                )
                for (order in orders) {
                    val rowData = order.getName()?.let {
                        order.getProducts()?.let { it1 ->
                            arrayOf<Any>(
                                order.getId(),
                                it,
                                it1,
                                order.getTotal(),
                                order.getSub_total(),
                                order.getTax(),
                                order.getDate()!!,
                                order.getImage()!!,
                                order.getType()!!
                            )
                        }
                    }
                    resultsCursor.addRow(rowData)
                }
            }
            ORDERS_WITH_ID -> {
                val id = uri.pathSegments[1].toInt()
                val order: OrderOld =
                    Realm.getDefaultInstance().where(OrderOld::class.java).equalTo("id", id)
                        .findFirst()!!
                val rowData = order.getName()?.let {
                    order.getProducts()?.let { it1 ->
                        arrayOf<Any>(
                            order.getId(),
                            it,
                            it1,
                            order.getTotal(),
                            order.getSub_total(),
                            order.getTax(),
                            order.getDate()!!,
                            order.getImage()!!,
                            order.getType()!!
                        )
                    }
                }
                resultsCursor!!.addRow(rowData)
            }
        }
        return resultsCursor
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        // TODO: Implement this to handle requests to update one or more rows.
        throw UnsupportedOperationException("Not yet implemented")
    }

    //endregion
}
