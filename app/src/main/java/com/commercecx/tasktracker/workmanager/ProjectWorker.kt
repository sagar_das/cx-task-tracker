package com.commercecx.tasktracker.workmanager

import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.commercecx.tasktracker.R
import com.commercecx.tasktracker.application.BaseApplication
import com.commercecx.tasktracker.interactors.GetPendingProjectsInteractor
import com.commercecx.tasktracker.interactors.GetProjectsInteractor
import com.commercecx.tasktracker.model.Project

/**
 * Created by Sagar Das on 12/8/20.
 */
class ProjectWorker (val context: Context, workerParameters: WorkerParameters) :
    Worker(context, workerParameters) {
    override fun doWork(): Result {

        try{

            var pendingProjects: List<Project>? = null
            var existingProjects: MutableList<Project>? = null

            //Fetching from soup
            GetProjectsInteractor(BaseApplication.instance.restClient!!, false).execute {

                it.data?.let { data ->

                    existingProjects = data.toMutableList()

                }
            }

            //Fetching from server
            GetProjectsInteractor(BaseApplication.instance.restClient!!, true).execute {

                it.data?.let { data ->

                    var newProjects = data.toMutableList()

                    if(newProjects.size > existingProjects!!.size){

                        //Fetching pending projects

                        GetPendingProjectsInteractor().execute() {
                            //view()?.hideLoader()
                            it.data?.let { data ->
                                pendingProjects = data
                            }
                        }

                        createNotification()

                    }

                }
            }







            // Indicate whether the task finished successfully with the Result
            return Result.success()


        }
        catch (ex: Exception){

            return Result.failure()
        }


    }

    fun createNotification(){
        val nm = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notification = NotificationCompat.Builder(context, "first").apply {
            setContentTitle("New Projects")
            setContentText("New projects are pending for your action")
            setSmallIcon(R.drawable.trackerlogo)
            priority = NotificationCompat.PRIORITY_DEFAULT
        }.build()

        nm.notify(System.currentTimeMillis().toInt(), notification)
    }
}