package com.commercecx.tasktracker.network

import android.app.Fragment
import android.content.Context
import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import com.commercecx.tasktracker.model.Note
import io.realm.Realm
import okhttp3.*
import java.io.File
import java.net.HttpURLConnection
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Sagar Das on 8/19/20.
 */
class NetworkOperations {
    private var note: Note? = null
    private var filesList: List<String>? = null
    private var note_id: String? = null
    private var fileName: String? = null
    private var file: File? = null
    private var OPERATION_TYPE = ""
    var value: String? = null

    //OAuthTokens accessTokens;
    var context: Context? = null

    interface IRoutesFetch {
        fun routesFetchCompleted(response: String?)
    }

    interface INotesFetch {
        fun notesFetchCompleted(response: String?)
    }

    interface IFilesFetch {
        fun filesFetchCompleted()
    }

    private val mHandler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            when (msg.what) {
                1 -> {
                    mFilesFetch!!.filesFetchCompleted()
                    Log.i(TAG, "handleMessage: Download successful")
                }
                2 -> Log.i(TAG, "handleMessage: Upload successful")
                else -> Log.i(TAG, "handleMessage: Operation failed")
            }
        }
    }
    var mROutesFetch: IRoutesFetch? = null
    var mNotesFetch: INotesFetch? = null
    var mFilesFetch: IFilesFetch? = null

    constructor(context: Context?) {
        this.context = context
    }

    fun receiveRoutes(fragment: Fragment?) {
        OPERATION_TYPE = "ROUTES"
        mROutesFetch = fragment as IRoutesFetch?
        try {
            val date = Date()
            val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
            val strDate = dateFormat.format(date)
            okHttpClient = OkHttpClient()
            val MEDIA_TYPE = MediaType.parse("application/json")
            request = Request.Builder()
                .url(NetworkConstant.ROUTES_GET_URI + "?routeDate=" + strDate)
                .build()
            NetworkAsyncPost().execute()
        } catch (e: Exception) {
            Log.e(TAG, "query: ", e)
        }
    }

    fun receiveRoutesJob() {
        OPERATION_TYPE = "ROUTES_RECEIVER"
        try {
            var date = Date()
            val c = Calendar.getInstance()
            c.time = date
            c.add(Calendar.DATE, 1)
            date = c.time
            val dateFormat: DateFormat = SimpleDateFormat("MM-dd-yyyy")
            val strDate = dateFormat.format(date)
            okHttpClient = OkHttpClient()
            val MEDIA_TYPE = MediaType.parse("application/json")
            request = Request.Builder()
                .url(NetworkConstant.ROUTES_GET_URI + "?routeDate=" + strDate)
                .build()
            NetworkAsyncPost().execute()
        } catch (e: Exception) {
            Log.e(TAG, "query: ", e)
        }
    }

    fun receiveNotes(fragment: Fragment?) {
        OPERATION_TYPE = "NOTE_FETCH"
        mNotesFetch = fragment as INotesFetch?
        try {
            okHttpClient = OkHttpClient()
            val MEDIA_TYPE = MediaType.parse("application/json")
            request = Request.Builder()
                .url(NetworkConstant.NOTES_POST_URI)
                .build()
            NetworkAsyncPost().execute()
        } catch (e: Exception) {
            Log.e(TAG, "query: ", e)
        }
    }

    fun sendNoteToServer(note: Note, note_id: String?) {
        OPERATION_TYPE = "NOTE"
        this.note_id = note_id
        this.note = note
        var attachName: String? = " "
        var attachPath: String? = " "
        if (note.isAttachment()) {
            attachName = note.getAttachName()
            attachPath = note.getAttachPath()
        }
        val postData = """{
 "operation_type":"${note.getPendingOperation()}",
 "noteid":"${note.getId()}",
 "title":"${note.getName()}",
 "date":"${note.getDate()}",
 "contents":"${note.getRemark()}",
 "isAttachment":${note.isAttachment()},
 "attachName":"$attachName",
 "attachPath":"$attachPath"
    }"""
        Log.i(TAG, "sendNoteToServer: $postData")
        try {
            okHttpClient = OkHttpClient()
            val MEDIA_TYPE = MediaType.parse("application/json")
            val postBody = RequestBody.create(MEDIA_TYPE, postData)
            request = Request.Builder()
                .url(NetworkConstant.NOTES_POST_URI)
                .post(postBody)
                .build()
            NetworkAsyncPost().execute()
        } catch (e: Exception) {
            Log.e(TAG, "query: ", e)
        }
    }

    constructor() : super()

    fun sendFileToServer(fileName: String?, pathNAme: String?) {
        this.fileName = fileName
        file = File(pathNAme, fileName)
        val r = Runnable { uploadWithTransferUtility() }
        mHandler.post(r)
    }

    fun fetchFilesFromServer(filesList: List<String>?, fragment: Fragment?) {
        this.filesList = filesList
        mFilesFetch = fragment as IFilesFetch?
        val r = Runnable { downloadWithTransferUtility() }
        mHandler.post(r)
    }

    fun deleteNoteFRomServer(note_id: String) {
        OPERATION_TYPE = "NOTE_DELETE"
        this.note_id = note_id
        note = note
        val value = false
        val postData = """{
 "operation_type":"delete",
 "noteid":"$note_id",
 "title":" ",
 "date":" ",
 "contents":" ",
 "isAttachment":$value,
 "attachName":" ",
 "attachPath":" "
    }"""
        Log.i(TAG, "sendNoteToServer: $postData")
        try {
            okHttpClient = OkHttpClient()
            val MEDIA_TYPE = MediaType.parse("application/json")
            val postBody = RequestBody.create(MEDIA_TYPE, postData)
            request = Request.Builder()
                .url(NetworkConstant.NOTES_POST_URI)
                .post(postBody)
                .build()
            NetworkAsyncPost().execute()
        } catch (e: Exception) {
            Log.e(TAG, "query: ", e)
        }
    }

    internal inner class NetworkAsyncPost :
        AsyncTask<Void?, Void?, String>() {
        override fun doInBackground(vararg params: Void?): String? {
            value = null
            var result = "FAILED"
            try {
                val res = okHttpClient!!.newCall(request).execute()
                value = res.body()!!.string()
                Log.i(TAG, "doInBackground: $value")
                if (!value!!.contains("error")) {
                    result = "SUCCESS"
                }
            } catch (e: Exception) {
                Log.e(TAG, "doInBackground: ", e)
            }
            return result
        }

        override fun onPostExecute(result: String) {
            when (OPERATION_TYPE) {
                "NOTE" -> when (result) {
                    "SUCCESS" -> updateNoteStatus()
                    else -> {
                    }
                }
                "ROUTES" -> when (result) {
                    "SUCCESS" -> mROutesFetch!!.routesFetchCompleted(value)
                    else -> {
                    }
                }
                "NOTE_FETCH" -> when (result) {
                    "SUCCESS" -> mNotesFetch!!.notesFetchCompleted(value)
                    else -> {
                    }
                }
//                "ROUTES_RECEIVER" -> when (result) {
//                    "SUCCESS" -> saveToLocalDB(value)
//                    else -> {
//                    }
//                }
            }
        }
    }

    private fun updateNoteStatus() {
        val realm = Realm.getDefaultInstance()
        if (note_id != null) {
            realm.executeTransactionAsync { realm ->
                val note = realm.where(Note::class.java).contains("id", note_id).findFirst()
                note!!.setServerCommitted(true)
                realm.insertOrUpdate(note)
                Log.i(TAG, "execute: ")
            }
        } else {
            realm.executeTransactionAsync { realm ->
                note!!.setServerCommitted(true)
                realm.insertOrUpdate(note)
                Log.i(TAG, "execute: ")
            }
        }
    }

    fun downloadWithTransferUtility() {

//        AWSMobileClient.getInstance().initialize(context).execute();
//
//        TransferUtility transferUtility =
//                TransferUtility.builder()
//                        .context(context)
//                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
//                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
//                        .build();
//
//        ContextWrapper contextWrapper = new ContextWrapper(context);
//        File directory = contextWrapper.getDir("NoteAttachments", Context.MODE_PRIVATE);
//
//
//
//        for(int i=0;i<filesList.size();i++) {
//
//            final int position = i;
//            fileName = filesList.get(i);
//
//            final File file = new File(directory, fileName);
//
//            TransferObserver downloadObserver =
//                    transferUtility.download(
//                            "lmx-notes/" + fileName,
//                            file);
//
//            // Attach a listener to the observer to get state update and progress notifications
//            downloadObserver.setTransferListener(new TransferListener() {
//
//                @Override
//                public void onStateChanged(int id, TransferState state) {
//                    if (TransferState.COMPLETED == state) {
//                        // Handle a completed upload.
//                        Log.i(TAG, "onStateChanged: completed ");
//                        FileOutputStream fos = null;
//                        try {
//                            Bitmap bitmapImage = BitmapFactory.decodeFile(file.getAbsolutePath());
//                            fos = new FileOutputStream(file);
//                            bitmapImage.compress(Bitmap.CompressFormat.PNG,100,fos);
//                          //  BufferedWriter b = new BufferedWriter(new FileWriter(file));
//
//                            Log.i(TAG, "onStateChanged: ");
//                        } catch (Exception e) {
//                            Log.e(TAG, "onStateChanged: ", e);
//                        }
//                        finally {
//                            try{
//                                fos.close();
//                            }
//                            catch (Exception e){
//                                Log.e(TAG, "Error closing file output stream ",e );
//                            }
//                        }
//
//                        if(position==filesList.size()-1){
//                            Message m = new Message();
//                            m.what = FILE_DOWNLOAD_SUCCESS;
//                            mHandler.sendMessage(m);
//                        }
//                    }
//                    Log.i(TAG, "onStateChanged: " + state.name());
//                }
//
//                @Override
//                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
//                    int percentDone = (int) percentDonef;
//
//                    Log.d(TAG, "   ID:" + id + "   bytesCurrent: " + bytesCurrent + "   bytesTotal: " + bytesTotal + " " + percentDone + "%");
//                }
//
//                @Override
//                public void onError(int id, Exception ex) {
//                    // Handle errors
//                    Log.e(TAG, "AWS download onError: ", ex);
//                }
//
//            });
//
//
//            // If you prefer to poll for the data, instead of attaching a
//            // listener, check for the state and progress in the observer.
//            if (TransferState.COMPLETED == downloadObserver.getState()) {
//                // Handle a completed upload.
//                Log.i(TAG, "downloadWithTransferUtility: Completed");
//            }
//        }

//        Log.d("YourActivity", "Bytes Transferred: " + downloadObserver.getBytesTransferred());
//        Log.d("YourActivity", "Bytes Total: " + downloadObserver.getBytesTotal());
    }

    fun uploadWithTransferUtility() {

//        AWSMobileClient.getInstance().initialize(context).execute();
//        TransferUtility transferUtility =
//                TransferUtility.builder()
//                        .context(context)
//                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
//                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
//                        .build();
//
//        TransferObserver uploadObserver =
//                transferUtility.upload(
//                        "lmx-notes/"+fileName,
//                        file);
//
//        // Attach a listener to the observer to get state update and progress notifications
//        uploadObserver.setTransferListener(new TransferListener() {
//
//            @Override
//            public void onStateChanged(int id, TransferState state) {
//                if (TransferState.COMPLETED == state) {
//                    // Handle a completed upload.
//                    Message m = new Message();
//                    m.what = FILE_UPLOAD_SUCCESS;
//                    mHandler.sendMessage(m);
//                }
//            }
//
//            @Override
//            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
//                int percentDone = (int)percentDonef;
//
//                Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent
//                        + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
//            }
//
//            @Override
//            public void onError(int id, Exception ex) {
//                // Handle errors
//            }
//
//        });
//
//        // If you prefer to poll for the data, instead of attaching a
//        // listener, check for the state and progress in the observer.
//        if (TransferState.COMPLETED == uploadObserver.getState()) {
//            // Handle a completed upload.
//        }
//
//        Log.d("YourActivity", "Bytes Transferred: " + uploadObserver.getBytesTransferred());
//        Log.d("YourActivity", "Bytes Total: " + uploadObserver.getBytesTotal());
    }

//    private fun saveToLocalDB(response: String?) {
//        try {
//            val jsonObject = JSONObject(response)
//            val jsonArray = jsonObject.getJSONArray("Items")
//            val gsonBuilder = GsonBuilder()
//            val gson = gsonBuilder.create()
//            val realm = Realm.getDefaultInstance()
//            val routes: List<Route> = MutableList<Route>(
//                Arrays.asList<Array<Route>>(
//                    *gson.fromJson<Array<Route>>(
//                        jsonArray.toString(),
//                        Array<Route>::class.java
//                    )
//                )
//            )
//            for (route in routes) {
//                realm.executeTransactionAsync { realm -> realm.insertOrUpdate(route) }
//            }
//        } catch (e: Exception) {
//            Log.e(TAG, "saveToLocalDB: ", e)
//        }
//    }

    companion object {
        private val TAG = NetworkOperations::class.java.simpleName
        private const val FILE_DOWNLOAD_SUCCESS = 1
        private const val FILE_UPLOAD_SUCCESS = 2
        var urlConnection: HttpURLConnection? = null
        var okHttpClient: OkHttpClient? = null
        var request: Request? = null
    }
}
