package com.commercecx.tasktracker.network

/**
 * Created by Sagar Das on 8/19/20.
 */
object NetworkConstant {
    const val NOTES_POST_URI =
        "https://68sejjo60f.execute-api.us-east-2.amazonaws.com/Testing/notes"
    const val ROUTES_GET_URI = "https://fjs1izsaec.execute-api.us-east-2.amazonaws.com/Test/notes"
    const val SALESFORCE_PRODUCT_PATH = "/services/apexrest/productRest/0051U000007hVjhQAE"
    const val SALESFORCE_ORDER_PATH = "/services/apexrest/productRest"
    const val SALESFORCE_PROJECT_PATH = "/services/apexrest/Project"
    const val SALESFORCE_PROJECTTASK_PATH = "/services/apexrest/ProjectTask"
 //   const val QUOTES_GET_QUERY = "SELECT BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,Id,Name,QuoteNumber,ShippingCity,ShippingCountry,ShippingPostalCode,ShippingState,ShippingStreet FROM Quote"
 const val QUOTES_GET_QUERY = "SELECT Id,Name FROM Apttus_Proposal__Proposal__c"

    const val PROJECTS_GET_QUERY = "SELECT Id, Name, pse__Account__c, Budget_Amount__c, Budget_Status__c, Quote_Proposal__c FROM pse__Proj__c"
 //   const val PRICINGTASK_GET_QUERY = "SELECT Task_Id__c, Name, Project__c, Budget_Hours__c, Role__c FROM Pricing_Task__c"
    const val PRICINGTASK_GET_QUERY = "SELECT Id, Name, pse__Project__c, Hours_Per_Unit__c, Number_of_Units__c, Product__c, Delivery_Country__c FROM pse__Project_Task__c LIMIT 100"
 //const val PRICINGTASK_GET_QUERY = "SELECT Id, Name, Project__c, Hours_Per_Unit__c, Number_of_Units__c, Product__c, Delivery_Country__c FROM Project_Task__c LIMIT 100"


}
