package com.commercecx.tasktracker.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Sagar Das on 8/19/20.
 */


open class ProductOld : RealmObject() {
    /*
    {
      "_id": "5b2d202faf2a8d395eb3e42b",
      "index": 0,
      "name": "ipsum fugiat commodo",
      "weight": "20 x 100 cups",
      "date": "Thu May 29 2014 23:45:25 GMT+0000 (UTC)"
    }
     */
    @PrimaryKey
    @SerializedName("_id")
    private var id: String? = null

    @SerializedName("index")
    private var index = 0

    @SerializedName("guid")
    private var guid: String? = null

    @SerializedName("name")
    private var name: String? = null

    @SerializedName("item_qty")
    private var item_qty: String? = null

    @SerializedName("item_price")
    private var item_price: String? = null

    @SerializedName("item_packs")
    private var item_pack: String? = null

    @SerializedName("item_tax")
    private var item_tax: String? = null

    @SerializedName("date")
    private var date: String? = null

    @SerializedName("image")
    private var image: String? = null
    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getIndex(): Int {
        return index
    }

    fun setIndex(index: Int) {
        this.index = index
    }

    fun getGuid(): String? {
        return guid
    }

    fun setGuid(guid: String?) {
        this.guid = guid
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getItem_qty(): String? {
        return item_qty
    }

    fun setItem_qty(item_qty: String?) {
        this.item_qty = item_qty
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String?) {
        this.date = date
    }

    fun getItem_price(): String? {
        return item_price
    }

    fun setItem_price(item_price: String?) {
        this.item_price = item_price
    }

    fun getItem_pack(): String? {
        return item_pack
    }

    fun setItem_pack(item_pack: String?) {
        this.item_pack = item_pack
    }

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String?) {
        this.image = image
    }

    fun getItem_tax(): String? {
        return item_tax
    }

    fun setItem_tax(item_tax: String?) {
        this.item_tax = item_tax
    }
}
