package com.commercecx.tasktracker.model

import android.provider.BaseColumns

/**
 * Created by Sagar Das on 8/19/20.
 */


object DatabaseContract {
    const val AUTHORITY = "com.jde.org.provider"
    const val TABLE_ORDER = "order"
    const val TABLE_ROUTE = "route"
    const val TABLE_NOTE = "note"
    const val TABLE_TRUCK = "truck"

    object OrderColumns : BaseColumns {
        const val NAME = "name"
        const val PRODUCTS = "products"
        const val TOTAL = "total"
        const val SUBTOTAL = "subtotal"
        const val TAX = "tax"
        const val DATE = "date"
        const val IMAGE = "products"
        const val TYPE = "type"
    }

    object NoteColumns : BaseColumns {
        const val NAME = "name"
        const val REMARK = "remark"
        const val INDEX = "index"
        const val GUID = "guid"
        const val ISACTIVE = "isactive"
        const val ISATTACHMENT = "isattachment"
        const val DATE = "date"
        const val TYPE = "type"
    }

    object RouteColumns : BaseColumns {
        const val NAME = "name"
        const val ADDRESS = "address"
        const val INDEX = "index"
        const val GUID = "guid"
        const val ISACTIVE = "isactive"
        const val IMAGE = "image"
        const val DATE = "date"
    }

    object TruckColumns : BaseColumns {
        const val NAME = "name"
        const val INDEX = "index"
        const val GUID = "guid"
        const val ITEMWEIGHT = "item_weight"
        const val ITEMPRICE = "item_price"
        const val ITEMPACK = "item_packs"
        const val DATE = "date"
    }
}
