package com.commercecx.tasktracker.model.database

//import com.commercecx.tasktracker.interactors.ProjectSync
import android.util.Log
import com.commercecx.tasktracker.model.Project
import com.commercecx.tasktracker.model.dao.ProjectDBDao
import com.salesforce.androidsdk.smartstore.store.QuerySpec

/**
 * Created by Sagar Das on 11/18/20.
 */

class DBProjectHelper : BaseHelper() {
    companion object {
        const val TABLE_NAME = "PROJECTS"

    }


    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = ProjectDBDao.getIndexSpec()


    fun insertProjects(projects: List<Project>) {
        clearTable()
        dropTable()
        getSmartStore().registerSoup(TABLE_NAME, ProjectDBDao.getIndexSpec())
        projects.forEach { getSmartStore().create(TABLE_NAME, ProjectDBDao(it)) }
        //JDELog.e("inserted, there are... {${getAllProjects().size}} projects in table")
    }

    fun updateProjects(projects: List<Project>, clearSoup: Boolean) {

        if(clearSoup){
            clearTable()
        }
        projects.forEach {
            try {
                val id = insertOrUpdate(ProjectDBDao.ID, ProjectDBDao(it))
                Log.i("TAG", "updateProjects: ")

//                if (syncToServer) {
//
//                    val projectSync = ProjectSync()
//                    projectSync.syncUp()
//
//                }
            }
            catch (e: Exception){
                Log.i("TAG", "updateProjects: ")
//                if (syncToServer) {
//
//                    val projectSync = ProjectSync()
//                    projectSync.syncUp()
//
//                }
            }
        }
        //JDELog.e("updated projects, there are... {${getAllProjects().size}} projects in table")
    }

    fun upsertProjects(projects: List<Project>, syncToServer: Boolean) {
        projects.forEach {
            try {
                upsert(ProjectDBDao(it))


            }
            catch (e: Exception){
                if (syncToServer) {

      //              val projectSync = ProjectSync()
      //              projectSync.syncUp()

                }
            }
        }

        if (syncToServer) {

       //     val projectSync = ProjectSync()
        //    projectSync.syncUp()

        }
        //JDELog.e("updated projects, there are... {${getAllProjects().size}} projects in table")
    }

    fun getAllProjects(): List<Project> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, ProjectDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)

        return Project.Mapper.fromDB(result)

    }

    /*
   buildLikeQuerySpec(String soupName, String path, String likeKey, String orderPath, Order order, int pageSize)
   buildLikeQuerySpec(String soupName, String[] selectPaths, String path, String likeKey, String orderPath, Order order, int pageSize) {

    */
    fun getProjectByName(accountName: String): List<Project> {
        val result = getSmartStore().query(
            QuerySpec.buildLikeQuerySpec(
                TABLE_NAME,
                ProjectDBDao.NAME, accountName, ProjectDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return Project.Mapper.fromDB(result)
    }

    fun getAllPendingProjects(): List<Project> {
        val result = getSmartStore().query(
            QuerySpec.buildLikeQuerySpec(
                TABLE_NAME,
                ProjectDBDao.PROJECTSTATUS, "", ProjectDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return Project.Mapper.fromDB(result)
    }




    fun getProjectsById(accountId: String): List<Project> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${ProjectDBDao.ID}}  = \"$accountId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val projects = mutableListOf<Project>()
        for (i in 0 until result.length()) {
            projects.add(Project.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return projects
    }

    fun getProjectById(accountId: String): Project? {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${ProjectDBDao.ID}}  = \"$accountId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val projects = mutableListOf<Project>()
        for (i in 0 until result.length()) {
            projects.add(Project.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return projects.firstOrNull()
    }





    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, ProjectDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}