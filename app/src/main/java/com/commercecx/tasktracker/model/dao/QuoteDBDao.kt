package com.commercecx.tasktracker.model.dao


import com.commercecx.tasktracker.model.Quote
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject

/**
 * Created by Sagar Das on 11/17/20.
 */
class QuoteDBDao(quote: Quote) : JSONObject() {


    companion object {

        const val ID = "Id"
        const val NAME = "Name"
        const val QUOTENUMBER = "QuoteNumber"
        const val BILLINGSTREET = "BillingStreet"
        const val BILLINGCITY = "BillingCity"
        const val BILLINGSTATE = "BillingState"
        const val BILLINGPOSTALCODE = "BillingPostalCOde"
        const val BILLINGCOUNTRY = "BillingCountry"
        const val SHIPPINGSTREET = "ShippingStreet"
        const val SHIPPINGCITY = "ShippingCity"
        const val SHIPPINGSTATE = "ShippingState"
        const val SHIPPINGPOSTALCODE = "ShippingPostalCode"
        const val SHIPPINGCOUNTRY = "ShippingCountry"


        fun getIndexSpec() =
            arrayOf(
                IndexSpec(ID, SmartStore.Type.string),
                IndexSpec(NAME, SmartStore.Type.string),
                IndexSpec(QUOTENUMBER, SmartStore.Type.string),
                IndexSpec(BILLINGSTREET, SmartStore.Type.string),
                IndexSpec(BILLINGCITY, SmartStore.Type.string),
                IndexSpec(BILLINGSTATE, SmartStore.Type.string),
                IndexSpec(BILLINGPOSTALCODE, SmartStore.Type.string),
                IndexSpec(BILLINGCOUNTRY, SmartStore.Type.string),
                IndexSpec(SHIPPINGSTREET, SmartStore.Type.string),
                IndexSpec(SHIPPINGCITY, SmartStore.Type.string),
                IndexSpec(SHIPPINGSTATE, SmartStore.Type.string),
                IndexSpec(SHIPPINGPOSTALCODE, SmartStore.Type.string),
                IndexSpec(SHIPPINGCOUNTRY, SmartStore.Type.string)

            )
    }

    init {
        put(ID, quote.id)
        put(NAME, quote.name)
        put(QUOTENUMBER, quote.quotenumber)
        put(BILLINGSTREET, quote.billingStreet)
        put(BILLINGCITY, quote.billingCity)
        put(BILLINGSTATE, quote.billingState)
        put(BILLINGPOSTALCODE, quote.billingPostalCode)
        put(BILLINGCOUNTRY, quote.billingCountry)
        put(BILLINGSTREET, quote.billingStreet)
        put(BILLINGCITY, quote.billingCity)
        put(BILLINGSTATE, quote.billingState)
        put(BILLINGPOSTALCODE, quote.billingPostalCode)
        put(BILLINGCOUNTRY, quote.billingCountry)

    }
}