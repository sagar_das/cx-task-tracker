package com.commercecx.tasktracker.model

import com.commercecx.tasktracker.model.dao.SalesforceTaskDBDao
import com.commercecx.tasktracker.util.extensions.stringFromKey
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Sagar Das on 12/8/20.
 */
class SalesforceTask(
    val id: String,
    val ownerId: String
) : Serializable {

    object Mapper{

    fun from(salesforceTask: JSONObject) = SalesforceTask(
        salesforceTask.stringFromKey("Id"),
        salesforceTask.stringFromKey("OwnerId")

    )

    fun fromDB(dbSalesforceTask: JSONObject) = SalesforceTask(
        dbSalesforceTask.optString(SalesforceTaskDBDao.ID, "ID"),
        dbSalesforceTask.optString(SalesforceTaskDBDao.OWNERID, "OWNERID")


    )

    fun from(salesforceTaskArray: JSONArray): List<SalesforceTask> {
        val salesforceTasks = mutableListOf<SalesforceTask>()
        for (i in 0 until salesforceTaskArray.length()) {
            salesforceTasks.add(from(salesforceTaskArray.getJSONObject(i)))
        }
        return salesforceTasks
    }

    fun fromDB(salesforceTaskArray: JSONArray): List<SalesforceTask> {
        val salesforceTasks = mutableListOf<SalesforceTask>()
        for (i in 0 until salesforceTaskArray.length()) {
            salesforceTasks.add(fromDB(salesforceTaskArray.getJSONObject(i)))
        }
        return salesforceTasks
    }

}

}





