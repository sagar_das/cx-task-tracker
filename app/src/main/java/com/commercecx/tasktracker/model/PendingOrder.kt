package com.commercecx.tasktracker.model

/**
 * Created by Sagar Das on 8/20/20.
 */

import com.commercecx.tasktracker.model.dao.OrderDBDao
import org.json.JSONArray
import org.json.JSONObject


enum class Status(val code: Int) {
    SUBMITTED(0), PENDING_SIGNATURE(1), PENDING_ORDER(2)
}

data class PendingOrder(
    val index: Long,
    val customerName: String,
    val jsonOrder: JSONObject,
    val signature: String,
    var status: Status,
    var deliveryDate: Long

) {
    companion object {
        fun create(
            route: Route,
            operatorInfo: UserInfo,
            deliveryDate: Long,
            products: List<Product>,
            subtype: OrderSubtype,
            fromVan: Boolean,
            poNumber: String,
            customerComment: String,
            signature: String
        ): PendingOrder {
            val index = System.currentTimeMillis()
            val order = Order.Mapper.fromProductList(index, products, route, operatorInfo, subtype, fromVan, poNumber, customerComment, deliveryDate)
            return PendingOrder(
                index,
                route.locationName,
                Order.Mapper.toJson(order),
                signature,
                Status.PENDING_ORDER,
                deliveryDate
            )
        }

        fun fromDB(jsonOrder: JSONObject) = PendingOrder(
            jsonOrder.getLong(OrderDBDao.INDEX),
            jsonOrder.getString(OrderDBDao.CUSTOMER),
            jsonOrder.getJSONObject(OrderDBDao.ORDER),
            jsonOrder.getString(OrderDBDao.SIGNATURE),
            when (jsonOrder.getInt(OrderDBDao.STATUS)) {
                0 -> Status.SUBMITTED
                1 -> Status.PENDING_SIGNATURE
                else -> Status.PENDING_ORDER
            },
            jsonOrder.getLong(OrderDBDao.DELIVERYDATE)

        )

        fun fromDB(orderArray: JSONArray): List<PendingOrder> {
            val orders = mutableListOf<PendingOrder>()
            for (i in 0 until orderArray.length()) {
                orders.add(fromDB(orderArray.getJSONObject(i)))
            }
            return orders
        }
    }
}

