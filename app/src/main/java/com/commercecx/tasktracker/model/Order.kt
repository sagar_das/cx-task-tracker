package com.commercecx.tasktracker.model

import org.json.JSONArray
import org.json.JSONObject


enum class OrderSubtype(val subtype: String) {
    CREATE_ORDER("Standard Order"), STOCK_COUNT("Stock Count"), RETURN_ORDER("Unplanned Return")
}

data class OrderAdjustMent(val value: Long, val subtype: String = "ZP04") {
    object Mapper {
        fun toJson(input: OrderAdjustMent): JSONObject {
            return (JSONObject("{\"AdjustmentValue\": ${input.value.toInt()}, \"SubType\": \"${input.subtype}\"}"))
        }

        fun toJsonArray(input: List<OrderAdjustMent>): JSONArray {
            val result = JSONArray()
            input.forEach {
                result.put(OrderAdjustMent.Mapper.toJson(it))
            }

            return result
        }
    }
}

data class OrderLineItem(
        val productId: String,
        val quantity: Int,
        val reasonCode: String,
        val sellingUnit: String,
        val sourceLineNumber: String,
        val adjustments: List<OrderAdjustMent>
) {
    object Mapper {
        fun toJson(input: OrderLineItem): JSONObject {
            val result = JSONObject()
            result.put("ProductID", input.productId)
            result.put("Quantity", input.quantity)
            result.put("ReasonCode", input.reasonCode)
            result.put("SellingUnitofMeasure", input.sellingUnit)
            if (input.adjustments.isNotEmpty()) {
                result.put(
                        "Adjustments", OrderAdjustMent.Mapper.toJsonArray(input.adjustments)
                )
            }

            result.put("SourceLineNumber", input.sourceLineNumber)
            return result
        }

        fun toJsonArray(input: List<OrderLineItem>): JSONArray {
            val result = JSONArray()
            input.forEach {
                result.put(OrderLineItem.Mapper.toJson(it))
            }
            return result
        }
    }
}


data class Order(
        val fromVan: Boolean,
        val orderDate: Long,
        val deliveryDate: Long,
        val customerComment: String,
        val orderType: String,
        val orderSubtype: String,
        val poNumber: String,
        val routeId: String,
        val sfAccontId: String,
        val sfAccountLocationId: String,
        val sfContactId: String,
        val sfOrderNumber: String,
        val orderLineItem: List<OrderLineItem>
) {
    object Mapper {

        fun fromProductList(
            index: Long,
            productList: List<Product>,
            route: Route,
            clientInfo: UserInfo,
            orderSubtype: OrderSubtype,
            fromVan: Boolean,
            poNumber: String,
            customerComment: String,
            deliveryDate: Long
                ): Order {
            val lines = mutableListOf<OrderLineItem>()

            productList.forEachIndexed { index, product ->

                val adjustments = mutableListOf<OrderAdjustMent>()
                product.productdiscount?.let {
                    adjustments.add(OrderAdjustMent(it.toLong()))
                }

                val line = OrderLineItem(
                        product.productid,
                        product.productquantity!!,
                        product.productdiscountreason?.reasonCode.orEmpty(),
                        product.uom,
                        (index + 1).toString(),
                        adjustments
                )
                lines.add(line)
            }

            return Order(
                    fromVan,
                    System.currentTimeMillis(),
                    deliveryDate,
                    customerComment,
                    clientInfo.operatorType.orderString,
                    orderSubtype.subtype,
                    poNumber,
                    route.caseId, // TODO: Fech the right order id (right now we'']e not getting it)
                    route.accountId, // TODO: Check!
                    route.sapAccountLocationId, //TODO: Where can we get this!??? (SalesforceAccountLocationID)
                    route.contactId, //TODO: Where can we get this!??? (salesforcecontactid)
                    index.toString(),
                    lines
            )
        }

        fun toJson(order: Order): JSONObject {
            val result = JSONObject()
            result.put("FulfilledFromVan", order.fromVan)
            result.put("OrderDate", order.orderDate.toString())

            if(order.deliveryDate != 0L)
                result.put("DeliveryDate", order.deliveryDate.toString())
            else
                result.put("DeliveryDate", "")

            result.put("CustomerComment", order.customerComment)
            result.put("OrderSubType", order.orderSubtype)
            result.put("OrderType", order.orderType)
            result.put("PONumber", order.poNumber)
            result.put("CaseID", order.routeId)
            result.put("SalesforceAccountID", order.sfAccontId)
            result.put("SalesforceAccountLocationID", order.sfAccountLocationId) //Change to SalesforceLocationID
            result.put("SalesforceContactID", order.sfContactId)
            result.put("SourceOrderNumber", order.sfOrderNumber)

            result.put(
                    "OrderLineItems", OrderLineItem.Mapper.toJsonArray(order.orderLineItem)
            )

            return result
        }
    }
}

data class CustomerSignature(
        val email: String,
        val sfOrderNumber: String,
        val fileName: String,
        val encodedBase64Image: String
) {
    object Mapper {
        fun toJson(customerSignature: CustomerSignature) = JSONObject().apply {
            with(customerSignature) {
                put("SAPEmpEmail", email)
                put("SourceOrderNumber", sfOrderNumber)
                put("FileName", fileName)
                put("ImageString", encodedBase64Image)
            }
        }
    }
}
