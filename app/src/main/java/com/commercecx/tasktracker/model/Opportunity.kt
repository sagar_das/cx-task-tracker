package com.commercecx.tasktracker.model

import com.commercecx.tasktracker.model.dao.OpportunityDBDao
import com.commercecx.tasktracker.util.extensions.stringFromKey
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Sagar Das on 12/9/20.
 */
class Opportunity (
    val id: String,
    val name: String
) : Serializable {

    object Mapper {

        fun from(opportunity: JSONObject) = Opportunity(
            opportunity.stringFromKey("Id"),
            opportunity.stringFromKey("Name")

        )

        fun fromDB(dbOpportunity: JSONObject) = Opportunity(
            dbOpportunity.optString(OpportunityDBDao.ID, "ID"),
            dbOpportunity.optString(OpportunityDBDao.NAME, "NAME")

        )

        fun from(opportunityArray: JSONArray): List<Opportunity> {
            val opportunitys = mutableListOf<Opportunity>()
            for (i in 0 until opportunityArray.length()) {
                opportunitys.add(Opportunity.Mapper.from(opportunityArray.getJSONObject(i)))
            }
            return opportunitys
        }

        fun fromDB(opportunityArray: JSONArray): List<Opportunity> {
            val opportunitys = mutableListOf<Opportunity>()
            for (i in 0 until opportunityArray.length()) {
                opportunitys.add(Opportunity.Mapper.fromDB(opportunityArray.getJSONObject(i)))
            }
            return opportunitys
        }

    }



}