package com.commercecx.tasktracker.model

import com.commercecx.tasktracker.model.dao.ProjectDBDao
import com.commercecx.tasktracker.util.extensions.stringFromKey
//import com.salesforce.androidsdk.mobilesync.model.SalesforceObject
//import com.salesforce.androidsdk.mobilesync.target.SyncTarget
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Sagar Das on 11/18/20.
 */
data class Project(
    val id: String,
    var projectName: String,
    var account: String,
    var budgetamount: String,
    var projectstatus: String,
    var quoteProposal: String,
    var startDate: String,
    @Transient var data: JSONObject
) : Serializable {




        private var isLocallyModified = false



//    init {
//      objectId = ProjectDBDao.ID
//        val data = this.data
//
//        __locally_created__ = data.optBoolean(SyncTarget.LOCALLY_CREATED)
//        __locally_deleted__ = data.optBoolean(SyncTarget.LOCALLY_DELETED)
//        __locally_updated__ = data.optBoolean(SyncTarget.LOCALLY_UPDATED)
//        isLocallyModified = __locally_created__ || __locally_updated__ || __locally_deleted__
//    }

    object Mapper {

        //pse__Proj__c object
//        fun from(project: JSONObject) = Project(
//            project.stringFromKey("Id"),
//            project.stringFromKey("Name"),
//            project.stringFromKey("pse__Account__c"),
//            project.stringFromKey("Total_Bid_Value__c"),
//            project.stringFromKey("Budget_Status__c"),
//            project.stringFromKey("pse__Opportunity__c"),
//            project.stringFromKey("pse__Start_Date__c"),
//            project
//        )


        //Custom project object
        fun from(project: JSONObject) = Project(
            project.stringFromKey("Id"),
            project.stringFromKey("Name"),
            project.stringFromKey("pse__Account__c"),
            project.stringFromKey("Total_Bid_Value_R__c"),
            project.stringFromKey("Budget_Status__c"),
            project.stringFromKey("pse__Opportunity__c"),
            project.stringFromKey("Start_Date__c"),
            project
        )

        fun fromDB(dbProject: JSONObject) = Project(
            dbProject.optString(ProjectDBDao.ID, "ID"),
            dbProject.optString(ProjectDBDao.NAME, "NAME"),
            dbProject.optString(ProjectDBDao.ACCOUNT, "ACCOUNT"),
            dbProject.optString(ProjectDBDao.BUDGETAMOUNT, "BUDGETAMOUNT"),
            dbProject.optString(ProjectDBDao.PROJECTSTATUS, "PROJECTSTATUS"),
            dbProject.optString(ProjectDBDao.QUOTEPROPOSAL, "QUOTEPROPOSAL"),
            dbProject.optString(ProjectDBDao.STARTDATE, "STARTDATE"),
            dbProject

        )

        fun from(quoteArray: JSONArray): List<Project> {
            val projects = mutableListOf<Project>()
            for (i in 0 until quoteArray.length()) {
                projects.add(from(quoteArray.getJSONObject(i)))
            }
            return projects
        }

        fun fromDB(quoteArray: JSONArray): List<Project> {
            val projects = mutableListOf<Project>()
            for (i in 0 until quoteArray.length()) {
                projects.add(Mapper.fromDB(quoteArray.getJSONObject(i)))
            }
            return projects
        }

    }



}