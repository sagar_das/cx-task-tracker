package com.commercecx.tasktracker.model.database

import com.commercecx.tasktracker.model.PendingOrder
import com.commercecx.tasktracker.model.Status
import com.commercecx.tasktracker.model.dao.OrderDBDao

import com.salesforce.androidsdk.smartstore.store.QuerySpec
import java.util.*
import java.util.concurrent.TimeUnit

class DBOrdersHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "ORDERS"

    }

    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = OrderDBDao.getIndexSpec()

    fun insertOrder(pendingOrder: PendingOrder) {
        insertOrUpdate(OrderDBDao.INDEX, OrderDBDao(pendingOrder))
    }

    fun getAllOrders(): List<PendingOrder> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(getTableName(), OrderDBDao.INDEX, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return PendingOrder.fromDB(result)
    }

    fun getAllOrdersByStatus(status: Status): List<PendingOrder> {
        val result = getSmartStore().query(
            QuerySpec.buildExactQuerySpec(getTableName(), OrderDBDao.STATUS, status.code.toString(), OrderDBDao.INDEX, QuerySpec.Order.ascending, PAGE_SIZE),
            FIRST_PAGE_INDEX
        )
        return PendingOrder.fromDB(result)
    }

    fun getAllPendingOrders(): List<PendingOrder> {

        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${OrderDBDao.STATUS}}  IS NOT 0 )",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )

        val orders = mutableListOf<PendingOrder>()
        for (i in 0 until result.length()) {
            orders.add(PendingOrder.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }

        return orders
    }

    fun getOrderByDateQuery(date: Long) = QuerySpec.buildExactQuerySpec(getTableName(), OrderDBDao.INDEX, date.toString(), OrderDBDao.INDEX, QuerySpec.Order.ascending, PAGE_SIZE)

    fun deleteOldOrders() {
        getAllOrders().forEach {
            if (TimeUnit.DAYS.convert(Date().time - Date(it.index).time, TimeUnit.MILLISECONDS) > 3) {
                if (it.status == Status.SUBMITTED) {
                    getSmartStore().deleteByQuery(TABLE_NAME, getOrderByDateQuery(it.index))
                }
            }
        }


    }

    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, OrderDBDao.ORDER, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}

