package com.commercecx.tasktracker.model.database

import com.commercecx.tasktracker.model.SalesforceTask
import com.commercecx.tasktracker.model.dao.SalesforceTaskDBDao
import com.salesforce.androidsdk.smartstore.store.QuerySpec

/**
 * Created by Sagar Das on 12/8/20.
 */
class DBSalesforceTaskHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "SALESFORCETASKS"

    }


    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = SalesforceTaskDBDao.getIndexSpec()


    fun insertSalesforceTasks(salesforceTasks: List<SalesforceTask>) {
        dropTable()
        getSmartStore().registerSoup(TABLE_NAME, SalesforceTaskDBDao.getIndexSpec())
        salesforceTasks.forEach { getSmartStore().create(TABLE_NAME, SalesforceTaskDBDao(it)) }
        //JDELog.e("inserted, there are... {${getAllSalesforceTasks().size}} salesforceTasks in table")
    }

    fun updateSalesforceTasks(salesforceTasks: List<SalesforceTask>) {
        salesforceTasks.forEach {
            insertOrUpdate(SalesforceTaskDBDao.ID, SalesforceTaskDBDao(it))
        }
        //JDELog.e("updated salesforceTasks, there are... {${getAllSalesforceTasks().size}} salesforceTasks in table")
    }

    fun getAllSalesforceTasks(): List<SalesforceTask> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, SalesforceTaskDBDao.ID, QuerySpec.Order.ascending, 100), FIRST_PAGE_INDEX)

        return SalesforceTask.Mapper.fromDB(result)

    }

    /*
   buildLikeQuerySpec(String soupName, String path, String likeKey, String orderPath, Order order, int pageSize)
   buildLikeQuerySpec(String soupName, String[] selectPaths, String path, String likeKey, String orderPath, Order order, int pageSize) {

    */
//    fun getSalesforceTaskByName(accountName: String): List<SalesforceTask> {
//        val result = getSmartStore().query(
//            QuerySpec.buildLikeQuerySpec(
//                DBSalesforceTaskHelper.TABLE_NAME,
//                SalesforceTaskDBDao.NAME, accountName, SalesforceTaskDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
//        return SalesforceTask.Mapper.fromDB(result)
//    }

//    fun getSalesforceTaskBySalesforceName(salesforceName: String): List<SalesforceTask> {
//        val result = getSmartStore().query(
//            QuerySpec.buildLikeQuerySpec(
//                DBSalesforceTaskHelper.TABLE_NAME,
//                SalesforceTaskDBDao.PROJECTNAME, salesforceName, SalesforceTaskDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
//        return SalesforceTask.Mapper.fromDB(result)
//    }




    fun getSalesforceTasksById(accountId: String): List<SalesforceTask> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${SalesforceTaskDBDao.ID}}  = \"$accountId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val salesforceTasks = mutableListOf<SalesforceTask>()
        for (i in 0 until result.length()) {
            salesforceTasks.add(SalesforceTask.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return salesforceTasks
    }

    fun getSalesforceTaskById(accountId: String): SalesforceTask? {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${SalesforceTaskDBDao.ID}}  = \"$accountId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val salesforceTasks = mutableListOf<SalesforceTask>()
        for (i in 0 until result.length()) {
            salesforceTasks.add(SalesforceTask.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return salesforceTasks.firstOrNull()
    }





    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, SalesforceTaskDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}