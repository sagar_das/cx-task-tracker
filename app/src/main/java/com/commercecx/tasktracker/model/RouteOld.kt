package com.commercecx.tasktracker.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.io.Serializable

/**
 * Created by Sagar Das on 8/19/20.
 */


open class RouteOld : RealmObject(), Serializable {
    /*
    {
      "_id": "5b2ccd9908500a434ca67d56",
      "index": 8,
      "guid": "478f0e07-973c-462f-8549-e1d0226b9838",
      "isActive": false,
      "image": "http://placehold.it/64x64",
      "name": "Carpenter Barr",
      "address": "582 Seagate Avenue, Lynn, Arkansas, 6234",
      "date": "Thu Sep 04 2014 14:02:34 GMT+0000 (UTC)"
    }
     */
    @PrimaryKey
    @SerializedName("_id")
    private var id: String? = null

    @SerializedName("index")
    private var index = 0

    @SerializedName("guid")
    private var guid: String? = null

    @SerializedName("isActive")
    private var isActive = false

    @SerializedName("image")
    private var image: String? = null

    @SerializedName("name")
    private var name: String? = null

    @SerializedName("address")
    private var address: String? = null

    @SerializedName("date")
    private var date: String? = null

    @SerializedName("routeDate")
    private var routeDate: String? = null

    @SerializedName("isCompleted")
    private var isCompleted = false

    @SerializedName("updatedTSMP")
    private var updatedTSMP: Long = 0

    @SerializedName("customerName")
    private var customerName: String? = null

    @SerializedName("customerPhoneNo")
    private var customerPhoneNo: String? = null
    fun getCustomerName(): String? {
        return customerName
    }

    fun setCustomerName(customerName: String?) {
        this.customerName = customerName
    }

    fun getCustomerPhoneNo(): String? {
        return customerPhoneNo
    }

    fun setCustomerPhoneNo(customerPhoneNo: String?) {
        this.customerPhoneNo = customerPhoneNo
    }

    fun getUpdatedTSMP(): Long {
        return updatedTSMP
    }

    fun setUpdatedTSMP(updatedTSMP: Long) {
        this.updatedTSMP = updatedTSMP
    }

    fun getrouteDate(): String? {
        return routeDate
    }

    fun setrouteDatee(route_date: String?) {
        routeDate = route_date
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getIndex(): Int {
        return index
    }

    fun setIndex(index: Int) {
        this.index = index
    }

    fun getGuid(): String? {
        return guid
    }

    fun setGuid(guid: String?) {
        this.guid = guid
    }

    fun isActive(): Boolean {
        return isActive
    }

    fun setActive(active: Boolean) {
        isActive = active
    }

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String?) {
        this.image = image
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getAddress(): String? {
        return address
    }

    fun setAddress(address: String?) {
        this.address = address
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String?) {
        this.date = date
    }

    fun isCompleted(): Boolean {
        return isCompleted
    }

    fun setCompleted(completed: Boolean) {
        isCompleted = completed
    }
}
