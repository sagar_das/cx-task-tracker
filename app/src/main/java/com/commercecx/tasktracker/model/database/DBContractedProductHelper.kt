package com.commercecx.tasktracker.model.database


import com.commercecx.tasktracker.model.Category
import com.commercecx.tasktracker.model.Product
import com.commercecx.tasktracker.model.dao.ProductDBDao
import com.salesforce.androidsdk.smartstore.store.QuerySpec


//TODO: Check if it's possible to batch insert/update (sending jsonarrays)

class DBContractedProductHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "CONTRACTED_PRODUCTS"

    }

    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = ProductDBDao.getIndexSpec()


    fun insertProducts(baseContractedProducts: List<Product>) {
        getSmartStore().dropSoup(TABLE_NAME)
        getSmartStore().registerSoup(TABLE_NAME, ProductDBDao.getIndexSpec())
        baseContractedProducts.map {
            Product(
                    it.uom,
                    DBProductHelper().getProductById(it.productid)?.subsubcategory.orEmpty(),
                    DBProductHelper().getProductById(it.productid)?.subcategory.orEmpty(),
                    it.routesales,
                    it.productname,
                    it.productimage,
                    it.productid,
                    it.productcode,
                    it.active,
                    DBProductHelper().getProductById(it.productid)?.category.orEmpty(),
                    it.consignment,
                    it.isContractedProduct,
                    it.accountID,
                    it.accountID+it.productid)
        }.forEach {
            insertOrUpdate(ProductDBDao.ACCOUNTPRODUCTKEY, ProductDBDao(it))
        }
    }

    /*
    buildAllQuerySpec(String soupName, String orderPath, Order order, int pageSize)
     */
    fun getAllProducts(): List<Product> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, ProductDBDao.ACCOUNTPRODUCTKEY, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return Product.Mapper.fromDB(result)
    }



    fun getAllConsignmentProducts(): List<Product> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, ProductDBDao.ACCOUNTPRODUCTKEY, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return Product.Mapper.fromDB(result)
    }

    fun getCategories(): List<Category> {
        val result = getSmartStore().query(
                QuerySpec.buildSmartQuerySpec("SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.CATEGORY}} FROM {$TABLE_NAME}", PAGE_SIZE),
                FIRST_PAGE_INDEX
        )

        val categories = mutableListOf<Category>()
        for (i in 0 until result.length()) {
            categories.add(Category(i.toLong(), result.getJSONArray(0).getString(i), 0))
        }
        return categories
    }

    fun getSubCategories(category: String): List<Category> {
        val result = getSmartStore().query(
                QuerySpec.buildSmartQuerySpec(
                        "SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}} FROM {$TABLE_NAME} " +
                                "WHERE {$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\"",
                        -1
                ),
                FIRST_PAGE_INDEX
        )

        val subcategories = mutableListOf<Category>()
        for (i in 0 until result.length()) {
            subcategories.add(Category(i.toLong(), result.getJSONArray(i).getString(0), 0))
        }
        return subcategories
    }

    fun getBrands(category: String, subcategory: String): List<Category> {
        val result = getSmartStore().query(
                QuerySpec.buildSmartQuerySpec(
                        "SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.SUBSUBCATEGORY}} FROM {$TABLE_NAME} " +
                                "WHERE ({$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\" AND  {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}}  = \"$subcategory\")",
                        -1
                ),
                FIRST_PAGE_INDEX
        )
        val brands = mutableListOf<Category>()
        for (i in 0 until result.length()) {
            brands.add(Category(i.toLong(), result.getJSONArray(i).getString(0), 0))
        }
        return brands
    }


    fun getProducts(category: String, subcategory: String, brand: String): List<Product> {
        val result = getSmartStore().query(
                QuerySpec.buildSmartQuerySpec(
                        "SELECT * FROM {$TABLE_NAME} " +
                                "WHERE ({$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\" " +
                                "AND  {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}}  = \"$subcategory\" " +
                                "AND  {$TABLE_NAME:${ProductDBDao.SUBSUBCATEGORY}}  = \"$brand\")",
                        -1
                ),
                FIRST_PAGE_INDEX
        )
        val products = mutableListOf<Product>()
        for (i in 0 until result.length()) {
            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return products
    }

    fun getProductById(productId: String): List<Product> {
        val result = getSmartStore().query(
                QuerySpec.buildSmartQuerySpec(
                        "SELECT * FROM {$TABLE_NAME} " +
                                "WHERE ({$TABLE_NAME:${ProductDBDao.PRODUCTID}}  = \"$productId\")",
                        -1
                ),
                FIRST_PAGE_INDEX
        )
        val products = mutableListOf<Product>()
        for (i in 0 until result.length()) {
            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return products
    }

//    fun getProductByName(productName: String): List<Product> {
//        val result = getSmartStore().query(
//            QuerySpec.buildSmartQuerySpec(
//                "SELECT * FROM {$TABLE_NAME} " +
//                        "WHERE ({$TABLE_NAME:${ProductDBDao.PRODUCTNAME}}  LIKE \"%$productName%\")",
//                -1
//            ),
//            FIRST_PAGE_INDEX
//        )
//        val products = mutableListOf<Product>()
//        for (i in 0 until result.length()) {
//            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
//        }
//        return products
//    }

    fun getProductByProductCode(productCode: String): List<Product> {
        val result = getSmartStore().query(
                QuerySpec.buildSmartQuerySpec(
                        "SELECT * FROM {$TABLE_NAME} " +
                                "WHERE ({$TABLE_NAME:${ProductDBDao.PRODUCTCODE}}  = \"$productCode\")",
                        -1
                ),
                FIRST_PAGE_INDEX
        )
        val products = mutableListOf<Product>()
        for (i in 0 until result.length()) {
            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return products
    }

    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, ProductDBDao.PRODUCTID, QuerySpec.Order.ascending, PAGE_SIZE)
    }


}