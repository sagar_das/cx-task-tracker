package com.commercecx.tasktracker.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Sagar Das on 8/19/20.
 */


open class Note : RealmObject() {
    /*
     {
      "_id": "5b2d1dfd6429b1e6ae274e7e",
      "index": 0,
      "guid": "b1267a25-30c5-4387-95f5-a3d6318d689f",
      "isActive": true,
      "name": "ipsum dolore eu",
      "remark": "628 Newkirk Placez",
      "date": "Mon Aug 21 2017 12:01:11 GMT+0000 (UTC)"
    }
     */
    //region Variable Declaration
    @PrimaryKey
    @SerializedName("note_id")
    private var id: String? = null

    @SerializedName("index")
    private var index = 0

    @SerializedName("guid")
    private var guid: String? = null

    @SerializedName("isActive")
    private var isActive = false

    @SerializedName("isAttachment")
    private var isAttachment = false

    @SerializedName("title")
    private var title: String? = null

    @SerializedName("contents")
    private var contents: String? = null

    @SerializedName("date")
    private var date: String? = null
    private lateinit var attachments: ByteArray
    private var fileType: String? = null

    @SerializedName("isServerCommitted")
    private var isServerCommitted = false

    @SerializedName("pendingOperation")
    private var pendingOperation: String? = null

    @SerializedName("attachPath")
    private var attachPath: String? = null

    @SerializedName("attachName")
    private var attachName: String? = null

    @SerializedName("updatedTSMP")
    private var updatedTSMP: Long = 0

    //endregion
    //region getter and setter
    fun getUpdatedTSMP(): Long {
        return updatedTSMP
    }

    fun setUpdatedTSMP(updatedTSMP: Long) {
        this.updatedTSMP = updatedTSMP
    }

    fun getAttachPath(): String? {
        return attachPath
    }

    fun setAttachPath(attachPath: String?) {
        this.attachPath = attachPath
    }

    fun getAttachName(): String? {
        return attachName
    }

    fun setAttachName(attachName: String?) {
        this.attachName = attachName
    }

    fun getPendingOperation(): String? {
        return pendingOperation
    }

    fun setPendingOperation(pendingOperation: String?) {
        this.pendingOperation = pendingOperation
    }

    fun isServerCommitted(): Boolean {
        return isServerCommitted
    }

    fun setServerCommitted(serverCommitted: Boolean) {
        isServerCommitted = serverCommitted
    }

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getIndex(): Int {
        return index
    }

    fun setIndex(index: Int) {
        this.index = index
    }

    fun getGuid(): String? {
        return guid
    }

    fun setGuid(guid: String?) {
        this.guid = guid
    }

    fun isActive(): Boolean {
        return isActive
    }

    fun setActive(active: Boolean) {
        isActive = active
    }

    fun getName(): String? {
        return title
    }

    fun setName(name: String?) {
        title = name
    }

    fun getRemark(): String? {
        return contents
    }

    fun setRemark(remark: String?) {
        contents = remark
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String?) {
        this.date = date
    }

    fun isAttachment(): Boolean {
        return isAttachment
    }

    fun setAttachment(attachment: Boolean) {
        isAttachment = attachment
    }

    fun getAttachments(): ByteArray {
        return attachments
    }

    fun setAttachments(attachments: ByteArray) {
        this.attachments = attachments
    }

    fun getFileType(): String? {
        return fileType
    }

    fun setFileType(fileType: String?) {
        this.fileType = fileType
    } //endregion
}
