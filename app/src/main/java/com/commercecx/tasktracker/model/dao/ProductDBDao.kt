package com.commercecx.tasktracker.model.dao


import com.commercecx.tasktracker.model.Product
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject


class ProductDBDao(product: Product) : JSONObject() {


    companion object {

        const val CATEGORY = "Category"
        const val CONSIGNMENT = "Consignment"
        const val ISACTIVE = "isActive"
        const val PRODUCTCODE = "ProductCode"
        const val PRODUCTID = "ProductID"
        const val PRODUCTIMAGE = "ProductImage"
        const val PRODUCTNAME = "ProductName"
        const val ROUTESALES = "RouteSales"
        const val SUBCATEGORY = "SubCategory"
        const val SUBSUBCATEGORY = "SubSubCategory"
        const val UOM = "UOM"
        const val ISCONTRACTED = "ISCONTRACTED"
        const val ACCOUNTID = "ACCOUNTID"
        const val ACCOUNTPRODUCTKEY = "ACCOUNTIDProductID"

        fun getIndexSpec() =
                arrayOf(
                        IndexSpec(CATEGORY, SmartStore.Type.string),
                        IndexSpec(ISACTIVE, SmartStore.Type.string),
                        IndexSpec(CONSIGNMENT, SmartStore.Type.string),
                        IndexSpec(PRODUCTCODE, SmartStore.Type.string),
                        IndexSpec(PRODUCTID, SmartStore.Type.string),
//                IndexSpec(PRODUCTIMAGE, SmartStore.Type.string),
                        IndexSpec(PRODUCTNAME, SmartStore.Type.string),
//                IndexSpec(ROUTESALES, SmartStore.Type.string),
                        IndexSpec(SUBCATEGORY, SmartStore.Type.string),
                        IndexSpec(SUBSUBCATEGORY, SmartStore.Type.string),
//                IndexSpec(UOM, SmartStore.Type.string)
                        IndexSpec(ACCOUNTID, SmartStore.Type.string),
                        IndexSpec(ACCOUNTPRODUCTKEY, SmartStore.Type.string)

                )
    }

    init {
        put(UOM, product.uom)
        put(SUBSUBCATEGORY, product.subsubcategory)
        put(SUBCATEGORY, product.subcategory)
        put(
                ROUTESALES, if (product.routesales) {
            "true"
        } else {
            "false"
        }
        )
        put(PRODUCTNAME, product.productname)
        put(PRODUCTIMAGE, product.productimage)
        put(PRODUCTID, product.productid)
        put(PRODUCTCODE, product.productcode)
        put(
                ISACTIVE, if (product.active) {
            "true"
        } else {
            "false"
        }
        )

        put(CATEGORY, product.category)
        put(
                CONSIGNMENT, if (product.consignment) {
            "true"
        } else {
            "false"
        }
        )
        put(ISCONTRACTED, product.isContractedProduct)
        put(ACCOUNTID, product.accountID)
        put(ACCOUNTPRODUCTKEY, product.accountID+product.productid)
    }
}



