package com.commercecx.tasktracker.model

/**
 * Created by Sagar Das on 8/20/20.
 */


import com.commercecx.tasktracker.model.dao.RouteDBDao
import com.commercecx.tasktracker.model.dao.UserDBDAo
import com.commercecx.tasktracker.model.database.DBRoutesHelper
import com.commercecx.tasktracker.util.extensions.stringFromKey
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


enum class OperatorType(val code: String, val orderString: String, val operatorString: String) {
    VAN("S25", "Route Sales Order", "Route Sales"),
    WAREHOUSE("S20", "Operating Order", "Operating")
}

data class UserInfo(val operatorType: OperatorType, val executingEngineer: String, val availableUom: List<String>, val displayName: String, val email: String) {
    object Mapper {
        fun parse(jsonObject: JSONObject, displayName: String, email: String): UserInfo {
            return UserInfo(
                if (jsonObject.optString("WorkOrderType", "") == "S25") {
                    OperatorType.VAN
                } else {
                    OperatorType.WAREHOUSE
                },
                jsonObject.optString("ExecutingEngineer"),
                jsonObject.optString("AvailableUOM").split(","),
                displayName,
                email
            )
        }

        fun fromDB(userInfo: JSONObject) = UserInfo(
            if (userInfo.optString(UserDBDAo.OPERATORTYPE, OperatorType.VAN.code).equals(OperatorType.VAN.code, false)) {
                OperatorType.VAN
            } else {
                OperatorType.WAREHOUSE
            },
            userInfo.optString(UserDBDAo.EXECUTINGENGINEER),
            userInfo.optString(UserDBDAo.AVAILABLEUOM).split(","),
            userInfo.optString(UserDBDAo.DISPLAYNAME),
            userInfo.optString(UserDBDAo.EMAIL)
        )
    }
}

data class Route(
    val id: Long,
    val accountId: String,
    val accountName: String,
    val time: Date,
    val caseId: String,
    val city: String,
    val contactFirstName: String,
    val contactId: String,
    val contactLastName: String,
    val contactPhone: String,
    val contactEmail: String,
    val country: String,
    val houseNumber: String,
    val locationName: String,
    val orderNonContractedProducts: Boolean,
    val postalCode: String,
    val poNumber: String,
    val poNumberRequired: Boolean,
    val customerId: String,
    val sapAccountLocationId: String,
    val state: String,
    var dispatched: Boolean,
    var timeVisited: Long = 0L,
    val street: String
) {
    fun updateVisited(time: Long) {
        this.dispatched = (time == 0L)
        this.timeVisited = time
        DBRoutesHelper().updateRouteStatus(caseId, time)
    }

    fun isVisited() = (this.timeVisited != 0L)

    object Mapper {
        val inputFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.forLanguageTag("en-GB"))
        private fun from(id: Int, routeItem: JSONObject) = Route(
            id.toLong(),
            routeItem.stringFromKey("AccountID"),
            routeItem.stringFromKey("AccountName"),
            inputFormatter.parse(routeItem.optString("ActualStartDate", inputFormatter.format(Date()))),
            routeItem.stringFromKey("CaseID"),
            routeItem.stringFromKey("City"),
            routeItem.stringFromKey("ContactFirstName"),
            routeItem.stringFromKey("ContactID"),
            routeItem.stringFromKey("ContactLastName"),
            routeItem.stringFromKey("ContactPhone"),
            routeItem.stringFromKey("ContactEmail"),
            routeItem.stringFromKey("Country"),
            routeItem.stringFromKey("HouseNumber"),
            routeItem.stringFromKey("LocationName"),
            routeItem.optBoolean("OrderNonContractedProducts", false),
            routeItem.stringFromKey("PostalCode"),
            routeItem.stringFromKey("PONumber"),
            routeItem.optString("PONumberRequired", "yes").equals("yes", true),
            routeItem.stringFromKey("SAPCustomerID"),
            routeItem.stringFromKey("SalesforceAccountLocationID"),
            routeItem.stringFromKey("State"),
            routeItem.optString("Status", "Status").equals("Dispatched", true),
            0L,
            routeItem.stringFromKey("Street")
        )

        fun from(routeArray: JSONArray): List<Route> {
            val routes = mutableListOf<Route>()
            for (i in 0 until routeArray.length()) {
                routes.add(Route.Mapper.from(i, routeArray.getJSONObject(i)))
            }
            return routes
        }

        private fun fromDB(id: Int, item: JSONObject) = Route(
            id.toLong(),
            item.optString(RouteDBDao.ACCOUNTID, "AccountID"),
            item.optString(RouteDBDao.ACCOUNTNAME, "AccountName"),
            Date(item.optLong(RouteDBDao.TIME)),
            item.optString(RouteDBDao.CASEID, "CaseID"),
            item.optString(RouteDBDao.CITY, "City"),
            item.optString(RouteDBDao.CONTACTFIRSTNAME, "ContactFirstName"),
            item.optString(RouteDBDao.CONTACTID, "ContactID"),
            item.optString(RouteDBDao.CONTACTLASTNAME, "ContactLastName"),
            item.optString(RouteDBDao.CONTACTPHONE, "ContactPhone"),
            item.optString(RouteDBDao.CONTACTEMAIL, "ContactEmail"),
            item.optString(RouteDBDao.COUNTRY, "Country"),
            item.optString(RouteDBDao.HOUSENUMBER, "HouseNumber"),
            item.optString(RouteDBDao.LOCATIONNAME, "LocationName"),
            item.optString(RouteDBDao.ORDERNONCONTRACTEDPRODUCTS, "true").equals("true", true),
            item.optString(RouteDBDao.POSTALCODE, "PostalCode"),
            item.optString(RouteDBDao.PONUMBER, "PostalCode"),
            item.optString(RouteDBDao.PONUMBERREQUIRED, "true").equals("true", true),
            item.optString(RouteDBDao.CUSTOMERID, "CustomerId"),
            item.optString(RouteDBDao.SALESFORCEACCOUNTLOCATIONID, "SALESFORCEACCOUNTLOCATIONID"),
            item.optString(RouteDBDao.STATE, "State"),
            item.optString(RouteDBDao.VISITED, "false").equals("true", true),
            item.optLong(RouteDBDao.TIME_VISITED, 0L),
            item.optString(RouteDBDao.STREET, "Street")
        )


        fun fromDB(routesArray: JSONArray): List<Route> {
            val routes = mutableListOf<Route>()
            for (i in 0 until routesArray.length()) {
                routes.add(Route.Mapper.fromDB(i, routesArray.getJSONObject(i)))
            }
            return routes
        }
    }
}



data class RoutesInfo(val userInfo: UserInfo, val routes: List<Route>, val products: List<Product>, val contractedProduct: List<Product>) {

    object Mapper {
        fun from(jsonObject: JSONObject, displayName: String, email: String) = RoutesInfo(
            UserInfo.Mapper.parse(jsonObject.optJSONObject("UserInfo"), displayName, email),
            Route.Mapper.from(jsonObject.getJSONArray("RoutesInfo")),
            Product.Mapper.from(jsonObject.getJSONArray("Products")),
            Product.Mapper.from(jsonObject.getJSONArray("ContractedProducts"), true, true)
        )
    }

    object ProductMapper{
        fun from(jsonArray: JSONArray){
            Product.Mapper.from(jsonArray)
        }
    }
}

data class ProductInfo(val products: List<Product>){
    object Mapper{
        fun from(jsonArray: JSONArray) = ProductInfo(
            Product.Mapper.from(jsonArray)
        )
    }

}

data class AccountInfo(val accounts: List<Account>){
    object Mapper{
        fun from(jsonArray: JSONArray) = AccountInfo(
            Account.Mapper.from(jsonArray)
        )
    }

}

data class OpportunityInfo(val opportunities: List<Opportunity>){
    object Mapper{
        fun from(jsonArray: JSONArray) = OpportunityInfo(
            Opportunity.Mapper.from(jsonArray)
        )
    }

}

data class QuoteInfo(val quotes: List<Quote>){
    object Mapper{
        fun from(jsonArray: JSONArray) = QuoteInfo(
            Quote.Mapper.from(jsonArray)
        )
    }

}

data class ProjectInfo(val projects: List<Project>){
    object Mapper{
        fun from(jsonArray: JSONArray) = ProjectInfo(
            Project.Mapper.from(jsonArray)
        )
    }

}

data class PricingTaskInfo(val projectTasks: List<ProjectTask>){
    object Mapper{
        fun from(jsonArray: JSONArray) = PricingTaskInfo(
            ProjectTask.Mapper.from(jsonArray)
        )
    }

}

data class SalesforceTaskInfo(val salesforceTasks: List<SalesforceTask>){
    object Mapper{
        fun from(jsonArray: JSONArray) = SalesforceTaskInfo(
            SalesforceTask.Mapper.from(jsonArray)
        )
    }

}




