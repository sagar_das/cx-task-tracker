package com.commercecx.tasktracker.model.dao


import com.commercecx.tasktracker.model.PendingOrder
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject


class OrderDBDao(pendingOrder: PendingOrder) : JSONObject() {
    companion object {
        const val INDEX = "index"
        const val DELIVERYDATE = "deliveryDate"
        const val CUSTOMER = "customer"
        const val ORDER = "order"
        const val SIGNATURE = "signature"
        const val STATUS = "status"


        fun getIndexSpec() =
                arrayOf(
                        IndexSpec(INDEX, SmartStore.Type.integer),
                        IndexSpec(CUSTOMER, SmartStore.Type.string),
                        IndexSpec(ORDER, SmartStore.Type.json1),
                        IndexSpec(SIGNATURE, SmartStore.Type.string),
                        IndexSpec(STATUS, SmartStore.Type.integer),
                        IndexSpec(DELIVERYDATE, SmartStore.Type.integer)

                )
    }

    init {


        put(INDEX, pendingOrder.index)
        put(CUSTOMER, pendingOrder.customerName)
        put(ORDER, pendingOrder.jsonOrder)
        put(SIGNATURE, pendingOrder.signature)
        put(STATUS, pendingOrder.status.code)
        put(DELIVERYDATE, pendingOrder.deliveryDate)
    }
}