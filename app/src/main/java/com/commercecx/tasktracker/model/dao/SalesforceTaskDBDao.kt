package com.commercecx.tasktracker.model.dao

import com.commercecx.tasktracker.model.SalesforceTask
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject

/**
 * Created by Sagar Das on 12/8/20.
 */
class SalesforceTaskDBDao (salesforceTask: SalesforceTask) : JSONObject() {


    companion object {

        const val ID = "Id"
        const val OWNERID = "OwnerId"





        fun getIndexSpec() =
            arrayOf(
                IndexSpec(ID, SmartStore.Type.string),
                IndexSpec(OWNERID, SmartStore.Type.string)
            )
    }

    init {
        put(ID, salesforceTask.id)
        put(OWNERID, salesforceTask.ownerId)




    }
}