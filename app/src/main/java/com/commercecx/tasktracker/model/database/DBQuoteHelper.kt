package com.commercecx.tasktracker.model.database

import com.commercecx.tasktracker.model.Quote
import com.commercecx.tasktracker.model.dao.QuoteDBDao
import com.salesforce.androidsdk.smartstore.store.QuerySpec

/**
 * Created by Sagar Das on 11/17/20.
 */
class DBQuoteHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "QUOTES"

    }


    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = QuoteDBDao.getIndexSpec()


    fun insertQuotes(quotes: List<Quote>) {
        dropTable()
        getSmartStore().registerSoup(TABLE_NAME, QuoteDBDao.getIndexSpec())
        quotes.forEach { getSmartStore().create(TABLE_NAME, QuoteDBDao(it)) }
        //JDELog.e("inserted, there are... {${getAllQuotes().size}} quotes in table")
    }

    fun updateQuotes(quotes: List<Quote>) {
        quotes.forEach {
            insertOrUpdate(QuoteDBDao.ID, QuoteDBDao(it))
        }
        //JDELog.e("updated quotes, there are... {${getAllQuotes().size}} quotes in table")
    }

    fun getAllQuotes(): List<Quote> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, QuoteDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)

        return Quote.Mapper.fromDB(result)

    }

    /*
   buildLikeQuerySpec(String soupName, String path, String likeKey, String orderPath, Order order, int pageSize)
   buildLikeQuerySpec(String soupName, String[] selectPaths, String path, String likeKey, String orderPath, Order order, int pageSize) {

    */
    fun getQuoteByName(accountName: String): List<Quote> {
        val result = getSmartStore().query(
            QuerySpec.buildLikeQuerySpec(
            DBQuoteHelper.TABLE_NAME,
            QuoteDBDao.NAME, accountName, QuoteDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return Quote.Mapper.fromDB(result)
    }




    fun getQuotesById(quoteID: String): List<Quote> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${QuoteDBDao.ID}}  = \"$quoteID\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val quotes = mutableListOf<Quote>()
        for (i in 0 until result.length()) {
            quotes.add(Quote.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return quotes
    }

    fun getQuoteById(accountId: String): Quote? {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${QuoteDBDao.ID}}  = \"$accountId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val quotes = mutableListOf<Quote>()
        for (i in 0 until result.length()) {
            quotes.add(Quote.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return quotes.firstOrNull()
    }





    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, QuoteDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}