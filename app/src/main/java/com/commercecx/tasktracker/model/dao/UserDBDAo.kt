package com.commercecx.tasktracker.model.dao


import com.commercecx.tasktracker.model.UserInfo
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject

class UserDBDAo(userInfo: UserInfo) : JSONObject() {
    companion object {
        const val OPERATORTYPE = "operatorType"
        const val EXECUTINGENGINEER = "executingEngineer"
        const val AVAILABLEUOM = "availableUom"
        const val DISPLAYNAME = "displayName"
        const val EMAIL = "email"

        fun getIndexSpec() =
            arrayOf(
                IndexSpec(EMAIL, SmartStore.Type.string)
            )
    }

    init {
        put(OPERATORTYPE, userInfo.operatorType.code)
        put(EXECUTINGENGINEER, userInfo.executingEngineer)
        put(AVAILABLEUOM, userInfo.availableUom)
        put(DISPLAYNAME, userInfo.displayName)
        put(EMAIL, userInfo.email)
    }
}
