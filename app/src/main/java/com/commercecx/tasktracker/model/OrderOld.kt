package com.commercecx.tasktracker.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.io.Serializable

/**
 * Created by Sagar Das on 8/19/20.
 */


open class OrderOld : RealmObject(), Serializable {
    @PrimaryKey
    private var id = 0
    private var name: String? = null
    private var products: RealmList<ProductOld>? = null
    private var total: Long = 0
    private var sub_total: Long = 0
    private var tax: Long = 0
    private var date: String? = null
    private var image: String? = null
    private var type: String? = null
    private var route_id: String? = null
    fun getRoute_id(): String? {
        return route_id
    }

    fun setRoute_id(route_id: String?) {
        this.route_id = route_id
    }

    fun getId(): Int {
        return id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getProducts(): RealmList<ProductOld>? {
        return products
    }

    fun setProducts(products: RealmList<ProductOld>?) {
        this.products = products
    }

    fun getTotal(): Long {
        return total
    }

    fun setTotal(total: Long) {
        this.total = total
    }

    fun getSub_total(): Long {
        return sub_total
    }

    fun setSub_total(sub_total: Long) {
        this.sub_total = sub_total
    }

    fun getTax(): Long {
        return tax
    }

    fun setTax(tax: Long) {
        this.tax = tax
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String?) {
        this.date = date
    }

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String?) {
        this.image = image
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String?) {
        this.type = type
    }
}
