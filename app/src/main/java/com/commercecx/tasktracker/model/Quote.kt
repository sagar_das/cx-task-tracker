package com.commercecx.tasktracker.model

import com.commercecx.tasktracker.model.dao.QuoteDBDao
import com.commercecx.tasktracker.util.extensions.stringFromKey
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Sagar Das on 11/17/20.
 */
data class Quote(
    val id: String,
    val name: String,
    val quotenumber: String,
    val billingStreet: String,
    val billingCity: String,
    val billingState: String,
    val billingPostalCode: String,
    val billingCountry: String,
    val shippingStreet: String,
    val shippingCity: String,
    val shippingState: String,
    val shippingPostalCode: String,
    val shippingCountry: String
) : Serializable {

    object Mapper {

        fun from(quote: JSONObject) = Quote(
            quote.stringFromKey("Id"),
            quote.stringFromKey("Name"),
            quote.stringFromKey("QuoteNumber"),
            quote.stringFromKey("BillingStreet"),
            quote.stringFromKey("BillingCity"),
            quote.stringFromKey("BillingState"),
            quote.stringFromKey("BillingPostalCode"),
            quote.stringFromKey("BillingCountry"),
            quote.stringFromKey("ShippingStreet"),
            quote.stringFromKey("ShippingCity"),
            quote.stringFromKey("ShippingState"),
            quote.stringFromKey("ShippingPostalCode"),
            quote.stringFromKey("ShippingCountry")

        )

        fun fromDB(dbQuote: JSONObject) = Quote(
            dbQuote.optString(QuoteDBDao.ID, "ID"),
            dbQuote.optString(QuoteDBDao.NAME, "NAME"),
            dbQuote.optString(QuoteDBDao.QUOTENUMBER, "QUOTENUMBER"),
            dbQuote.optString(QuoteDBDao.BILLINGSTREET, "BILLINGSTREET"),
            dbQuote.optString(QuoteDBDao.BILLINGCITY, "BILLINGCITY"),
            dbQuote.optString(QuoteDBDao.BILLINGSTATE, "BILLINGSTATE"),
            dbQuote.optString(QuoteDBDao.BILLINGPOSTALCODE, "BILLINGPOSTALCODE"),
            dbQuote.optString(QuoteDBDao.BILLINGCOUNTRY, "BILLINGCOUNTRY"),
            dbQuote.optString(QuoteDBDao.SHIPPINGSTREET, "SHIPPINGSTREET"),
            dbQuote.optString(QuoteDBDao.SHIPPINGCITY, "SHIPPINGCITY"),
            dbQuote.optString(QuoteDBDao.SHIPPINGSTATE, "SHIPPINGSTATE"),
            dbQuote.optString(QuoteDBDao.SHIPPINGPOSTALCODE, "SHIPPINGPOSTALCODE"),
            dbQuote.optString(QuoteDBDao.SHIPPINGCOUNTRY, "SHIPPINGCOUNTRY")

        )

        fun from(quoteArray: JSONArray): List<Quote> {
            val quotes = mutableListOf<Quote>()
            for (i in 0 until quoteArray.length()) {
                quotes.add(Quote.Mapper.from(quoteArray.getJSONObject(i)))
            }
            return quotes
        }

        fun fromDB(quoteArray: JSONArray): List<Quote> {
            val quotes = mutableListOf<Quote>()
            for (i in 0 until quoteArray.length()) {
                quotes.add(Quote.Mapper.fromDB(quoteArray.getJSONObject(i)))
            }
            return quotes
        }

    }



}