package com.commercecx.tasktracker.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Sagar Das on 8/19/20.
 */
open class CartOld : RealmObject() {
    @PrimaryKey
    private var id = 0
    private var name: String? = null
    private var oldProducts: RealmList<ProductOld>? = null
    private var total: Long = 0
    private var sub_total: Long = 0
    private var tax: Long = 0
    private var date: String? = null
    private var previousProducts: RealmList<ProductOld>? = null

    fun getId(): Int {
        return id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getOldProducts(): RealmList<ProductOld>? {
        return oldProducts
    }

    fun setOldProducts(products: RealmList<ProductOld>?) {
        this.oldProducts = products
    }

    fun getTotal(): Long {
        return total
    }

    fun setTotal(total: Long) {
        this.total = total
    }

    fun getSub_total(): Long {
        return sub_total
    }

    fun setSub_total(sub_total: Long) {
        this.sub_total = sub_total
    }

    fun getTax(): Long {
        return tax
    }

    fun setTax(tax: Long) {
        this.tax = tax
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String?) {
        this.date = date
    }

    fun getPreviousProducts(): RealmList<ProductOld>? {
        return previousProducts
    }

    fun setPreviousProducts(previousProducts: RealmList<ProductOld>?) {
        this.previousProducts = previousProducts
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }
}
