package com.commercecx.tasktracker.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Sagar Das on 8/19/20.
 */


open class Notification : RealmObject() {
    @PrimaryKey
    @SerializedName("notification_id")
    private var notification_id: String? = null

    @SerializedName("title")
    private var title: String? = null

    @SerializedName("contents")
    private var contents: String? = null

    @SerializedName("type")
    private var type: String? = null

    @SerializedName("isRead")
    private var isRead = false

    @SerializedName("updatedTSMP")
    private var updatedTSMP: Long = 0
    fun getUpdatedTSMP(): Long {
        return updatedTSMP
    }

    fun setUpdatedTSMP(updatedTSMP: Long) {
        this.updatedTSMP = updatedTSMP
    }

    fun isRead(): Boolean {
        return isRead
    }

    fun setRead(read: Boolean) {
        isRead = read
    }

    fun getNotification_id(): String? {
        return notification_id
    }

    fun setNotification_id(notification_id: String?) {
        this.notification_id = notification_id
    }

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getContents(): String? {
        return contents
    }

    fun setContents(contents: String?) {
        this.contents = contents
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String?) {
        this.type = type
    }
}
