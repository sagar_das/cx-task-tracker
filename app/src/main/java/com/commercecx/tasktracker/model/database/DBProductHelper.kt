package com.commercecx.tasktracker.model.database

import com.commercecx.tasktracker.model.Category
import com.commercecx.tasktracker.model.Product
import com.commercecx.tasktracker.model.dao.ProductDBDao


import com.salesforce.androidsdk.smartstore.store.QuerySpec


//TODO: Check if it's possible to batch insert/update (sending jsonarrays)

class DBProductHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "PRODUCTS"

    }


    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = ProductDBDao.getIndexSpec()


    fun insertProducts(products: List<Product>) {
        dropTable()
        getSmartStore().registerSoup(TABLE_NAME, ProductDBDao.getIndexSpec())
        products.forEach { getSmartStore().create(TABLE_NAME, ProductDBDao(it)) }
        //JDELog.e("inserted, there are... {${getAllProducts().size}} products in table")
    }

    fun updateProducts(products: List<Product>) {
        clearTable()
        products.forEach {
            insertOrUpdate(ProductDBDao.PRODUCTID, ProductDBDao(it))
        }
        //JDELog.e("updated products, there are... {${getAllProducts().size}} products in table")
    }

    fun getAllProducts(includeInactive: Boolean = false): List<Product> {
        val result = if (includeInactive) {
            getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, ProductDBDao.PRODUCTID, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        } else {
            getSmartStore().query(
                QuerySpec.buildExactQuerySpec(TABLE_NAME, ProductDBDao.ISACTIVE, "true", ProductDBDao.PRODUCTID, QuerySpec.Order.ascending, PAGE_SIZE),
                FIRST_PAGE_INDEX
            )
        }

        return Product.Mapper.fromDB(result)

    }

    /*
    buildLikeQuerySpec(String soupName, String path, String likeKey, String orderPath, Order order, int pageSize)
    buildLikeQuerySpec(String soupName, String[] selectPaths, String path, String likeKey, String orderPath, Order order, int pageSize) {

     */
    fun getProductByName(productName: String): List<Product> {
        val result = getSmartStore().query(QuerySpec.buildLikeQuerySpec(TABLE_NAME,ProductDBDao.PRODUCTNAME, productName, ProductDBDao.ACCOUNTPRODUCTKEY, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return Product.Mapper.fromDB(result)
    }


    fun getCategories(includeInactive: Boolean = false): List<Category> {
        val query =
            if (includeInactive) {
                "SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.CATEGORY}} FROM {$TABLE_NAME}"
            } else {
                "SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.CATEGORY}} FROM {$TABLE_NAME} WHERE {$TABLE_NAME:${ProductDBDao.ISACTIVE}}  = \"true\""
            }

        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(query, PAGE_SIZE),
            FIRST_PAGE_INDEX
        )

        val categories = mutableListOf<Category>()
        for (i in 0 until result.length()) {
            categories.add(Category(i.toLong(), result.getJSONArray(i).getString(0), 0))
        }
        return categories
    }

    fun getSubCategories(category: String, includeInactive: Boolean = false): List<Category> {
        val query = if (includeInactive) {
            "SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}} FROM {$TABLE_NAME} " +
                    "WHERE {$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\""
        } else {
            "SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}} FROM {$TABLE_NAME} " +
                    "WHERE {$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\" AND {$TABLE_NAME:${ProductDBDao.ISACTIVE}}  = \"true\""
        }


        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                query,
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )

        val subcategories = mutableListOf<Category>()
        for (i in 0 until result.length()) {
            subcategories.add(Category(i.toLong(), result.getJSONArray(i).getString(0), 0))
        }
        return subcategories
    }

    fun getBrands(category: String, subcategory: String, includeInactive: Boolean = false): List<Category> {
        val query = if (includeInactive) {
            "SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.SUBSUBCATEGORY}} FROM {$TABLE_NAME} " +
                    "WHERE ({$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\" AND  {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}}  = \"$subcategory\""
        } else {
            "SELECT DISTINCT {$TABLE_NAME:${ProductDBDao.SUBSUBCATEGORY}} FROM {$TABLE_NAME} " +
                    "WHERE ({$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\" AND  {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}}  = \"$subcategory\" AND {$TABLE_NAME:${ProductDBDao.ISACTIVE}}  = \"true\")"
        }

        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                query,
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val brands = mutableListOf<Category>()
        for (i in 0 until result.length()) {
            brands.add(Category(i.toLong(), result.getJSONArray(i).getString(0), 0))
        }
        return brands
    }


    fun getProducts(category: String, subcategory: String, brand: String, includeInactive: Boolean = false): List<Product> {
        val query = if (includeInactive) {
            "SELECT * FROM {$TABLE_NAME} " +
                    "WHERE ({$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\" " +
                    "AND  {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}}  = \"$subcategory\" " +
                    "AND  {$TABLE_NAME:${ProductDBDao.SUBSUBCATEGORY}}  = \"$brand\")"
        } else {
            "SELECT * FROM {$TABLE_NAME} " +
                    "WHERE ({$TABLE_NAME:${ProductDBDao.CATEGORY}}  = \"$category\" " +
                    "AND  {$TABLE_NAME:${ProductDBDao.SUBCATEGORY}}  = \"$subcategory\" " +
                    "AND  {$TABLE_NAME:${ProductDBDao.SUBSUBCATEGORY}}  = \"$brand\" " +
                    "AND {$TABLE_NAME:${ProductDBDao.ISACTIVE}}  = \"true\"" +
                    ")"

        }

        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                query,
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val products = mutableListOf<Product>()
        for (i in 0 until result.length()) {
            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return products
    }

    fun getProductsById(productId: String): List<Product> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${ProductDBDao.PRODUCTID}}  = \"$productId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val products = mutableListOf<Product>()
        for (i in 0 until result.length()) {
            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return products
    }

    fun getProductById(productId: String): Product? {
        val result = getSmartStore().query(
                QuerySpec.buildSmartQuerySpec(
                        "SELECT * FROM {$TABLE_NAME} " +
                                "WHERE ({$TABLE_NAME:${ProductDBDao.PRODUCTID}}  = \"$productId\")",
                        PAGE_SIZE
                ),
                FIRST_PAGE_INDEX
        )
        val products = mutableListOf<Product>()
        for (i in 0 until result.length()) {
            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return products.firstOrNull()
    }



    fun getProductByCode(productCode: String): Product? {
        val result = getSmartStore().query(
                QuerySpec.buildSmartQuerySpec(
                        "SELECT * FROM {$TABLE_NAME} " +
                                "WHERE ({$TABLE_NAME:${ProductDBDao.PRODUCTCODE}}  = \"$productCode\")",
                        PAGE_SIZE
                ),
                FIRST_PAGE_INDEX
        )
        val products = mutableListOf<Product>()
        for (i in 0 until result.length()) {
            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return products.firstOrNull()
    }

    fun getProductByProductCode(productCode: String): List<Product> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${ProductDBDao.PRODUCTCODE}}  = \"$productCode\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val products = mutableListOf<Product>()
        for (i in 0 until result.length()) {
            products.add(Product.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return products
    }

    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, ProductDBDao.PRODUCTID, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}