package com.commercecx.tasktracker.model.database

import com.commercecx.tasktracker.model.ProjectTask
import com.commercecx.tasktracker.model.dao.ProjectTaskDBDao
import com.salesforce.androidsdk.smartstore.store.QuerySpec

/**
 * Created by Sagar Das on 11/19/20.
 */
class DBProjectTaskHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "PRICINGTASKS"

    }


    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = ProjectTaskDBDao.getIndexSpec()


    fun insertProjectTasks(projectTasks: List<ProjectTask>) {
        dropTable()
        getSmartStore().registerSoup(TABLE_NAME, ProjectTaskDBDao.getIndexSpec())
        projectTasks.forEach { getSmartStore().create(TABLE_NAME, ProjectTaskDBDao(it)) }
        //JDELog.e("inserted, there are... {${getAllProjectTasks().size}} projectTasks in table")
    }

    fun updateProjectTasks(projectTasks: List<ProjectTask>) {

        clearTable()

        projectTasks.forEach {
            insertOrUpdate(ProjectTaskDBDao.ID, ProjectTaskDBDao(it))
        }
        //JDELog.e("updated projectTasks, there are... {${getAllProjectTasks().size}} projectTasks in table")
    }

    fun getAllProjectTasks(): List<ProjectTask> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, ProjectTaskDBDao.ID, QuerySpec.Order.ascending, 100), FIRST_PAGE_INDEX)

        return ProjectTask.Mapper.fromDB(result)

    }

    /*
   buildLikeQuerySpec(String soupName, String path, String likeKey, String orderPath, Order order, int pageSize)
   buildLikeQuerySpec(String soupName, String[] selectPaths, String path, String likeKey, String orderPath, Order order, int pageSize) {

    */
    fun getProjectTaskByName(accountName: String): List<ProjectTask> {
        val result = getSmartStore().query(
            QuerySpec.buildLikeQuerySpec(
                DBProjectTaskHelper.TABLE_NAME,
                ProjectTaskDBDao.NAME, accountName, ProjectTaskDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return ProjectTask.Mapper.fromDB(result)
    }

    fun getProjectTaskByProjectName(projectName: String): List<ProjectTask> {
        val result = getSmartStore().query(
            QuerySpec.buildLikeQuerySpec(
                DBProjectTaskHelper.TABLE_NAME,
                ProjectTaskDBDao.PROJECTNAME, projectName, ProjectTaskDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return ProjectTask.Mapper.fromDB(result)
    }

    fun getProjectTaskByCountryName(countryName: String): List<ProjectTask> {
        val result = getSmartStore().query(
            QuerySpec.buildLikeQuerySpec(
                DBProjectTaskHelper.TABLE_NAME,
                ProjectTaskDBDao.DELIVERYCOUNTRY, countryName, ProjectTaskDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return ProjectTask.Mapper.fromDB(result)
    }

    fun getProjectTaskByProductName(productName: String): List<ProjectTask> {
        val result = getSmartStore().query(
            QuerySpec.buildLikeQuerySpec(
                DBProjectTaskHelper.TABLE_NAME,
                ProjectTaskDBDao.PRODUCT, productName, ProjectTaskDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return ProjectTask.Mapper.fromDB(result)
    }




    fun getProjectTasksById(taskID: String): List<ProjectTask> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${ProjectTaskDBDao.ID}}  = \"$taskID\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val projectTasks = mutableListOf<ProjectTask>()
        for (i in 0 until result.length()) {
            projectTasks.add(ProjectTask.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return projectTasks
    }

    fun getProjectTasksByProjectId(taskID: String): List<ProjectTask> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${ProjectTaskDBDao.PROJECTNAME}}  = \"$taskID\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val projectTasks = mutableListOf<ProjectTask>()
        for (i in 0 until result.length()) {
            projectTasks.add(ProjectTask.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return projectTasks
    }

    fun getProjectTaskById(accountId: String): ProjectTask? {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${ProjectTaskDBDao.ID}}  = \"$accountId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val projectTasks = mutableListOf<ProjectTask>()
        for (i in 0 until result.length()) {
            projectTasks.add(ProjectTask.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return projectTasks.firstOrNull()
    }





    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, ProjectTaskDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}