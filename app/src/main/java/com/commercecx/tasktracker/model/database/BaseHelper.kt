package com.commercecx.tasktracker.model.database

import android.util.Log
import com.commercecx.tasktracker.application.BaseApplication


import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.QuerySpec
import org.json.JSONObject

abstract class BaseHelper{
    companion object {
        val FIRST_PAGE_INDEX = 0
        val PAGE_SIZE = -1
    }

    init {
        getSmartStore().registerSoup(getTableName(), getIndexSpecs())
    }

    abstract fun getTableName(): String
    abstract fun getIndexSpecs(): Array<IndexSpec>
    abstract fun getAllQuerySpec(): QuerySpec


    fun dropTable() {
        getSmartStore().dropSoup(getTableName())
    }

    fun clearTable(){
        getSmartStore().clearSoup(getTableName())
    }

    fun insertOrUpdate(field: String, jsonObject: JSONObject): Long {
        val soupEntryId = getSmartStore().lookupSoupEntryId(getTableName(), field, jsonObject.getString(field))
        if (soupEntryId == -1L) {
            //JDELog.w("Creating product")
            getSmartStore().create(getTableName(), jsonObject)
        } else {
            //JDELog.w("Updating")
            getSmartStore().update(getTableName(), jsonObject, soupEntryId)
        }
        return soupEntryId
    }

    fun upsert(jsonObject: JSONObject){
        try {
            val y = getSmartStore().upsert(getTableName(), jsonObject)
            Log.i("TAG", "upsert: ")
        }
        catch (e: Exception){
            Log.e("TAG", "upsert: ",e )
        }

    }

    fun deleteAll() {
        getSmartStore().deleteByQuery(getTableName(), getAllQuerySpec())
    }

    fun getSmartStore() = BaseApplication.instance.smartStore


}