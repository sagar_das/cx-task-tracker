package com.commercecx.tasktracker.model

/**
 * Created by Sagar Das on 8/19/20.
 */
import com.google.gson.annotations.SerializedName

import io.realm.RealmObject

open class CartProduct : RealmObject() {
    /*
    {
      "_id": "5b2d202faf2a8d395eb3e42b",
      "index": 0,
      "name": "ipsum fugiat commodo",
      "weight": "20 x 100 cups",
      "date": "Thu May 29 2014 23:45:25 GMT+0000 (UTC)"
    }
     */
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("index")
    var index = 0

    @SerializedName("guid")
    var guid: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("item_qty")
    var item_qty: String? = null

    @SerializedName("item_price")
    var item_price: String? = null

    @SerializedName("item_packs")
    var item_pack: String? = null

    @SerializedName("item_tax")
    var item_tax: String? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("image")
    var image: String? = null
}
