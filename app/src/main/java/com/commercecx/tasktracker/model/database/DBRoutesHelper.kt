package com.commercecx.tasktracker.model.database

import com.commercecx.tasktracker.model.Route
import com.commercecx.tasktracker.model.dao.RouteDBDao

import com.salesforce.androidsdk.smartstore.store.QuerySpec
import java.util.*


class DBRoutesHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "ROUTES"
    }

    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = RouteDBDao.getIndexSpec()

    fun insertRoutes(routes: List<Route>) {
        routes.forEach {
            insertOrUpdate(RouteDBDao.CASEID, RouteDBDao(it))
        }
    }

    fun getAllRoutes(): List<Route> {
        val result = getSmartStore().query(

                QuerySpec.buildAllQuerySpec(
                        TABLE_NAME,
                        RouteDBDao.VISITED,
                        QuerySpec.Order.ascending,
                        PAGE_SIZE
                ), FIRST_PAGE_INDEX
        )
        return Route.Mapper.fromDB(result)
    }


    fun updateRouteStatus(caseId: String, time: Long) {
        getRouteById(caseId)?.let {
            it.dispatched = (time == 0L)
            it.timeVisited = time
            insertOrUpdate(RouteDBDao.CASEID, RouteDBDao(it))
        }

    }

    fun getRouteById(id: String): Route? {
        val result = getSmartStore().query(
                getRouteByIdQuery(id)
                , FIRST_PAGE_INDEX
        )

        return Route.Mapper.fromDB(result).firstOrNull()
    }

    fun getRouteByIdQuery(id: String) =
            QuerySpec.buildExactQuerySpec(
                    TABLE_NAME, RouteDBDao.CASEID, id, RouteDBDao.VISITED,
                    QuerySpec.Order.ascending,
                    PAGE_SIZE
            )


    fun deletePreviousRoutes(keepFrom: Date) {
        val limit = Calendar.getInstance()
        limit.time = keepFrom

        val cal = Calendar.getInstance()

        getAllRoutes().forEach {
            cal.time = it.time
            if (cal.get(Calendar.DAY_OF_MONTH) != limit.get(Calendar.DAY_OF_MONTH)) {
                getSmartStore().deleteByQuery(TABLE_NAME, getRouteByIdQuery(it.caseId))

            }
        }

    }

    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, RouteDBDao.CASEID, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}
