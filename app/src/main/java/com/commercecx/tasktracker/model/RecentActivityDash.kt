package com.commercecx.tasktracker.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Sagar Das on 8/19/20.
 */


open class RecentActivityDash : RealmObject() {
    @PrimaryKey
    private var id = 0
    private var activity_name: String? = null
    private var activity_date: String? = null
    fun getId(): Int {
        return id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getActivity_name(): String? {
        return activity_name
    }

    fun setActivity_name(activity_name: String?) {
        this.activity_name = activity_name
    }

    fun getActivity_date(): String? {
        return activity_date
    }

    fun setActivity_date(activity_date: String?) {
        this.activity_date = activity_date
    }
}
