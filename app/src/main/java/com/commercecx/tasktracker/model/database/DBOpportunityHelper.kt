package com.commercecx.tasktracker.model.database

import com.commercecx.tasktracker.model.Opportunity
import com.commercecx.tasktracker.model.dao.OpportunityDBDao
import com.salesforce.androidsdk.smartstore.store.QuerySpec

/**
 * Created by Sagar Das on 12/9/20.
 */
class DBOpportunityHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "OPPORTUNITY"

    }


    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = OpportunityDBDao.getIndexSpec()


    fun insertOpportunitys(opportunitys: List<Opportunity>) {
        dropTable()
        getSmartStore().registerSoup(TABLE_NAME, OpportunityDBDao.getIndexSpec())
        opportunitys.forEach { getSmartStore().create(TABLE_NAME, OpportunityDBDao(it)) }
        //JDELog.e("inserted, there are... {${getAllOpportunitys().size}} opportunitys in table")
    }

    fun updateOpportunitys(opportunitys: List<Opportunity>, clearSoup: Boolean ) {
        if(clearSoup){
            clearTable()
        }

        opportunitys.forEach {
            insertOrUpdate(OpportunityDBDao.ID, OpportunityDBDao(it))
        }
        //JDELog.e("updated opportunitys, there are... {${getAllOpportunitys().size}} opportunitys in table")
    }

    fun getAllOpportunitys(): List<Opportunity> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, OpportunityDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)

        return Opportunity.Mapper.fromDB(result)

    }

    /*
   buildLikeQuerySpec(String soupName, String path, String likeKey, String orderPath, Order order, int pageSize)
   buildLikeQuerySpec(String soupName, String[] selectPaths, String path, String likeKey, String orderPath, Order order, int pageSize) {

    */
    fun getOpportunityByName(opportunityName: String): List<Opportunity> {
        val result = getSmartStore().query(QuerySpec.buildLikeQuerySpec(
            DBOpportunityHelper.TABLE_NAME,
            OpportunityDBDao.NAME, opportunityName, OpportunityDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return Opportunity.Mapper.fromDB(result)
    }




    fun getOpportunitysById(opportunityId: String): List<Opportunity> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${OpportunityDBDao.ID}}  = \"$opportunityId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val opportunitys = mutableListOf<Opportunity>()
        for (i in 0 until result.length()) {
            opportunitys.add(Opportunity.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return opportunitys
    }

    fun getOpportunityById(opportunityId: String): Opportunity? {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${OpportunityDBDao.ID}}  = \"$opportunityId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val opportunitys = mutableListOf<Opportunity>()
        for (i in 0 until result.length()) {
            opportunitys.add(Opportunity.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return opportunitys.firstOrNull()
    }





    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, OpportunityDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}