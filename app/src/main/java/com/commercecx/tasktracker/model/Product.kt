package com.commercecx.tasktracker.model

import com.commercecx.tasktracker.model.dao.ProductDBDao
import com.commercecx.tasktracker.util.extensions.stringFromKey
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Sagar Das on 8/21/20.
 */
data class Product(
    var uom: String,
    val subsubcategory: String,
    val subcategory: String,
    val routesales: Boolean,
    val productname: String,
    val productimage: String,
    val productid: String,
    val productcode: String,
    val active: Boolean,
    val category: String,
    val consignment: Boolean,
    val isContractedProduct: Boolean,
    var accountID: String? = "",
    var accountIDProductId: String? = "",
    var productquantity: Int? = null,
    var productdiscount: Int? = null,
    var productdiscountreason: DiscountReason? = null
) : Serializable {

    object Mapper {
//        fun from(product: JSONObject): Product {
//
//            val pbEntry = product["PricebookEntries"] as JSONObject
//            val records = pbEntry["records"] as JSONArray
//
//            val priceBookDetails = records.get(0) as JSONObject
//            val unitPrice = priceBookDetails["UnitPrice"].toString()
//
//
//            return Product(
//                "",
//                "",
//                "",
//                false,
//                product.jdeString("Name"),
//                product.jdeString("DisplayUrl"),
//                product.jdeString("Id"),
//                unitPrice,
//                true,
//                "",
//                false,
//                false
//
//            )
//        }


        fun from(product: JSONObject) = Product(
            "",
            "",
            "",
            false,
            product.stringFromKey("Name"),
            product.stringFromKey("DisplayUrl"),
            product.stringFromKey("Id"),
            product.stringFromKey("Description"),
            true,
            "",
            false,
            false

        )


        fun from(product: JSONObject, isContracted: Boolean = false, needsAccountId: Boolean) = Product(
            product.stringFromKey("UOM"),
            product.stringFromKey("SubSubCategory"),
            product.stringFromKey("SubCategory"),
            product.optBoolean("RouteSales", false),
            product.stringFromKey("Name"),
            product.stringFromKey("ProductImage"),
            product.stringFromKey("ProductID"),
            product.stringFromKey("ProductCode"),
            product.optBoolean("IsActive", true),
            product.stringFromKey("Category"),
            product.optBoolean("Consignment", false),
            isContracted,
            product.stringFromKey("AccountID"),
            product.stringFromKey("AccountID")+product.stringFromKey("ProductID")

        )

        fun fromContracted(product: JSONObject) = Product(
            product.stringFromKey("UOM"),
            product.stringFromKey("SubSubCategory"),
            product.stringFromKey("SubCategory"),
            product.optBoolean("RouteSales", false),
            product.stringFromKey("ProductName"),
            product.stringFromKey("ProductImage"),
            product.stringFromKey("ProductID"),
            product.stringFromKey("ProductCode"),
            product.optBoolean("IsActive", true),
            product.stringFromKey("Category"),
            product.optBoolean("Consignment", false),
            true

        )


        fun fromDB(dbProduct: JSONObject) = Product(
            dbProduct.optString(ProductDBDao.UOM, "UOM"),
            dbProduct.optString(ProductDBDao.SUBSUBCATEGORY, "SubSubCategory"),
            dbProduct.optString(ProductDBDao.SUBCATEGORY, "SubCategory"),
            dbProduct.optString(ProductDBDao.ROUTESALES, "true").equals("true", true),
            dbProduct.optString(ProductDBDao.PRODUCTNAME, "ProductName"),
            dbProduct.optString(ProductDBDao.PRODUCTIMAGE, "ProductImage"),
            dbProduct.optString(ProductDBDao.PRODUCTID, "ProductID"),
            dbProduct.optString(ProductDBDao.PRODUCTCODE, "ProductCode"),
            dbProduct.optString(ProductDBDao.ISACTIVE, "true").equals("true", true),
            dbProduct.optString(ProductDBDao.CATEGORY, "Category"),
            dbProduct.optString(ProductDBDao.CONSIGNMENT, "true").equals("true", true),
            dbProduct.optString(ProductDBDao.ISCONTRACTED, "false").equals("true", true),
            dbProduct.optString(ProductDBDao.ACCOUNTID, ""),
            dbProduct.optString(ProductDBDao.ACCOUNTPRODUCTKEY, "")

        )

        fun from(productArray: JSONArray): List<Product> {
            val products = mutableListOf<Product>()
            for (i in 0 until productArray.length()) {
                products.add(Product.Mapper.from(productArray.getJSONObject(i)))
            }
            return products
        }


        fun from(productArray: JSONArray, isContractedProduct: Boolean = false, needsAccountId: Boolean = false): List<Product> {
            val products = mutableListOf<Product>()
            for (i in 0 until productArray.length()) {
                products.add(Product.Mapper.from(productArray.getJSONObject(i), isContractedProduct, needsAccountId))
            }
            return products
        }

        fun fromDB(productArray: JSONArray): List<Product> {
            val products = mutableListOf<Product>()
            for (i in 0 until productArray.length()) {
                products.add(Product.Mapper.fromDB(productArray.getJSONObject(i)))
            }
            return products
        }


    }

    enum class DiscountReason(val reasonCode: String) {
        PROMOTION_DISCOUNT("1"), LOYALTY("2"), SALES_PROMOTION("3");
    }
}