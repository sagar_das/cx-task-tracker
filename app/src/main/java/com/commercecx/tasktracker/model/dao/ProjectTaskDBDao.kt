package com.commercecx.tasktracker.model.dao

import com.commercecx.tasktracker.model.ProjectTask
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject

/**
 * Created by Sagar Das on 11/19/20.
 */
class ProjectTaskDBDao(projectTask: ProjectTask) : JSONObject() {


    companion object {

        const val ID = "Id"
        const val NAME = "Name"
        const val PROJECTNAME = "PROJECTNAME"
        const val BUDGETHOURS = "BUDGETHOURS"
        const val ROLE = "ROLE"
        const val PRODUCT = "PRODUCT"
        const val DELIVERYCOUNTRY = "DELIVERYCOUNTRY"
     



        fun getIndexSpec() =
            arrayOf(
                IndexSpec(ID, SmartStore.Type.string),
                IndexSpec(NAME, SmartStore.Type.string),
                IndexSpec(PROJECTNAME, SmartStore.Type.string),
                IndexSpec(BUDGETHOURS, SmartStore.Type.string),
                IndexSpec(ROLE, SmartStore.Type.string),
                IndexSpec(PRODUCT, SmartStore.Type.string),
                IndexSpec(DELIVERYCOUNTRY, SmartStore.Type.string)
            

            )
    }

    init {
        put(ID, projectTask.id)
        put(NAME, projectTask.name)
        put(PROJECTNAME, projectTask.projectName)
        put(BUDGETHOURS, projectTask.budgetHours)
        put(ROLE, projectTask.numberOfUnits)
        put(PRODUCT, projectTask.product)
        put(DELIVERYCOUNTRY, projectTask.deliveryCountry)




    }
}