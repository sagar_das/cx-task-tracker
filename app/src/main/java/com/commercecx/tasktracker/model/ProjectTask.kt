package com.commercecx.tasktracker.model

import com.commercecx.tasktracker.model.dao.ProjectTaskDBDao
import com.commercecx.tasktracker.util.extensions.stringFromKey
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Sagar Das on 11/19/20.
 */
data class ProjectTask(
    val id: String,
    val name: String,
    val projectName: String,
    val budgetHours: String,
    val numberOfUnits: String,
    val product: String,
    val deliveryCountry: String
) : Serializable {

    object Mapper {



        //pse__Project_Task__c object
//        fun from(projectTask: JSONObject) = ProjectTask(
//            projectTask.stringFromKey("Id"),
//            projectTask.stringFromKey("Name"),
//            projectTask.stringFromKey("pse__Project__c"),
//            projectTask.stringFromKey("Hours_Per_Unit__c"),
//            projectTask.stringFromKey("Number_of_Units__c"),
//            projectTask.stringFromKey("Product__c"),
//            projectTask.stringFromKey("Delivery_Country__c")
//
//        )


        //Custom Project Task object
        fun from(projectTask: JSONObject) = ProjectTask(
            projectTask.stringFromKey("Id"),
            projectTask.stringFromKey("Name"),
            projectTask.stringFromKey("Project__c"),
            projectTask.stringFromKey("Hours_Per_Unit__c"),
            projectTask.stringFromKey("Number_of_Units__c"),
            projectTask.stringFromKey("Product__c"),
            projectTask.stringFromKey("Delivery_Country__c")

        )

        fun fromDB(dbProjectTask: JSONObject) = ProjectTask(
            dbProjectTask.optString(ProjectTaskDBDao.ID, "ID"),
            dbProjectTask.optString(ProjectTaskDBDao.NAME, "NAME"),
            dbProjectTask.optString(ProjectTaskDBDao.PROJECTNAME, "PROJECTNAME"),
            dbProjectTask.optString(ProjectTaskDBDao.BUDGETHOURS, "BUDGETHOURS"),
            dbProjectTask.optString(ProjectTaskDBDao.ROLE, "ROLE"),
            dbProjectTask.optString(ProjectTaskDBDao.PRODUCT, "PRODUCT"),
            dbProjectTask.optString(ProjectTaskDBDao.DELIVERYCOUNTRY, "DELIVERYCOUNTRY")


        )

        fun from(projectTaskArray: JSONArray): List<ProjectTask> {
            val projectTasks = mutableListOf<ProjectTask>()
            for (i in 0 until projectTaskArray.length()) {
                projectTasks.add(from(projectTaskArray.getJSONObject(i)))
            }
            return projectTasks
        }

        fun fromDB(projectTaskArray: JSONArray): List<ProjectTask> {
            val projectTasks = mutableListOf<ProjectTask>()
            for (i in 0 until projectTaskArray.length()) {
                projectTasks.add(fromDB(projectTaskArray.getJSONObject(i)))
            }
            return projectTasks
        }

    }



}