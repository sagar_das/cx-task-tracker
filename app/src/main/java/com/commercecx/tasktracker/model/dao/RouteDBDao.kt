package com.commercecx.tasktracker.model.dao


import com.commercecx.tasktracker.model.Route
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject


class RouteDBDao(route: Route) : JSONObject() {
    companion object {
        const val ACCOUNTID = "accountId"
        const val ACCOUNTNAME = "accountName"
        const val CASEID = "caseId"
        const val CITY = "city"
        const val CONTACTEMAIL = "contactEmail"
        const val CONTACTFIRSTNAME = "contactFirstName"
        const val CONTACTID = "contactId"
        const val CONTACTLASTNAME = "contactLastName"
        const val CONTACTPHONE = "contactPhone"
        const val COUNTRY = "country"
        const val HOUSENUMBER = "houseNumber"
        const val LOCATIONNAME = "locationName"
        const val ORDERNONCONTRACTEDPRODUCTS = "ordernoncontractedproducts"
        const val POSTALCODE = "postalCode"
        const val PONUMBER = "ponumber"
        const val PONUMBERREQUIRED = "ponumberrequired"
        const val CUSTOMERID = "customerId"
        const val SALESFORCEACCOUNTLOCATIONID = "sapAccountLocationId"
        const val STATE = "state"
        const val STREET = "street"
        const val TIME = "time"
        const val TIME_VISITED = "time_visited"
        const val VISITED = "visited"

        fun getIndexSpec() =
                arrayOf(
                        IndexSpec(ACCOUNTID, SmartStore.Type.string),
                        IndexSpec(ACCOUNTNAME, SmartStore.Type.string),
                        IndexSpec(CASEID, SmartStore.Type.string),
                        IndexSpec(CITY, SmartStore.Type.string),
                        IndexSpec(CONTACTEMAIL, SmartStore.Type.string),
                        IndexSpec(CONTACTFIRSTNAME, SmartStore.Type.string),
                        IndexSpec(CONTACTID, SmartStore.Type.string),
                        IndexSpec(CONTACTLASTNAME, SmartStore.Type.string),
                        IndexSpec(CONTACTPHONE, SmartStore.Type.string),
                        IndexSpec(COUNTRY, SmartStore.Type.string),
                        IndexSpec(HOUSENUMBER, SmartStore.Type.string),
                        IndexSpec(LOCATIONNAME, SmartStore.Type.string),
                        IndexSpec(POSTALCODE, SmartStore.Type.string),
                        IndexSpec(CUSTOMERID, SmartStore.Type.string),
                        IndexSpec(SALESFORCEACCOUNTLOCATIONID, SmartStore.Type.string),
                        IndexSpec(STATE, SmartStore.Type.string),
                        IndexSpec(STREET, SmartStore.Type.string),
                        IndexSpec(TIME, SmartStore.Type.string),
                        IndexSpec(TIME_VISITED, SmartStore.Type.string),
                        IndexSpec(VISITED, SmartStore.Type.string)
                )
    }

    init {
        put(ACCOUNTID, route.accountId)
        put(ACCOUNTNAME, route.accountName)
        put(CASEID, route.caseId)
        put(CITY, route.city)
        put(CONTACTEMAIL, route.contactEmail)
        put(CONTACTFIRSTNAME, route.contactFirstName)
        put(CONTACTID, route.contactId)
        put(CONTACTLASTNAME, route.contactLastName)
        put(CONTACTPHONE, route.contactPhone)
        put(COUNTRY, route.country)
        put(HOUSENUMBER, route.houseNumber)
        put(LOCATIONNAME, route.locationName)
        put(
                ORDERNONCONTRACTEDPRODUCTS, if (route.orderNonContractedProducts) {
            "true"
        } else {
            "false"
        }
        )
        put(POSTALCODE, route.postalCode)
        put(PONUMBER, route.poNumber)
        put(
                PONUMBERREQUIRED, if (route.poNumberRequired) {
            "true"
        } else {
            "false"
        }
        )

        put(CUSTOMERID, route.customerId)
        put(SALESFORCEACCOUNTLOCATIONID, route.sapAccountLocationId)
        put(STATE, route.state)
        put(STREET, route.street)
        put(TIME, route.time.time)
        put(TIME_VISITED, route.timeVisited)
        put(
                VISITED, if (route.dispatched) {
            "true"
        } else {
            "false"
        }
        )
    }


}
