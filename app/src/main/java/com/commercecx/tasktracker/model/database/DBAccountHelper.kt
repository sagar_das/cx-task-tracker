package com.commercecx.tasktracker.model.database

import com.commercecx.tasktracker.model.Account

import com.commercecx.tasktracker.model.dao.AccountDBDao
import com.salesforce.androidsdk.smartstore.store.QuerySpec

/**
 * Created by Sagar Das on 8/25/20.
 */

class DBAccountHelper : BaseHelper() {
    companion object {
        private const val TABLE_NAME = "ACCOUNTS"

    }


    override fun getTableName() = TABLE_NAME

    override fun getIndexSpecs() = AccountDBDao.getIndexSpec()


    fun insertAccounts(accounts: List<Account>) {
        dropTable()
        getSmartStore().registerSoup(TABLE_NAME, AccountDBDao.getIndexSpec())
        accounts.forEach { getSmartStore().create(TABLE_NAME, AccountDBDao(it)) }
        //JDELog.e("inserted, there are... {${getAllAccounts().size}} accounts in table")
    }

    fun updateAccounts(accounts: List<Account>, clearSoup: Boolean) {

        if(clearSoup){
            clearTable()
        }
        accounts.forEach {
            insertOrUpdate(AccountDBDao.ID, AccountDBDao(it))
        }
        //JDELog.e("updated accounts, there are... {${getAllAccounts().size}} accounts in table")
    }

    fun getAllAccounts(): List<Account> {
        val result = getSmartStore().query(QuerySpec.buildAllQuerySpec(TABLE_NAME, AccountDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        
        return Account.Mapper.fromDB(result)

    }

    /*
   buildLikeQuerySpec(String soupName, String path, String likeKey, String orderPath, Order order, int pageSize)
   buildLikeQuerySpec(String soupName, String[] selectPaths, String path, String likeKey, String orderPath, Order order, int pageSize) {

    */
    fun getAccountByName(accountName: String): List<Account> {
        val result = getSmartStore().query(QuerySpec.buildLikeQuerySpec(
            DBAccountHelper.TABLE_NAME,
            AccountDBDao.NAME, accountName, AccountDBDao.NAME, QuerySpec.Order.ascending, PAGE_SIZE), FIRST_PAGE_INDEX)
        return Account.Mapper.fromDB(result)
    }

    
    

    fun getAccountsById(accountId: String): List<Account> {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${AccountDBDao.ID}}  = \"$accountId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val accounts = mutableListOf<Account>()
        for (i in 0 until result.length()) {
            accounts.add(Account.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return accounts
    }

    fun getAccountById(accountId: String): Account? {
        val result = getSmartStore().query(
            QuerySpec.buildSmartQuerySpec(
                "SELECT * FROM {$TABLE_NAME} " +
                        "WHERE ({$TABLE_NAME:${AccountDBDao.ID}}  = \"$accountId\")",
                PAGE_SIZE
            ),
            FIRST_PAGE_INDEX
        )
        val accounts = mutableListOf<Account>()
        for (i in 0 until result.length()) {
            accounts.add(Account.Mapper.fromDB(result.getJSONArray(i).getJSONObject(1)))
        }
        return accounts.firstOrNull()
    }





    override fun getAllQuerySpec(): QuerySpec {
        return QuerySpec.buildAllQuerySpec(TABLE_NAME, AccountDBDao.ID, QuerySpec.Order.ascending, PAGE_SIZE)
    }
}