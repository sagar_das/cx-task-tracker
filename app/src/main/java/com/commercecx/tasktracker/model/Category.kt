package com.commercecx.tasktracker.model

/**
 * Created by Sagar Das on 8/20/20.
 */
import java.io.Serializable


data class Category(val id: Long, val name: String, var imageResourceId: Int) : Serializable