package com.commercecx.tasktracker.model.dao

/**
 * Created by Sagar Das on 8/25/20.
 */

import com.commercecx.tasktracker.model.Account
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject


class AccountDBDao(account: Account) : JSONObject() {


    companion object {

        const val ID = "Id"
        const val NAME = "Name"
        const val DESCRIPTION = "Description"
        const val BILLINGSTREET = "BillingStreet"
        const val BILLINGCITY = "BillingCity"
        const val BILLINGSTATE = "BillingState"
        const val BILLINGPOSTALCODE = "BillingPostalCOde"
        const val BILLINGCOUNTRY = "BillingCountry"
        const val SHIPPINGSTREET = "ShippingStreet"
        const val SHIPPINGCITY = "ShippingCity"
        const val SHIPPINGSTATE = "ShippingState"
        const val SHIPPINGPOSTALCODE = "ShippingPostalCode"
        const val SHIPPINGCOUNTRY = "ShippingCountry"


        fun getIndexSpec() =
            arrayOf(
                IndexSpec(ID, SmartStore.Type.string),
                IndexSpec(NAME, SmartStore.Type.string),
                IndexSpec(DESCRIPTION, SmartStore.Type.string),
                IndexSpec(BILLINGSTREET, SmartStore.Type.string),
                IndexSpec(BILLINGCITY, SmartStore.Type.string),
                IndexSpec(BILLINGSTATE, SmartStore.Type.string),
                IndexSpec(BILLINGPOSTALCODE, SmartStore.Type.string),
                IndexSpec(BILLINGCOUNTRY, SmartStore.Type.string),
                IndexSpec(SHIPPINGSTREET, SmartStore.Type.string),
                IndexSpec(SHIPPINGCITY, SmartStore.Type.string),
                IndexSpec(SHIPPINGSTATE, SmartStore.Type.string),
                IndexSpec(SHIPPINGPOSTALCODE, SmartStore.Type.string),
                IndexSpec(SHIPPINGCOUNTRY, SmartStore.Type.string)

            )
    }

    init {
        put(ID, account.id)
        put(NAME, account.name)
        put(DESCRIPTION, account.description)
        put(BILLINGSTREET, account.billingStreet)
        put(BILLINGCITY, account.billingCity)
        put(BILLINGSTATE, account.billingState)
        put(BILLINGPOSTALCODE, account.billingPostalCode)
        put(BILLINGCOUNTRY, account.billingCountry)
        put(BILLINGSTREET, account.billingStreet)
        put(BILLINGCITY, account.billingCity)
        put(BILLINGSTATE, account.billingState)
        put(BILLINGPOSTALCODE, account.billingPostalCode)
        put(BILLINGCOUNTRY, account.billingCountry)

    }
}



