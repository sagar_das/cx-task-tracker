package com.commercecx.tasktracker.model.dao

import com.commercecx.tasktracker.model.Opportunity
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject

/**
 * Created by Sagar Das on 12/9/20.
 */
class OpportunityDBDao (opportunity: Opportunity) : JSONObject() {


    companion object {

        const val ID = "Id"
        const val NAME = "Name"



        fun getIndexSpec() =
            arrayOf(
                IndexSpec(ID, SmartStore.Type.string),
                IndexSpec(NAME, SmartStore.Type.string)


            )
    }

    init {
        put(ID, opportunity.id)
        put(NAME, opportunity.name)


    }
}



