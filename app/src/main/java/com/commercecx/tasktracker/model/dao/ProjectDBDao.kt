package com.commercecx.tasktracker.model.dao

import com.commercecx.tasktracker.model.Project
//import com.salesforce.androidsdk.mobilesync.target.SyncTarget.*
import com.salesforce.androidsdk.smartstore.store.IndexSpec
import com.salesforce.androidsdk.smartstore.store.SmartStore
import org.json.JSONObject

/**
 * Created by Sagar Das on 11/18/20.
 */
class ProjectDBDao(project: Project) : JSONObject() {


    companion object {

        const val ID = "Id"
        const val NAME = "Name"
        const val ACCOUNT = "Amount"
        const val BUDGETAMOUNT = "BudgetAmount"
        const val PROJECTSTATUS = "ProjectStatus"
        const val QUOTEPROPOSAL = "QuoteProposal"
        const val STARTDATE = "StartDate"



        fun getIndexSpec() =
            arrayOf(
                IndexSpec(ID, SmartStore.Type.string),
                IndexSpec(NAME, SmartStore.Type.string),
                IndexSpec(ACCOUNT, SmartStore.Type.string),
                IndexSpec(BUDGETAMOUNT, SmartStore.Type.string),
                IndexSpec(PROJECTSTATUS, SmartStore.Type.string),
                IndexSpec(QUOTEPROPOSAL, SmartStore.Type.string),
                IndexSpec(STARTDATE, SmartStore.Type.string)

            )
    }

    init {
        put(ID, project.id)
        put(NAME, project.projectName)
        put(ACCOUNT, project.account)
        put(BUDGETAMOUNT, project.budgetamount)
        put(PROJECTSTATUS, project.projectstatus)
        put(QUOTEPROPOSAL, project.quoteProposal)
        put(STARTDATE, project.startDate)

    }
}