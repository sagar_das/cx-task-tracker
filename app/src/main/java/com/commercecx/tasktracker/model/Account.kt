package com.commercecx.tasktracker.model

import com.commercecx.tasktracker.model.dao.AccountDBDao
import com.commercecx.tasktracker.util.extensions.stringFromKey
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Sagar Das on 8/25/20.
 */
data class Account(
    val id: String,
    val name: String,
    val description: String,
    val billingStreet: String,
    val billingCity: String,
    val billingState: String,
    val billingPostalCode: String,
    val billingCountry: String,
    val shippingStreet: String,
    val shippingCity: String,
    val shippingState: String,
    val shippingPostalCode: String,
    val shippingCountry: String
) : Serializable {

    object Mapper {

        fun from(account: JSONObject) = Account(
            account.stringFromKey("Id"),
            account.stringFromKey("Name"),
            account.stringFromKey("Description"),
            account.stringFromKey("BillingStreet"),
            account.stringFromKey("BillingCity"),
            account.stringFromKey("BillingState"),
            account.stringFromKey("BillingPostalCode"),
            account.stringFromKey("BillingCountry"),
            account.stringFromKey("ShippingStreet"),
            account.stringFromKey("ShippingCity"),
            account.stringFromKey("ShippingState"),
            account.stringFromKey("ShippingPostalCode"),
            account.stringFromKey("ShippingCountry")

        )

        fun fromDB(dbAccount: JSONObject) = Account(
            dbAccount.optString(AccountDBDao.ID, "ID"),
            dbAccount.optString(AccountDBDao.NAME, "NAME"),
            dbAccount.optString(AccountDBDao.DESCRIPTION, "DESCRIPTION"),
            dbAccount.optString(AccountDBDao.BILLINGSTREET, "BILLINGSTREET"),
            dbAccount.optString(AccountDBDao.BILLINGCITY, "BILLINGCITY"),
            dbAccount.optString(AccountDBDao.BILLINGSTATE, "BILLINGSTATE"),
            dbAccount.optString(AccountDBDao.BILLINGPOSTALCODE, "BILLINGPOSTALCODE"),
            dbAccount.optString(AccountDBDao.BILLINGCOUNTRY, "BILLINGCOUNTRY"),
            dbAccount.optString(AccountDBDao.SHIPPINGSTREET, "SHIPPINGSTREET"),
            dbAccount.optString(AccountDBDao.SHIPPINGCITY, "SHIPPINGCITY"),
            dbAccount.optString(AccountDBDao.SHIPPINGSTATE, "SHIPPINGSTATE"),
            dbAccount.optString(AccountDBDao.SHIPPINGPOSTALCODE, "SHIPPINGPOSTALCODE"),
            dbAccount.optString(AccountDBDao.SHIPPINGCOUNTRY, "SHIPPINGCOUNTRY")

        )

        fun from(accountArray: JSONArray): List<Account> {
            val accounts = mutableListOf<Account>()
            for (i in 0 until accountArray.length()) {
                accounts.add(Account.Mapper.from(accountArray.getJSONObject(i)))
            }
            return accounts
        }

        fun fromDB(accountArray: JSONArray): List<Account> {
            val accounts = mutableListOf<Account>()
            for (i in 0 until accountArray.length()) {
                accounts.add(Account.Mapper.fromDB(accountArray.getJSONObject(i)))
            }
            return accounts
        }

    }



}